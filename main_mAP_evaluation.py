import numpy as np
import json
from tqdm import tqdm
import os
import collections
import torch
from collections import defaultdict
import itertools


import settings
import scripts.visualization as visualization
import scripts.mmdetection as MMDetWrapper
from scripts.domain_centric_filters import calculate_area
from scripts.metrics.mean_avg_precision import mean_average_precision
from scripts.functions import xywh2xyxy_1d


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def nested_dict():
    return collections.defaultdict(nested_dict)


ap_labels = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def predict_on_dataset(_model, _dataset, _dict_of_predictions):
    prediction_threshold = 0.5
    for cnt, (images, gts) in enumerate(_dataset.coco_format_image_iterator()):
        print('\r {}/{}'.format(cnt, int(_dataset.dataset_len / _dataset.batch_size)), end='', flush=True)

        _boxes, _classes, _labels, _scores = list(), list(), list(), list()

        _model.predict(images, prediction_threshold, _dataset, _boxes, _classes, _labels, _scores)

        if len(gts[0]) > 0:
            image_id = gts[0][0]["image_id"]
            list_of_predictions = list()
            if len(_boxes) > 0:
                for bb, cls, lab, scr in zip(_boxes[0], _classes[0], _labels[0], _scores[0]):
                    list_of_predictions.append([image_id, cls[0], scr[0], bb])
            _dict_of_predictions[image_id] = [gts[0], [list_of_predictions, _dataset.image_size]]

        # # @FixMe - just to break out
        # if image_id > 15:
        #     break


def calculate_f_mAP_distance(_dict_of_predictions, _path, _models_name, _dataset_name):
    dict_of_preds = collections.defaultdict(list)
    dict_of_gts = collections.defaultdict(list)

    print("Analyzing predictions")
    for image_id, datas in tqdm(_dict_of_predictions.items()):
        gts = datas[0]
        preds = datas[1]

        im_width, im_height = preds[1]#[2:4]
        for gt in gts:
            true_boxes = xywh2xyxy_1d(np.asarray(gt["bbox"]))
            rel_size = calculate_area(true_boxes) / (im_width * im_height)
            dict_of_gts[int(gt["category_id"])].append([gt["image_id"], gt["category_id"], 1.0, true_boxes, gt["distance"], rel_size, (im_height, im_width)])
            #list_of_true_boxes.append(
            #    [gt["image_id"], gt["category_id"], 1.0, true_boxes, gt["distance"], rel_size, (im_height, im_width)])

        for pred in preds[0]:
            # id, class, conf, bb
            dict_of_preds[int(pred[1])].append([pred[0], pred[1], pred[2], pred[3]])
            #list_of_predictions.append([pred[0], pred[1], pred[2], pred[3]])

    print("Calculating mAPs")
    dict_average_precisions = nested_dict()
    for iou_threshold in tqdm(settings.iou_thresholds):
        average_precision = mean_average_precision(dict_of_preds, dict_of_gts, dict_average_precisions,
                                                   labels=dataset.labels,
                                                   iou_threshold=iou_threshold, box_format="corners",
                                                   num_classes=len(dataset.labels))

    with open(os.path.join(_path, "{}_{}_mAP_dataset_stats_dict.json".format(_models_name, _dataset_name)),
              'w') as mAP_statistics:
        json.dump(dict_average_precisions, mAP_statistics)

    return dict_average_precisions


def plot_f_mAP_distance(_dict_average_precisions, path_to_save_data, models_name, dataset_name):
    print("Plotting results of mAP")
    for cls, ious in tqdm(_dict_average_precisions.items()):
        x, y, z = list(), list(), list()
        x_fp = list()
        y_fp = list()
        mAP = list()
        for iou, datas in ious.items():
            # [mAP_class, recalls[1], precisions[1], SCOREs, IOUs, BBrel_size, IMGsize, OBJdistances, FP_score, FP_BB_rel_size]
            gt_indices = [index for index, bb in enumerate(datas[5]) if bb > 0.0]
            mAP.append(datas[0])
            x.append(list(np.asarray(datas[3])[gt_indices]))  # scores
            y.append(list(np.asarray(datas[5])[gt_indices]))  # bb rel size
            z.append(list(np.asarray(datas[7])[gt_indices]))  # object distances
            x_fp.append(datas[8])
            y_fp.append(datas[9])

        name = cls + "_prediction_vs_object_size"
        label = "Prediction vs. Object size"
        title = "Prediction conf. of class: {} related to the object size, for model {} on dataset {}".format(cls, models_name, dataset_name)
        x = list(itertools.chain.from_iterable(x))
        y = list(itertools.chain.from_iterable(y))
        z = list(itertools.chain.from_iterable(z))
        fp = list(itertools.chain.from_iterable(x_fp))
        FN = x.count(0.0)
        TP = len(y) - FN
        FP = len(fp)
        legend = "mAP:{:2.3f}\nTP:{}\nFN:{}".format(np.sum(mAP) / len(mAP), TP, FN)
        visualization.Visualization.plot_x_y(path_to_save_data, name, np.asarray(x), np.asarray(y), label,
                                             exp_decay_f=None, legends=legend, xlabel="Confidence [-]", ylabel="BB relative size [-]",
                                             title=title)

        label = cls + "_gt_vs_predicted"
        det_indices = [index for index, scr in enumerate(x) if scr > 0.0]
        bb_detected = np.asarray(y)[det_indices]
        n_bb, bins_bb, _ = visualization.Visualization. \
            plot_histogram_of_dataset(np.asarray(y),  # relative BB sizes
                                      path_to_save_data,
                                      label + "_relative_BB_sizes",
                                      dataset_name + "'s dataset distribution of BB relative size for objects: " + cls,
                                      label,
                                      _x_label="Relative BB size [-]",
                                      r_min=0.0, r_max=1.0,
                                      num_bins=100,
                                      _np_data_s=bb_detected)

        n_bb, bins_bb, _ = visualization.Visualization. \
            plot_histogram_of_dataset(np.asarray(z),  # object distances
                                      path_to_save_data,
                                      label + "_object_distance",
                                      dataset_name + ":'s dataset distribution of distance for objects: " + cls,
                                      label,
                                      _x_label="Object distance [m]",
                                      r_min=0.0, r_max=1.0,
                                      num_bins=100,
                                      _np_data_s=bb_detected)


def save_ap(_dataset, _dict_average_precisions, _ap_statistics_dict, _class_indices, _text, _number_of_turns):
    for class_index, ious in _dict_average_precisions.items():
        if class_index in _class_indices:
            ious_aps = [value for iou, value in ious.items()]
            ap = float(sum(ious_aps) / len(ious_aps))
            _ap_statistics_dict[str(_number_of_turns)][_dataset.labels[int(class_index)]][_text] = [ap]
            print("AP for class {}, is {:.2f}".format(_dataset.labels[int(class_index)], ap))


if __name__ == '__main__':

    analyze_predictions = True
    online = True

    """
    ### 1 ) Analyze predictions of a model
    ### a) mAP related to object size
    ### -- x: precision, y: recall, z: relative size of object
    """
    if analyze_predictions:
        for dataset_name, train_test_val in zip(settings.datasets_names, settings.datasets_split):

            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name)
            dataset = settings.get_a_dataset(dataset_name,
                                             train_test_val=train_test_val,
                                             _device=device,
                                             _batch_size=1,
                                             import_coco_format=True)

            for models_name in settings.models_names:

                boxes, classes, labels, scores = list(), list(), list(), list()

                model = MMDetWrapper.MMDetWrapper(models_name, device, dataset)

                path_to_save_data = os.path.join(settings.path_to_logging_file, models_name, dataset_name)
                if not os.path.exists(path_to_save_data):
                    os.makedirs(path_to_save_data)

                if online:
                    # predict on the complete dataset
                    dict_of_predictions = nested_dict()
                    predict_on_dataset(model, dataset, dict_of_predictions)

                    with open(
                            os.path.join(path_to_save_data, '{}_{}_predictions.json'.format(models_name, dataset_name)),
                            'w') as outfile:
                        json.dump(dict_of_predictions, outfile, cls=NpEncoder)

                else:
                    with open(
                            os.path.join(path_to_save_data, '{}_{}_predictions.json'.format(models_name, dataset_name)),
                            'r') as json_predictions:
                        dict_of_predictions = json.load(json_predictions)

                    #d1 = dict(list(dict_of_predictions.items())[:1000])
                    #d1 = dict(list(dict_of_predictions.items()))

                dict_average_precisions = calculate_f_mAP_distance(dict_of_predictions, path_to_save_data, models_name, dataset_name)

                plot_f_mAP_distance(dict_average_precisions, path_to_save_data, models_name, dataset_name)

