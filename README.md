# Object Detection Criticality & Domain-centric Automotive Datasets

This repository contains the code for two of our papers:

1. [Neural Criticality Metric for Object Detection Deep Neural Networks](https://link.springer.com/chapter/10.1007/978-3-031-14862-0_20)
2. Domain-centric ADAS datasets at [SafeAI workshop 2023](https://safeai.webs.upv.es/)

## Usage

1. Clone the repository and install the requirements via pip:

    `pip install -r requirements.txt`

2. Download the datasets you want to analyze, e.g. KITTI, nuScenes or Waymo; and use the [setting.py](settings.py) for configuring the paths to the datasets etc.

3. Use the files [main_analyze_network.py](main_analyze_network.py) and [main_evaluate_statistics.py](main_evaluate_statistics.py) to generate results for your neural network similar to our paper [Neural Criticality Metric for Object Detection Deep Neural Networks](https://link.springer.com/chapter/10.1007/978-3-031-14862-0_20)

4. Use the file [main_analyze_datasets.py](main_analyze_datasets.py) to reproduce the results from our dataset analysis (paper 2).

5. You can use files x and y for visualizing z.

## Additional results for paper 'Domain-centric ADAS datasets'

Additional analysis results for the different analysed datasets in the paper "Domain-centric ADAS datasets" can be found [here](/additional_results/dataset_analysis)

The structure for each dataset is as follows:

```
dataset_name
--results
----train_val
------visualization
--------class_1
--------class_2
--------class_n
```

You can find distribution heatmaps, BBs' relative size and distance-to-object analyses for each class.

For instance, see the folowing images from the A2D2 dataset for class car:

![heatmap](/additional_results/dataset_analysis/audi/train_val/visualization/Car/Car_BB_heatmap.png)

![bb_relative](/additional_results/dataset_analysis/audi/train_val/visualization/Car/Car_bb_relative_to_distance.png)

![distance](/additional_results/dataset_analysis/audi/train_val/visualization/Car/Car_distances_in_meters_hist.png)

