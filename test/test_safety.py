import sys
sys.path.append('/home/tobias/Git_projects/ObjectDetectionCriticality/')
import scripts.SafetyAlgorithm as safety_algo

import numpy as np
import matplotlib.pyplot as plt


def test_criticality(
        stimul_groundtruth_conf_list: list,
        stimul_masked_conf_list: list,
        stimul_goundtruth_index_list: list,
        stimul_masked_index_list: list,
        stimul_criticality_tau: float,
        stimul_groundtruth_IOU_list: list,
        stimul_masked_IOU_list: list) -> [list,list,list]:

    safety = safety_algo.SafetyAlgorithm()

    results_classification_cricitality = list()
    for index in range(len(stimul_groundtruth_conf_list)):
        results_classification_cricitality.append(
            safety.calculate_criticality_classification(
                stimul_groundtruth_conf_list[index],
                stimul_masked_conf_list[index],
                stimul_goundtruth_index_list[index],
                stimul_masked_index_list[index],
                stimul_criticality_tau
            ))

    results_detection_cricitality = list()
    for index in range(len(stimul_groundtruth_IOU_list)):
        results_detection_cricitality.append(
            safety.calculate_criticality_detection(
                stimul_groundtruth_IOU_list[index],
                stimul_masked_IOU_list[index]
            ))

    resulting_criticality = list()
    for criticality_tuple in zip(results_classification_cricitality, results_detection_cricitality):
        resulting_criticality.append(safety.calculate_resulting_criticality(criticality_tuple[0], criticality_tuple[1]))

    return results_classification_cricitality, results_detection_cricitality, resulting_criticality


if __name__ == '__main__':
    stimul_groundtruth_conf_list = [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                                 # masked conf is lower     masked conf is higher
                                    0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                                    0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                                    0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1,
                                    0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1]
    stimul_masked_conf_list = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                              # masked conf is lower     masked conf is higher
                               0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                               0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                               0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
                               0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9
                               ]
    stimul_goundtruth_index_list = [1, 1, 1, 1, 1, 1, 1, 1, 1, # same class
                                    2, 2, 2, 2, 2, 2, 2, 2, 2, # different class
                                    1, 1, 1, 1, 1, 1, 1, 1, 1, # same class
                                    2, 2, 2, 2, 2, 2, 2, 2, 2, # different class
                                    1, 1, 1, 1, 1, 1, 1, 1, 1 # same class
                                    ]
    stimul_masked_index_list = [1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1,
                                1, 1, 1, 1, 1, 1, 1, 1, 1
                                ]
    stimul_criticality_tau = 0.0

    stimul_groundtruth_IOU_list = [0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, # same IOU
                                   0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, # same IOU
                                   0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99,
                                   # masked IOU is higher     masked IOU is lower
                                   0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99,
                                   # masked IOU is higher     masked IOU is lower
                                   0.99, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6,]
                                  # masked IOU is lower     masked IOU is higher

    stimul_masked_IOU_list = [0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6,
                              0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6,
                              0.99, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6,
                              0.99, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6,
                              0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99,]

    results_classification, results_detection, results_criticality = test_criticality(
        stimul_groundtruth_conf_list,
        stimul_masked_conf_list,
        stimul_goundtruth_index_list,
        stimul_masked_index_list,
        stimul_criticality_tau,
        stimul_groundtruth_IOU_list,
        stimul_masked_IOU_list
    )
    print(results_classification)
    print(results_detection)
    print(results_criticality)

    plt.plot(results_classification)
    plt.plot(results_detection)
    plt.plot(results_criticality)
    plt.legend(["classification", "detection", "resulting"])
    plt.xticks(np.arange(0, len(stimul_masked_IOU_list), 1.0))
    # plt.show()
    plt.grid()
    plt.savefig('Criticality_plot.png',dpi=400)
