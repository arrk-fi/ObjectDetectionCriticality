import sys
sys.path.append('/home/tobias/Git_projects/ObjectDetectionCriticality/')
import scripts.SafetyAlgorithm as safety_algo

import numpy as np
import matplotlib.pyplot as plt


def test_criticality(
        tp_array_list: list,
        masked_tp_array_list: list,
        tp_array_IOU_list: list,
        masked_TP_array_IOU_list: list,
        tp_array_conf_list: list,
        masked_tp_array_conf_list: list,
        non_masked_fp_list: list,
        masked_fp_list: list)-> [list,list,list]:

    safety = safety_algo.SafetyAlgorithm()

    results_tp_fn = list()

    for index in range(len(tp_array_list)):
        results_tp_fn.append(
            safety.calculate_criticality_TP_FN(
                tp_array_list[index],
                masked_tp_array_list[index],
                tp_array_IOU_list[index],
                masked_TP_array_IOU_list[index],
                tp_array_conf_list[index],
                masked_tp_array_conf_list[index]
            ))

    results_fp = list()

    for index in range(len(tp_array_list)):
        results_fp.append(
            safety.calculate_criticality_FP(
                non_masked_fp_list[index],
                masked_fp_list[index]
            ))

    resulting_criticality = list()
    for criticality_tuple in zip(results_tp_fn, results_fp):
        resulting_criticality.append(safety.calculate_resulting_criticality(criticality_tuple[0], criticality_tuple[1]))

    return results_tp_fn, results_fp, resulting_criticality


if __name__ == '__main__':
    # Use case according to our image with bike, ped and car
    tp_array_list=       [[0,1,0],[1,1,1,1,1,1,1,0,0,0]]
    masked_tp_array_list=[[0,0,1],[0,0,0,0,0,1,1,1,0,0]]
    tp_array_IOU_list=       [[0.0,0.8,0.0], [0.6,0.7,0.7,0.8,0.99,0.65,0.8,0.0,0.0,0.0]]
    masked_TP_array_IOU_list=[[0.0,0.0,0.9], [0.0,0.0,0.0,0.0,0.0 ,0.55,0.9,0.6,0.0,0.0]]
    tp_array_conf_list=       [[0.0,0.6,0.0], [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.0,0.0,0.0]]
    masked_tp_array_conf_list=[[0.0,0.0,0.75],[0.0,0.0,0.0,0.0,0.0,0.3,0.8,0.0,0.0]]
    non_masked_fp_list=[2, 5]
    masked_fp_list=    [3, 100]


    results_tp_fn, results_fp, results_criticality = test_criticality(
        tp_array_list,
        masked_tp_array_list,
        tp_array_IOU_list,
        masked_TP_array_IOU_list,
        tp_array_conf_list,
        masked_tp_array_conf_list,
        non_masked_fp_list,
        masked_fp_list
    )
    print(results_tp_fn)
    print(results_fp)
    print(results_criticality)

    # plt.plot(results_classification)
    # plt.plot(results_detection)
    # plt.plot(results_criticality)
    # plt.legend(["classification", "detection", "resulting"])
    # plt.xticks(np.arange(0, len(stimul_masked_IOU_list), 1.0))
    # # plt.show()
    # plt.grid()
    # plt.savefig('Criticality_plot.png',dpi=400)
