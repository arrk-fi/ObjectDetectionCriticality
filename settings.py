import os
import collections
import torch
import torchvision
import json

import scripts.coco_dataset as CoCoDataSet
import scripts.automotive_datasets as AutomotiveDataSet

import scripts.model as ModelWrapper
import scripts.detr_wrapper as Detr
from scripts.visualization import Visualization

logging_images = False
batch_size = 1

#path_to_logging_file = os.path.join("data", "logging")
path_to_logging_file = os.path.join("E:", "dataset", "data", "logging")
#path_to_logging_file = os.path.join("/media", "oem", "Elements", "dataset", "data", "logging")
#path_to_labels = os.path.join("data", "dataset", "labels")
path_to_labels = os.path.join("E:", "dataset", "labels")
#path_to_labels = os.path.join("/media", "oem", "Elements", "dataset", "labels")
#path_to_dataset = os.path.join("data", "dataset")
path_to_dataset = os.path.join("E:", "dataset")
#path_to_dataset = os.path.join("/media", "oem", "Elements", "dataset")
#path_to_annotations = os.path.join("data", "dataset")
path_to_annotations = os.path.join("E:", "dataset")
#path_to_annotations = os.path.join("/media", "oem", "Elements", "dataset")
path_to_models = os.path.join("E:", "models")


models_names = ["faster-rcnn_r50", "yolox-l", "detr_r50"]

datasets_split = ["val", "train_val", "train_val", "train_val", "train", "train"]
datasets_names = ["A2D2", "Kitti", "LyftLevel5", "nuScenes", "ONCE", "Waymo"]
datasets_img_sizes = [(1920, 1208), (1242, 375), (1224, 1024), (1600, 900), (1920, 1020), (1920, 1280)]
datasets_opt_flows = [1.25, 1.45, 2.55, 1.0, 1.0, 0.95]

# datasets_split = ["train_val"]
# datasets_names = ["Kitti"]
# datasets_img_sizes = [(1242, 375)]
# datasets_opt_flows = [1.45]

list_of_analyzed_layers = None

images_to_be_investigated = "all"

iou_thresholds = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]

global_analyzed_classes = ["person"]
global_analyzed_classes = ["person", "car", "bicycle", "motorcycle", "bus", "train",
                           "truck", "traffic light", "stop sign"]

verbose = False

allowed_classes = global_analyzed_classes


def get_a_dataset(name, _path_to_dataset=path_to_dataset,
                  _path_to_labels=path_to_labels,
                  _path_to_annotations=path_to_annotations,
                  size_h=(800, 600), size_v=(800, 1052),
                  _batch_size=batch_size,
                  _device="cpu",
                  export_to_coco=False,
                  train_test_val="train",
                  import_coco_format=False):

    if name == "COCO":
        if images_to_be_investigated != "all":
            dataset = CoCoDataSet.SmallDataSet(_path_to_dataset, _path_to_annotations, _path_to_labels, device=_device,
                                               batch_size=batch_size, h_size=(800, 600), v_size=(800, 1052),
                                               allowed_classes=allowed_classes,
                                               images_to_be_investigated=images_to_be_investigated,
                                               path_to_dataset=_path_to_dataset)
        else:
            dataset = CoCoDataSet.CocoDataSet(_path_to_dataset, _path_to_annotations, _path_to_labels, device=_device,
                                              batch_size=batch_size, h_size=(800, 600), v_size=(800, 1052),
                                              allowed_classes=allowed_classes,
                                              images_to_be_investigated=images_to_be_investigated,
                                              path_to_dataset=_path_to_dataset)

    elif name == "A2D2":
        dataset = AutomotiveDataSet.AudiDataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                batch_size=batch_size, export_to_coco=export_to_coco,
                                                train_test_val=train_test_val, import_coco_format=import_coco_format)

    elif name == "Waymo":
        dataset = AutomotiveDataSet.WaymoDataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                 batch_size=batch_size, export_to_coco=export_to_coco,
                                                 train_test_val=train_test_val, import_coco_format=import_coco_format)

    elif name == "Kitti":
        dataset = AutomotiveDataSet.KittiDataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                 batch_size=batch_size, export_to_coco=export_to_coco,
                                                 train_test_val=train_test_val, import_coco_format=import_coco_format)

    elif name == "nuScenes":
        dataset = AutomotiveDataSet.NuScenesDataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                    batch_size=batch_size, export_to_coco=export_to_coco,
                                                    train_test_val=train_test_val, import_coco_format=import_coco_format)

    elif name == "LyftLevel5":
        dataset = AutomotiveDataSet.LyftLevel5DataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                      batch_size=batch_size, export_to_coco=export_to_coco,
                                                      train_test_val=train_test_val, import_coco_format=import_coco_format)

    elif name == "ONCE":
        dataset = AutomotiveDataSet.OnceDataSet(_path_to_dataset, _path_to_dataset, _path_to_labels,
                                                batch_size=batch_size, export_to_coco=export_to_coco,
                                                train_test_val=train_test_val, import_coco_format=import_coco_format)

    else:
        raise ValueError("Wrong dataset name.")

    return dataset


def custom_layers_and_kernels(logging_path, most_or_anti_neurons, number_of_analyzed_neurons):
    with open(os.path.join(logging_path, "criticality", most_or_anti_neurons + '_critical_neurons.json'),
              'r') as json_file:
        critical_neurons = json.load(json_file)
    sorted_critical_neurons = Visualization.filter_neurons(critical_neurons)

    def nested_dict():
        return collections.defaultdict(nested_dict)

    ciritical_neurons_dict = nested_dict()
    count = 0
    for mean_value, std_values in sorted_critical_neurons.items():
        for std_value, labels in std_values.items():
            for label, critical_neuron in labels.items():
                if 0.5 > std_value > 0.01 and label == global_analyzed_classes and count < number_of_analyzed_neurons:
                    ciritical_neurons_dict[critical_neuron["layer"]][critical_neuron["kernel"]] = std_value
                    count += 1

    return ciritical_neurons_dict


def return_weights(_model, _models_name):
    _parameter_dict = collections.defaultdict(list)
    if _models_name == "detr":
        models = [_model.model.backbone, _model.model.transformer]
    else:
        models = [_model]

    for pytorch_model in models:
        for name, parameter in pytorch_model.named_parameters():
            if list_of_analyzed_layers is not None:
                # TBD add biases and mask them simultunasly
                if 'weight' in name and name in list_of_analyzed_layers:
                    _parameter_dict[name].append(parameter)
            else:
                if 'weight' in name:
                    _parameter_dict[name].append(parameter)

    return _parameter_dict


def take_a_model(name, device):
    _image_size_h = (800, 600)
    _image_size_v = (800, 1052)

    if name == "fasterrcnn_resnet50_fpn":
        _model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True,
                                                                      progress=True,
                                                                      num_classes=91,
                                                                      trainable_backbone_layers=False)
        _parameter_dict = return_weights(_model, name)

        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name)

    elif name == "ssdlite320_mobilenet_v3_large":
        _model = torchvision.models.detection.ssdlite320_mobilenet_v3_large(pretrained=True,
                                                                            progress=True,
                                                                            num_classes=91,
                                                                            trainable_backbone_layers=False)
        _parameter_dict = return_weights(_model, name)

        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name)

    elif name == "yolov5":
        # https://pytorch.org/hub/ultralytics_yolov5/
        # pip install -qr https://raw.githubusercontent.com/ultralytics/yolov5/master/requirements.txt
        _model = torch.hub.load('ultralytics/yolov5', 'yolov5l', pretrained=True)  # large version taken
        _model.conf = 0.5  # NMS confidence threshold
        _model.iou = 0.5  # NMS IoU threshold
        _model.agnostic = False  # NMS class-agnostic
        _model.multi_label = False  # NMS multiple labels per box
        _model.classes = None  # (optional list) filter by class, i.e. = [0, 15, 16] for COCO persons, cats and dogs
        _model.max_det = 300  # maximum number of detections per image
        _model.amp = False  # Automatic Mixed Precision (AMP) inference
        # _model = torch.hub.load('ultralytics/yolov5', 'custom', 'yolov5s.mlmodel')
        #   torch:           = torch.zeros(16,3,320,640)  # BCHW (scaled to size=640, 0-1 values)
        _parameter_dict = return_weights(_model, name)
        _image_size_h = (640, 480)
        _image_size_v = (640, 800)

        _mapped_model = ModelWrapper.CNNWrapper(_model, device, name)

    elif name == "detr":
        # https://colab.research.google.com/github/facebookresearch/detr/blob/colab/notebooks/detr_demo.ipynb
        _model = torch.hub.load('facebookresearch/detr', 'detr_resnet50', pretrained=True, num_classes=91)
        _model.eval()
        _model.to(device)

        _mapped_model = Detr.DETRWrapper(_model)
        _parameter_dict = return_weights(_mapped_model, name)

    else:
        raise ValueError("Wrong models name.")

    print("{}, is being analyzed.".format(name))
    print("{}, layers is being analyzed.".format(len(list(_parameter_dict.keys()))))

    return _mapped_model, _parameter_dict
