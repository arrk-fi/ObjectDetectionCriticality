import json
import copy
import collections
import torch
import os
import numpy as np
from PIL import Image

import scripts.functions as functions


class GeneralDataSet:
    """
    General Class of Dataset - is inherited by all others
    """

    def __init__(self, name, train_val_test, batch_size, path_to_dataset=None):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.dataset_len = 0
        self.batch_size = batch_size
        self.num_classes = 0
        self.labels = 0
        self.name = name
        self.train_val_test = train_val_test
        self.image_size = None
        self.class_indices = list()
        self.dataset = collections.defaultdict(list)
        self.annotations = collections.defaultdict(list)
        self.path_to_dataset = path_to_dataset
        # with open(os.path.join(self.path_to_dataset, "labels", "matching_labels.json"), 'r') as json_file:
        #     self.clustered_labels = json.load(json_file)

    def coco_format_image_iterator(self):
        batch_images = list()
        batch_targets = list()
        index = 0

        for entry in self.dataset["images"]:
            batch_images.append(
                os.path.join(self.path_to_dataset, self.name, "dataset_coco_format", self.train_val_test,
                             entry["file_name"]))
            batch_targets.append(self.annotations[entry["id"]])
            index = index + 1
            if index == self.batch_size:
                yield batch_images, batch_targets
                batch_images = list()
                batch_targets = list()
                index = 0

    def coco_format_annotation_iterator(self):
        batch_targets = list()
        index = 0

        for entry in self.dataset["annotations"]:
            batch_targets.append(entry)
            index = index + 1
            if index == self.batch_size:
                yield batch_targets
                batch_targets = list()
                index = 0

    def dataset_iterator(self):
        batch_images = list()
        batch_targets = list()
        index = 0

        for key, entry in self.dataset.items():
            image = copy.deepcopy(Image.open(entry[0][0]))
            batch_images.append(np.moveaxis(np.asarray(image), -1, 0))
            batch_targets.append(entry[0][1])
            index = index + 1

            if index == self.batch_size:
                yield torch.Tensor(np.asarray(batch_images)).to(self.device), batch_targets
                # yield indices, np.asarray(batch_images), batch_targets
                batch_images = list()
                batch_targets = list()
                index = 0

    def label_iterator(self):
        batch_targets = list()
        batch_img_sizes = list()
        index = 0

        for key, entry in self.dataset.items():
            batch_img_sizes.append(Image.open(entry[0][0]).size)
            batch_targets.append(entry[0][1])
            index = index + 1

            if index == self.batch_size:
                yield batch_targets, batch_img_sizes
                batch_targets = list()
                batch_img_sizes = list()
                index = 0


class CustomDataSet(GeneralDataSet):
    def __init__(self, path_to_data, path_to_labels, allowed_classes=None):
        super().__init__()

        self.num_classes, self.labels = functions.open_labels(path_to_labels)
        self.labels_numpy = np.asarray(self.labels)
        self.color_maps = np.random.uniform(0, 255, size=(self.num_classes, 3))
        self.filter_labels = allowed_classes
        # self.class_indices = [str(self.labels.index(cls)) for cls in settings.global_analyzed_classes]

        self.dataset = collections.defaultdict(list)
        from PIL import Image
        for root, dirs, files in os.walk(path_to_data, topdown=False):
            image_files = [file for file in files if file.split(".")[1].lower() == "jpg"]
            for name in image_files:
                image = Image.open(os.path.join(root, name))
                json_name = name.split(".")[0] + ".json"
                json_file = open(os.path.join(root, json_name))
                # @FixMe - within the annotation has to be the information about distance
                annotation = json.load(json_file)
                self.dataset[name.split(".")[0]].append((image, annotation))
                # self.dataset[name].append(annotation)
