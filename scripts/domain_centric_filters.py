import cv2
import copy
import numpy as np

from scripts.functions import xywh2xyxy
from scripts.metrics.iou import intersection_over_union_tensor, get_intersection_points


def exp_decay_dist_f(x):
    return np.power(np.e, -x / 6.0) + 0.075  # offset at least 1 meter from ego


def calculate_rel_object_size(im_width, im_height, item):
    # object area / image area = relative object size
    rel_size = calculate_area(item["bbox"]) / (im_width * im_height)

    # TB Investigated - in AUDI one object has relative size 85 - bug in dataset
    return rel_size, item["class"]


def outliers_filter_overlapping_bbs(_boxes, threshold_iou=0.4):
    boxes_xyxy = xywh2xyxy(_boxes)
    iou_matrix = intersection_over_union_tensor(
        np.asarray(boxes_xyxy),
        copy.deepcopy(np.asarray(boxes_xyxy))
    )

    iou_matrix = np.tril(iou_matrix.numpy(), -1)

    if np.max(iou_matrix) > threshold_iou:
        return True, iou_matrix
    else:
        return False, iou_matrix


def outliers_filter(rel_size, dist):
    # Definition of outlier conditions
    outlier_condition = 0.0 > rel_size > 1.0
    outlier_condition |= rel_size > exp_decay_dist_f(dist)
    return outlier_condition


def outliers_dense_optical_flow(prvs_im, next_im, _flow_constant):
    # cv.calcOpticalFlowFarneback(	prev, next, flow, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma,
    # flags	) ->	flow
    flow_or_diff = True
    if prvs_im.shape[0] != next_im.shape[0]:
        dim = (prvs_im.shape[1], prvs_im.shape[0])
        next_im = cv2.resize(next_im, dim, interpolation=cv2.INTER_AREA)
    if flow_or_diff:
        flow = cv2.calcOpticalFlowFarneback(cv2.cvtColor(prvs_im, cv2.COLOR_BGR2GRAY),
                                            cv2.cvtColor(next_im, cv2.COLOR_BGR2GRAY),
                                            None, 0.5, 1, 7, 3, 5, 1.1, 0)
        # to discover stop and go
        # hsv = np.zeros_like(prvs_im)
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
        mean_val = np.mean(mag, axis=1)  # to take the mean of each row

        mean_val = np.mean(mean_val, axis=0) / _flow_constant
        # flow_mag = np.sum(np.sqrt(flow[:, :, 0] * flow[:, :, 0] + flow[:, :, 1] * flow[:, :, 1]))
        return float(mean_val) < 1.0, mean_val

    else:
        if prvs_im.shape[0] == next_im.shape[0] and prvs_im.shape[1] == next_im.shape[1]:
            diff_im = np.sum(np.abs(prvs_im - next_im))  # background subtraction - switch images to greyscale
        else:
            diff_im = _flow_constant
        # ratio between static and dynamic images
        return np.sum(diff_im) < _flow_constant, np.sum(diff_im)


def calculate_area(bbox):
    # within the datasets classes I cast the format to xywh
    return bbox[2] * bbox[3]
