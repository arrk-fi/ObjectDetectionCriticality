import cv2
import numpy as np
import os
import json
import copy


def open_labels(path_to_labels):
    extension = path_to_labels.split(".")[-1]
    assert extension != "json" or extension != "txt", "Unknown format of labels."

    if extension == "json":
        with open(path_to_labels) as jsonFile:
            labels = json.load(jsonFile)
            jsonFile.close()
    else:
        with open(path_to_labels) as f:
            labels = list()
            for line in f:
                labels.append(line)

    return len(labels), labels


def draw_boxes(boxes, boxes_m, classes, classes_m, labels, labels_m, image, colors, line=1):
    # this will help us create a different color for each class
    #
    # read the image with OpenCV
    image = cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB)
    for i, box in enumerate(boxes):
        color = colors[labels[i]]
        cv2.rectangle(
            image,
            (int(box[0]), int(box[1])),
            (int(box[2]), int(box[3])),
            color, line
        )
        cv2.putText(image, classes[i], (int(box[0]), int(box[1] - 5)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, color, 1,
                    lineType=cv2.LINE_AA)

    for i, box in enumerate(boxes_m):
        color = colors[labels_m[i]]
        cv2.rectangle(
            image,
            (int(box[0]), int(box[1])),
            (int(box[2]), int(box[3])),
            color, line + 2
        )

    return image


def draw_box(boxes, classes, labels, input_image, colors, line=1):
    # this will help us create a different color for each class
    #
    # read the image with OpenCV
    image = np.moveaxis(np.asarray(input_image), 0, -1) * 255.0
    image_copy = cv2.cvtColor(image.astype(np.uint8).copy(), cv2.COLOR_BGR2RGB)
    #image = cv2.cvtColor(np.asarray(np.transpose(input_image).shape), cv2.COLOR_BGR2RGB)
    for i, box in enumerate(boxes):
        color = colors[labels[i]]
        cv2.rectangle(
            image_copy,
            (int(box[0]), int(box[1])),
            (int(box[2]), int(box[3])),
            color, line
        )
        cv2.putText(image_copy, classes[i], (int(box[0]), int(box[1] - 5)),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, color, 1,
                    lineType=cv2.LINE_AA)

    return image_copy


def set_key(dictionary, key, value):
    if key not in dictionary:
        dictionary[key] = [value]
    elif type(dictionary[key]) == list:
        dictionary[key].append(value)
    else:
        dictionary[key] = [dictionary[key], value]


def open_txt_as_dict(path):
    d = {}
    with open(path) as f:
        for line in f:
            (key, val) = line.split()
            d[int(key)] = val
    return d


def open_txt_as_list(path):
    l = []
    with open(path) as f:
        for line in f:
            (key, output_class, rest) = line.split("'", 2)
            l.append(output_class)
    return l


def open_txt_as_numpy(path):
    l = []
    with open(path) as f:
        for line in f:
            values = line.rstrip().split(",")
            l.append(np.array(values, dtype=np.float32))
    return l


def save_numpy_array(_np_data, _path, _name, _index=None):
    # add a 'best fit' line
    weights_path = os.path.join(_path, _name)

    if not os.path.exists(weights_path):
        os.makedirs(weights_path)
    if _index:
        np.save(os.path.join(weights_path, "%04d_" % (_index)) + _name, _np_data)
    else:
        np.save(weights_path, _np_data)


def xyxy2xywh(x):
    # Convert nx4 boxes from [x1, y1, x2, y2] to [x, y, w, h] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[2] - x[0]  # bottom right x
    y[3] = x[3] - x[1]  # bottom right y
    return y


def xywh2xyxy_1d(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return y

def xywh2topleft_bottomright(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return (int(y[0]), int(y[1])), (int(y[2]), int(y[3]))


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = np.empty(shape=x.shape)
    y[:, 0] = x[:, 0]  # - x[:, 2] / 2  # top left x
    y[:, 1] = x[:, 1]  # - x[:, 3] / 2  # top left y
    y[:, 2] = x[:, 0] + x[:, 2]  # bottom right x
    y[:, 3] = x[:, 1] + x[:, 3]  # bottom right y
    return y


def xyxy_abs_to_rel_bbox(boxes, im_width, im_height):
    boxes_xy = np.empty(shape=boxes.shape)
    boxes_xy[:, 0] = boxes[:, 0] / im_width
    boxes_xy[:, 1] = boxes[:, 1] / im_height
    boxes_xy[:, 2] = boxes[:, 2] / im_width
    boxes_xy[:, 3] = boxes[:, 3] / im_height
    return boxes_xy


def calculate_true_boxes(boxes, im_width, im_height, gts, dataset, labels, scores):
    if len(boxes) > 0:
        boxes_xy = xyxy_abs_to_rel_bbox(np.asarray(boxes, dtype=float), im_width, im_height)
    else:
        boxes_xy = np.empty(0)

    true_boxes = [entry['bbox'] for entry in gts]
    true_labels_list = [entry['category_id'] for entry in gts]

    true_boxes = xywh2xyxy(np.asarray(true_boxes))
    true_boxes_rel = xyxy_abs_to_rel_bbox(true_boxes, im_width, im_height)
    gt_labels = [dataset.labels[i] for i in true_labels_list]

    # Provide the box coordinates in [x1, y1, x2, y2] format and relative to the image size,
    # so 0<=x1,x2,y1, y2<=1
    tp_list, iou_list, conf_list, fp_list = per_image_eval_NC(boxes_xy, labels, scores, true_boxes_rel,
                                                              true_labels_list, dataset, 0.5)

    return tp_list, iou_list, conf_list, fp_list, gt_labels


from collections import defaultdict


def nested_dict(depth=1):
    if depth == 1:
        return defaultdict(list)
    elif depth == 2:
        return defaultdict(lambda: defaultdict(list))
    elif depth == 3:
        return defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    else:
        return defaultdict(nested_dict(4))


def per_image_eval_NC(det_boxes, det_labels, det_scores, true_boxes, true_labels, dataset, iou_threshold=0.5):
    """
    Calculates the necessary infos required for computing the NC per image for all objects in an image.
    :param det_boxes: numpy array of bounding box arrays in [x1,y1,x2,y2] format per image
    :param det_labels: numpy array of corresponding labels of detections per image
    :param det_scores: numpy array of corresponding confidences of detections per image
    :param true_boxes: numpy array of all GTs per image
    :param true_labels: list of corresponding true classes of GTs per image
    :param iou: IoU threshold for the evaluation of true positives, standard is 0.5
    :return: TP, IoU, conf and FP list needed for calculation of neural criticality
    """
    tp_list = np.zeros(shape=len(true_labels), dtype=int)
    iou_list = np.zeros(shape=len(true_labels), dtype=float)
    conf_list = np.zeros(shape=len(true_labels), dtype=float)
    fp_list = list()

    class_det = nested_dict(depth=2)
    class_annotations = nested_dict(depth=3)

    if len(det_scores) > 0:
        for label, box, score in zip(det_labels, det_boxes, det_scores):
            class_det[dataset.labels[label]]['image_ids'].append("Image1")
            class_det[dataset.labels[label]]['bbox'].append(box)
            class_det[dataset.labels[label]]['score'].append(score)

    for i, item in enumerate(true_labels):
        class_annotations[dataset.labels[item]]["Image1"]['bbox'].append(true_boxes[i])
        class_annotations[dataset.labels[item]]["Image1"]['det'].append(False)
        class_annotations[dataset.labels[item]]["Image1"]['orig_index'].append(i)

    # Iterate over classes in data
    for class_index, class_name in enumerate(dataset.labels):

        if class_name not in class_det.keys():
            continue

        class_gts = copy.deepcopy(class_annotations[class_name])

        if len(class_det[class_name]['image_ids']) == 0 and len(class_gts["Image1"]["bbox"]) == 0:
            # if both GTs and predictions are empty for this class, then no matching must be done
            continue
        elif len(class_det[class_name]['image_ids']) == 0 and len(class_gts["Image1"]["bbox"]) != 0:
            # if predictions are empty for this class, then no matching must be done
            continue
        elif len(class_det[class_name]['image_ids']) != 0 and len(class_gts["Image1"]["bbox"]) == 0:
            for index in range(len(class_det[class_name]['image_ids'])):
                fp_list.append(int(class_index))
            # if predictions are not empty for this class, but we have no GT of this class, then we have only FPs
        else:
            BB = np.array(class_det[class_name]['bbox'])
            confidence = np.array(class_det[class_name]['score'])

            # sort by confidence - @FixMe - they are most probably already sorted out
            sorted_ind = np.argsort(-confidence)

            BB = BB[tuple(sorted_ind), :]
            confs = confidence[sorted_ind]
            image_ids = [class_det[class_name]['image_ids'][x] for x in sorted_ind]

            # Iterate over all detections of the respective class
            for image_id, image_name in enumerate(image_ids):
                R = class_gts[image_name]
                bb = BB[image_id, :].astype(float)
                ovmax = -np.inf
                BBGT = np.asarray(R['bbox']).astype(float)
                if BBGT.size > 0:
                    # Compute iou
                    ixmin = np.maximum(BBGT[:, 0], bb[0])
                    iymin = np.maximum(BBGT[:, 1], bb[1])
                    ixmax = np.minimum(BBGT[:, 2], bb[2])
                    iymax = np.minimum(BBGT[:, 3], bb[3])
                    iw = np.maximum(ixmax - ixmin, 0.)
                    ih = np.maximum(iymax - iymin, 0.)
                    inters = iw * ih
                    uni = ((bb[2] - bb[0]) * (bb[3] - bb[1]) +
                           (BBGT[:, 2] - BBGT[:, 0]) *
                           (BBGT[:, 3] - BBGT[:, 1]) - inters)
                    overlaps = inters / uni
                    ovmax = np.max(overlaps)
                    jmax = np.argmax(overlaps)

                    if ovmax > iou_threshold:

                        if not R['det'][jmax]:
                            tp_list[R['orig_index'][jmax]] = 1
                            iou_list[R['orig_index'][jmax]] = ovmax
                            conf_list[R['orig_index'][jmax]] = confs[image_id]
                            R['det'][jmax] = 1
                        else:
                            fp_list.append(int(class_index))
                            # tp_list[R['orig_index'][jmax]] = 0
                            # iou_list[R['orig_index'][jmax]] = ovmax
                            # conf_list[R['orig_index'][jmax]] = confs[d]
                    else:
                        fp_list.append(int(class_index))
                        # if tp_list[R['orig_index'][jmax]] != 1 and confs[d] > conf_list[R['orig_index'][jmax]]:
                        #     tp_list[R['orig_index'][jmax]] = 0
                        #     iou_list[R['orig_index'][jmax]] = ovmax
                        #     conf_list[R['orig_index'][jmax]] = confs[d]
    # TBV - dtype=int
    return tp_list, iou_list, conf_list, np.asarray(fp_list)
