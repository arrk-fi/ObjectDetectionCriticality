import json
import os
import copy

in_file = open("train_old.json", "r")
train_json = json.load(in_file)
in_file.close()

train_dataset = copy.deepcopy(train_json)
train_img = list()
for item in train_json["images"]:
    filename, file_extension = os.path.splitext(item["file_name"])
    new_item = item
    new_item["file_name"] = filename + ".jpg"
    train_img.append(new_item)
train_dataset["images"] = train_img

out_file = open("train.json", "w")
json.dump(train_dataset, out_file)
out_file.close()

in_file = open("val_old.json", "r")
val_json = json.load(in_file)
in_file.close()


val_dataset = copy.deepcopy(val_json)
val_img = list()
for index, item in enumerate(val_json["images"]):
    filename, file_extension = os.path.splitext(item["file_name"])
    new_item = item
    new_item["file_name"] = filename + ".jpg"
    val_img.append(new_item)
val_dataset["images"] = val_img 

out_file = open("val.json", "w")
json.dump(val_dataset, out_file)
out_file.close()
