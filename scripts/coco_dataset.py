import copy
import torch
import numpy as np
import os
from tqdm import tqdm
import torchvision.datasets as dset

import settings
import scripts.functions as functions
from scripts.general_dataset import GeneralDataSet

# https://pytorch.org/vision/stable/_modules/torchvision/datasets/coco.html
def rescale(image, target, newsize):
    # For object detection annotations, the format is "bbox" : [x,y,width,height]
    # Where:
    # x, y: the upper-left coordinates of the bounding box
    # width, height: the dimensions of your bounding box
    #
    width, height = image.size

    scale_width = float(newsize[0]) / float(width)
    # newsize[1] = int(height * scale_width)
    scale_height = float(newsize[1]) / float(height)

    resized_image = image.resize(tuple(newsize))

    resized_target = copy.deepcopy(target)
    for det_object in resized_target:
        det_object["bbox"][0] = det_object["bbox"][0] * scale_width
        det_object["bbox"][1] = det_object["bbox"][1] * scale_height
        det_object["bbox"][2] = det_object["bbox"][2] * scale_width
        det_object["bbox"][3] = det_object["bbox"][3] * scale_height

    return resized_image, resized_target


class CocoDataSet(GeneralDataSet):
    def __init__(self, path_to_data,
                 path_to_annotations,
                 path_to_labels,
                 allowed_classes=None,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600),
                 v_size=(800, 1052),
                 images_to_be_investigated=["all"],
                 path_to_dataset=None
                 ):

        super().__init__(name="coco", train_val_test="train", batch_size=1, path_to_dataset=path_to_dataset)

        self.name = "coco"
        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "coco_labels.json"))
        self.labels_numpy = np.asarray(self.labels)
        self.color_maps = np.random.uniform(0, 255, size=(self.num_classes, 3))
        self.filter_labels = allowed_classes
        if allowed_classes is None:
            self.filter_labels = self.labels
        self.device = device
        self.batch_size = batch_size
        self.h_size = h_size
        self.v_size = v_size
        self.images_to_be_investigated = images_to_be_investigated

        self.dataset = dset.CocoDetection(
            root=os.path.join(path_to_data, "coco", "train2017"),
            annFile=os.path.join(path_to_annotations, "coco", "annotations", "instances_train2017.json"))

        self.dataset_len = len(self.dataset.ids)
        self.class_indices = [str(self.labels.index(cls)) for cls in settings.global_analyzed_classes]

    @staticmethod
    def resize_image(image, target, size, batch_images, batch_targets, indices, index_batch, index, resize=True):
        resized_image, resized_target = rescale(image, target, size)
        image_numpy = np.asarray(resized_image, dtype=float)
        normalized = image_numpy / 255.0  # (image_numpy - np.min(image_numpy)) / (np.max(image_numpy) - np.min(image_numpy))
        batch_images.append(np.moveaxis(normalized, -1, 0))
        batch_targets.append(resized_target)
        indices.append(index)
        index_batch += 1
        return index_batch

    def dataset_iterator(self):
        batch_images = list()
        batch_targets = list()
        index = 0

        for image, target in self.dataset:
            image_numpy = np.asarray(image, dtype=float)
            normalized = image_numpy / 255.0
            batch_images.append(np.moveaxis(normalized, -1, 0))
            batch_targets.append(target)
            index = index + 1

            if index == self.batch_size:
                yield torch.Tensor(np.asarray(batch_images)).to(self.device), batch_targets
                # yield indices, np.asarray(batch_images), batch_targets
                batch_images = list()
                batch_targets = list()
                index = 0

    def rescale_dataset_iterator(self):
        index_v = 0
        index_h = 0
        batch_images_v = list()
        batch_images_h = list()
        batch_targets_v = list()
        batch_targets_h = list()
        batch_object_distance = list()
        indices_h = list()
        indices_v = list()

        for index, (image, target) in enumerate(self.dataset):

            if index_h == self.batch_size:
                yield indices_h, torch.Tensor(np.asarray(batch_images_h)).to(
                    self.device), batch_targets_h, batch_object_distance
                batch_images_h = list()
                batch_targets_h = list()
                indices_h = list()
                index_h = 0

            if index_v == self.batch_size:
                yield indices_v, torch.Tensor(np.asarray(batch_images_v)).to(
                    self.device), batch_targets_v, batch_object_distance
                batch_images_v = list()
                batch_targets_v = list()
                indices_v = list()
                index_v = 0

            labels_list = [self.labels[entry['category_id']] for entry in target]
            if len(set(self.filter_labels) & set(labels_list)) > 0:
                width, height = image.size
                if self.images_to_be_investigated == "all":
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)
                elif index in self.images_to_be_investigated:
                    if width > height:
                        index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                    indices_h, index_h, index)
                    else:
                        index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                    indices_v, index_v, index)

                batch_object_distance.append([0] * len(target))


class SmallDataSet(CocoDataSet):

    def __init__(self, path_to_data,
                 path_to_annotations,
                 path_to_labels,
                 allowed_classes=None,
                 device="cpu",
                 batch_size=16,
                 h_size=(800, 600),
                 v_size=(800, 1052),
                 images_to_be_investigated=["all"]):

        CocoDataSet.__init__(self, path_to_data,
                             path_to_annotations,
                             path_to_labels,
                             allowed_classes=allowed_classes,
                             device=device,
                             batch_size=batch_size,
                             h_size=h_size,
                             v_size=v_size,
                             images_to_be_investigated=images_to_be_investigated)

        self.ground_truths = list()
        self.images = list()
        self.images_cnt = list()
        for image_indices, image, gts in self.prepare_dataset():
            self.ground_truths.append(gts)
            self.images.append(image)
            self.images_cnt.append(image_indices)

        self.class_indices = [str(self.labels.index(cls)) for cls in settings.global_analyzed_classes]

    def dataset_iterator(self):
        for indices, images, gts in zip(self.images_cnt, self.images, self.ground_truths):
            yield indices, images, gts

    def prepare_dataset(self):
        index_v = 0
        index_h = 0
        batch_images_v = list()
        batch_images_h = list()
        batch_targets_v = list()
        batch_targets_h = list()
        indices_h = list()
        indices_v = list()

        print("Preparing dataset: ")
        counter = 0
        for index, (image, target) in tqdm(enumerate(self.coco_val)):

            if index_h == self.batch_size:
                yield indices_h, torch.Tensor(np.asarray(batch_images_h)).to(self.device), batch_targets_h
                batch_images_h = list()
                batch_targets_h = list()
                indices_h = list()
                index_h = 0

            if index_v == self.batch_size:
                yield indices_v, torch.Tensor(np.asarray(batch_images_v)).to(self.device), batch_targets_v
                batch_images_v = list()
                batch_targets_v = list()
                indices_v = list()
                index_v = 0

            # Assuring the only desired labels will be added in the batch
            labels_list = [self.labels[entry['category_id']] for entry in target]
            if len(set(self.filter_labels) & set(labels_list)) > 0:

                width, height = image.size
                # To choose the list of images you want to investigate
                if type(self.images_to_be_investigated) == list:
                    if self.images_to_be_investigated == "all":
                        if width > height:
                            index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                        indices_h, index_h, index)
                        else:
                            index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                        indices_v, index_v, index)
                    elif index in self.images_to_be_investigated:
                        if width > height:
                            index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                        indices_h, index_h, index)
                        else:
                            index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                        indices_v, index_v, index)

                # To choose the number of images you want to investigate
                else:
                    if counter < self.images_to_be_investigated:
                        if width > height:
                            index_h = self.resize_image(image, target, self.h_size, batch_images_h, batch_targets_h,
                                                        indices_h, index_h, index)
                        else:
                            index_v = self.resize_image(image, target, self.v_size, batch_images_v, batch_targets_v,
                                                        indices_v, index_v, index)
                        counter += 1
