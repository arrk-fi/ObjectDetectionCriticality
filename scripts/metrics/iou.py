import torch
import numpy as np
import torchvision.ops.boxes as bops


def get_intersection_points(box1, box2):
    # determine the coordinates of the intersection rectangle
    x_left = np.maximum(box1[0], box2[0])
    y_top = np.maximum(box1[1], box2[1])
    x_right = np.minimum(box1[2], box2[2])
    y_bottom = np.minimum(box1[3], box2[3])

    if x_right < x_left or y_bottom < y_top:
        return (0, 0), (0, 0)
    else:
        return (x_left, y_top), (x_right, y_bottom)


def intersection_over_union_tensor(boxes_preds, boxes_labels):
    box1 = torch.from_numpy(boxes_preds)
    box2 = torch.from_numpy(boxes_labels)
    iou = bops.box_iou(box1, box2)
    return iou


def intersection_over_union(boxes_preds, boxes_labels, box_format="midpoint"):
    """
    Calculates intersection over union

    Parameters:
        boxes_preds (tensor): Predictions of Bounding Boxes (BATCH_SIZE, 4)
        boxes_labels (tensor): Correct Labels of Boxes (BATCH_SIZE, 4)
        box_format (str): midpoint/corners, if boxes (x,y,w,h) or (x1,y1,x2,y2)

    Returns:
        tensor: Intersection over union for all examples
    """

    # Slicing idx:idx+1 in order to keep tensor dimensionality
    # Doing ... in indexing if there would be additional dimensions
    # Like for Yolo algorithm which would have (N, S, S, 4) in shape
    if box_format == "midpoint":
        box1_x1 = boxes_preds[..., 0:1] - boxes_preds[..., 2:3] / 2
        box1_y1 = boxes_preds[..., 1:2] - boxes_preds[..., 3:4] / 2
        box1_x2 = boxes_preds[..., 0:1] + boxes_preds[..., 2:3] / 2
        box1_y2 = boxes_preds[..., 1:2] + boxes_preds[..., 3:4] / 2
        box2_x1 = boxes_labels[..., 0:1] - boxes_labels[..., 2:3] / 2
        box2_y1 = boxes_labels[..., 1:2] - boxes_labels[..., 3:4] / 2
        box2_x2 = boxes_labels[..., 0:1] + boxes_labels[..., 2:3] / 2
        box2_y2 = boxes_labels[..., 1:2] + boxes_labels[..., 3:4] / 2

    elif box_format == "corners":
        box1_x1 = boxes_preds[..., 0:1]
        box1_y1 = boxes_preds[..., 1:2]
        box1_x2 = boxes_preds[..., 2:3]
        box1_y2 = boxes_preds[..., 3:4]
        box2_x1 = boxes_labels[..., 0:1]
        box2_y1 = boxes_labels[..., 1:2]
        box2_x2 = boxes_labels[..., 2:3]
        box2_y2 = boxes_labels[..., 3:4]

    #x1 = torch.max(box1_x1, box2_x1)
    x1 = np.maximum(box1_x1, box2_x1)
    #y1 = torch.max(box1_y1, box2_y1)
    y1 = np.maximum(box1_y1, box2_y1)
    #x2 = torch.min(box1_x2, box2_x2)
    x2 = np.minimum(box1_x2, box2_x2)
    #y2 = torch.min(box1_y2, box2_y2)
    y2 = np.minimum(box1_y2, box2_y2)

    # Need clamp(0) in case they do not intersect, then we want intersection to be 0
    #intersection = (x2 - x1).clamp(0) * (y2 - y1).clamp(0)
    intersection = (x2 - x1).clip(0.0) * (y2 - y1).clip(0.0)
    #box1_area = abs((box1_x2 - box1_x1) * (box1_y2 - box1_y1))
    box1_area = np.abs((box1_x2 - box1_x1) * (box1_y2 - box1_y1))
    #box2_area = abs((box2_x2 - box2_x1) * (box2_y2 - box2_y1))
    box2_area = np.abs((box2_x2 - box2_x1) * (box2_y2 - box2_y1))

    return intersection / (box1_area + box2_area - intersection + 1e-6)


def intersection_over_union_new(boxes_preds, boxes_labels, box_format="midpoint"):
    """
    Calculates intersection over union

    Parameters:
        boxes_preds (tensor): Predictions of Bounding Boxes (BATCH_SIZE, 4)
        boxes_labels (tensor): Correct Labels of Boxes (BATCH_SIZE, 4)
        box_format (str): midpoint/corners, if boxes (x,y,w,h) or (x1,y1,x2,y2)

    Returns:
        tensor: Intersection over union for all examples
    """

    # Slicing idx:idx+1 in order to keep tensor dimensionality
    # Doing ... in indexing if there would be additional dimensions
    # Like for Yolo algorithm which would have (N, S, S, 4) in shape
    if box_format == "midpoint":
        box1_x1 = boxes_preds[..., 0:1] - boxes_preds[..., 2:3] / 2
        box1_y1 = boxes_preds[..., 1:2] - boxes_preds[..., 3:4] / 2
        box1_x2 = boxes_preds[..., 0:1] + boxes_preds[..., 2:3] / 2
        box1_y2 = boxes_preds[..., 1:2] + boxes_preds[..., 3:4] / 2
        box2_x1 = boxes_labels[..., 0:1] - boxes_labels[..., 2:3] / 2
        box2_y1 = boxes_labels[..., 1:2] - boxes_labels[..., 3:4] / 2
        box2_x2 = boxes_labels[..., 0:1] + boxes_labels[..., 2:3] / 2
        box2_y2 = boxes_labels[..., 1:2] + boxes_labels[..., 3:4] / 2

    elif box_format == "corners":
        box1_x1 = boxes_preds[..., 0:1]
        box1_y1 = boxes_preds[..., 1:2]
        box1_x2 = boxes_preds[..., 2:3]
        box1_y2 = boxes_preds[..., 3:4]
        box2_x1 = boxes_labels[..., 0:1]
        box2_y1 = boxes_labels[..., 1:2]
        box2_x2 = boxes_labels[..., 2:3]
        box2_y2 = boxes_labels[..., 3:4]

    assert box1_x1 < box1_x2
    assert box1_y1 < box1_y2
    assert box2_x1 < box2_x2
    assert box2_y1 < box2_y2

    # determine the coordinates of the intersection rectangle
    x_left = np.maximum(box1_x1, box2_x1)
    y_top = np.maximum(box1_y1, box2_y1)
    x_right = np.minimum(box1_x2, box2_x2)
    y_bottom = np.minimum(box1_y2, box2_y2)

    if x_right < x_left or y_bottom < y_top:
        return 0.0

    # The intersection of two axis-aligned bounding boxes is always an
    # axis-aligned bounding box
    intersection_area = (x_right - x_left) * (y_bottom - y_top)

    # compute the area of both AABBs
    bb1_area = (box1_x2 - box1_x1) * (box1_y2 - box1_y1)
    bb2_area = (box2_x2 - box2_x1) * (box2_y2 - box2_y1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
    assert iou >= 0.0
    assert iou <= 1.0
    return iou