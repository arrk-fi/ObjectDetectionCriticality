import json
import collections
import os
import numpy as np
from tqdm import tqdm
from PIL import Image
import copy
import torch
import shutil

import scripts.functions as functions
from scripts.general_dataset import GeneralDataSet


def nested_dict():
    return collections.defaultdict(nested_dict)


def get_image_size(_path):
    image = Image.open(_path)
    return image.size


def clip_bb_size(bb_coordinates, image_size):
    clipped_bb = [0.0, 0.0, 0.0, 0.0]
    clipped_bb[0] = np.clip(bb_coordinates[0], 0.0, image_size[0])
    clipped_bb[1] = np.clip(bb_coordinates[1], 0.0, image_size[1])
    clipped_bb[2] = np.clip(bb_coordinates[2], 0.0, image_size[0])
    clipped_bb[3] = np.clip(bb_coordinates[3], 0.0, image_size[1])
    return clipped_bb


def calc_dist_to_corner(w, l, cx, cy, sig):
    # heading of object
    sigma = sig * 180.0 / np.pi
    alpha = np.arctan(np.abs(w / l)) * 180.0 / np.pi  # w / l
    beta = np.arctan(np.abs(cx / cy)) * 180.0 / np.pi  # cx / cy
    # square root of (x^2 and y^2 distance)
    dist_to_center = np.sqrt(cx * cx + cy * cy)
    if beta > (alpha + sigma):  # rotated anticlockwise
        center_to_corner = np.arccos((90.0 - beta) / 180.0 * np.pi) * w / 2.0
    else:
        center_to_corner = np.arccos((alpha - (sigma - beta)) / 180.0 * np.pi) * l / 2.0
    return dist_to_center - center_to_corner

def convert_images(destination, images):
    for image in tqdm(images):
        filename, file_extension = os.path.splitext(image)
        filename = filename.split(os.sep)[-1]
        if file_extension.lower() != ".jpg":
            if file_extension.lower() == ".png":
                img = Image.open(image)
                rgb_im = img.convert('RGB')
                rgb_im.save(os.path.join(destination, filename + ".jpg"), quality=100)
            elif file_extension.lower() == ".jpeg":
                shutil.copy(image,
                            os.path.join(destination, filename + ".jpg"))
        else:
            shutil.copy(image, destination)

def change_path(dataset, images_id, val_dataset):
    for item in dataset["images"]:
        if item["id"] in images_id:
            new_item = item
            new_item["file_name"] = new_item["file_name"].split(os.sep)[-1]
            val_dataset["images"].append(new_item)

    for item in dataset["annotations"]:
        if item["image_id"] in images_id:
            val_dataset["annotations"].append(item)

def export_to_coco_f(path_to_data, path_to_store, train_test_val, dataset, just_annotation=True):
    print("Saving annotations")
    if train_test_val == "train_val":
        if not os.path.exists(os.path.join(path_to_store, "data_coco_format")):
            os.makedirs(os.path.join(path_to_store, "data_coco_format"))
            os.makedirs(os.path.join(path_to_store, "data_coco_format", "train"))
            os.makedirs(os.path.join(path_to_store, "data_coco_format", "val"))
    else:
        if not os.path.exists(os.path.join(path_to_store, "data_coco_format")):
            os.makedirs(os.path.join(path_to_store, "data_coco_format"))
            os.makedirs(os.path.join(path_to_store, "data_coco_format", train_test_val))

    if not just_annotation:
        print("Copying images - can take some time")
        images = np.asarray([image_item["file_name"] for image_item in dataset["images"]])
        images_ids = np.asarray([image_item["id"] for image_item in dataset["images"]])
        print("Complete dataset has: {}".format(images.shape[0]))

        train_dataset = collections.defaultdict(list)

        # if the dataset wasn't split into train and val
        if train_test_val == "train_val":
            val_dataset = collections.defaultdict(list)

            indices = np.arange(images.shape[0])
            np.random.shuffle(indices)
            eighty_percent = int(images.shape[0] * 0.8)

            train_images = images[indices[:eighty_percent]]
            train_images_id = images_ids[indices[:eighty_percent]]
            print(" copying train part {}".format(train_images_id.shape[0]))

            destination = os.path.join(path_to_store, "data_coco_format", "train")
            convert_images(destination, train_images)
            change_path(dataset, train_images_id, train_dataset)
            train_dataset["categories"] = copy.deepcopy(dataset["categories"])

            val_images = images[indices[eighty_percent:]]
            val_images_id = images_ids[indices[eighty_percent:]]
            print(" copying validation part {}".format(val_images_id.shape[0]))

            destination = os.path.join(path_to_store, "data_coco_format", "val")
            convert_images(destination, val_images)
            change_path(dataset, val_images_id, val_dataset)
            val_dataset["categories"] = copy.deepcopy(dataset["categories"])

        # when dataset is split into train and val
        else:
            destination = os.path.join(path_to_store, "data_coco_format", train_test_val)
            for image_item in tqdm(dataset["images"]):
                path_to_image = image_item["file_name"]
                shutil.copy(path_to_image, destination)
                image_item["file_name"] = image_item["file_name"].split(os.sep)[-1]

            train_dataset = dataset

    else:
        if train_test_val == "train_val":
            train_dataset = dataset * 0.8  # @FixMe
            val_dataset = dataset
        else:
            train_dataset = dataset

    if train_test_val == "train_val":
        with open(os.path.join(path_to_store, "data_coco_format", "train.json"), "w") as annotations:
            json.dump(train_dataset, annotations)
        with open(os.path.join(path_to_store, "data_coco_format", "val.json"), "w") as annotations:
            json.dump(val_dataset, annotations)

    else:
        with open(os.path.join(path_to_store, "data_coco_format", train_test_val + ".json"), "w") as annotations:
            json.dump(train_dataset, annotations)


class KittiDataSet(GeneralDataSet):
    """
    Kitti Dataset
    link: http://www.cvlibs.net/datasets/kitti/
    link: https://github.com/bostondiditeam/kitti/blob/master/resources/devkit_object/readme.txt
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train_val",
                 import_coco_format=False
                 ):
        super().__init__("Kitti", train_test_val, batch_size, path_to_dataset)

        def compute3DBB(l, w, h, rot_y, x, y, z):
            R = np.asarray([[np.cos(rot_y), 0, np.sin(rot_y)],
                            [0, 1, 0],
                            [-np.sin(rot_y), 0, np.cos(rot_y)]])

            # 3D bounding box corners
            x_corners = [l / 2, l / 2, -l / 2, -l / 2, l / 2, l / 2, -l / 2, -l / 2]
            y_corners = [0, 0, 0, 0, -h, -h, -h, -h]
            z_corners = [w / 2, -w / 2, -w / 2, w / 2, w / 2, -w / 2, -w / 2, w / 2]

            # rotate and translate 3D bounding box
            corners_3D = np.dot(R, np.asarray([x_corners, y_corners, z_corners]))
            corners_3D[0, :] = corners_3D[0, :] + x
            corners_3D[1, :] = corners_3D[1, :] + y
            corners_3D[2, :] = corners_3D[2, :] + z
            return corners_3D

        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "kitti_labels.json"))
        self.labels_numpy = np.asarray(self.labels)
        self.image_size = [1242, 375]
        self.device = device

        if import_coco_format: # ToDo
            with open(os.path.join(path_to_dataset, self.name + "_coco_format", "annotations", train_test_val + ".json"), "r") as dataset_file:
                self.dataset = json.load(dataset_file)
        else:
            path_to_data = os.path.join(path_to_dataset, self.name, train_test_val)
            image_dir = os.path.join(path_to_data, "image_2")
            label_dir = os.path.join(path_to_data, "label_2")

            files = sorted(os.listdir(image_dir))
            image_files = [file for file in files if file.split(".")[1].lower() == "png"]
            files = sorted(os.listdir(label_dir))
            labels_files = [file for file in files if file.split(".")[1].lower() == "txt"]
            # lidar_dirs = [os.path.join(dire, "lidar", "cam_front_center") for dire in dirs]
            ann_id = 0
            for image_id, (image, label) in enumerate(zip(image_files, labels_files)):
                self.image_size = get_image_size(os.path.join(image_dir, image))
                text_file = open(os.path.join(label_dir, label))
                # Example:
                # type trunc occ alpha [ bbox                  ][hei  wid   len] [ x,  y,    z ] [rot_y]
                #   0     1  2    3     4      5      6      7    8    9     10    11  12    13    14
                # Truck 0.00 0 -1.57 599.41 156.40 629.75 189.25 2.85 2.63 12.34 0.47 1.49 69.44 -1.56
                # https://github.com/bostondiditeam/kitti/blob/master/resources/devkit_object/readme.txt
                annotation = list()
                ann_file = text_file.readlines()
                for line in ann_file:
                    items = line.split(" ")

                    item = nested_dict()
                    # item["segmentation"] = [[0.0]]
                    item["iscrowd"] = 0
                    item["id"] = ann_id
                    item["class"] = items[0]
                    item["image_id"] = image_id
                    clipped_bb = clip_bb_size([float(items[4]), float(items[5]), float(items[6]), float(items[7])],
                                              self.image_size)
                    # xywh
                    item["bbox"] = functions.xyxy2xywh(np.asarray(clipped_bb)).tolist()
                    item["category_id"] = self.labels.index(items[0])
                    item["area"] = float(item["bbox"][2] + item["bbox"][3])

                    # (l, w, h, rot_y, x,  y, z)
                    bb_3d = compute3DBB(float(items[10]), float(items[9]), float(items[8]), float(items[14]),
                                        float(items[11]), float(items[12]), float(items[13]))
                    item["distance"] = float(np.min([bb_3d[2][0], bb_3d[2][1], bb_3d[2][2], bb_3d[2][3]]))

                    annotation.append(item)

                    if export_to_coco:
                        self.dataset["annotations"].append(item)

                    ann_id += 1

                text_file.close()

                if not export_to_coco:
                    self.dataset[image].append((os.path.join(image_dir, image), annotation))
                else:
                    image_item = nested_dict()
                    image_item["file_name"] = os.path.join(image_dir, image)
                    image_item["width"], image_item["height"] = get_image_size(os.path.join(image_dir, image))
                    image_item["id"] = image_id
                    self.dataset["images"].append(image_item)
                # lidar_front_center['distance'],
                # lidar_front_center['depth']))

            self.dataset_len = len(list(self.dataset.keys()))
            self.batch_size = batch_size

            if export_to_coco:
                categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
                self.dataset["categories"] = categories
                export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=False)


class AudiDataSet(GeneralDataSet):
    """
    Audi Dataset
    link: http://www.cvlibs.net/datasets/kitti/
    link: https://www.a2d2.audi/a2d2/en/tutorial.html
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train_val",
                 import_coco_format=False
                 ):

        super().__init__("A2D2", train_test_val, batch_size, path_to_dataset)

        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "a2d2_labels.json"))
        self.labels_numpy = np.asarray(self.labels)
        with open(os.path.join(path_to_dataset, self.name, "train_val", "sensor_config.json"), 'r') as f:
            config = json.load(f)
        self.image_size = config["cameras"]["front_center"]["Resolution"]  # 1920x1208
        self.device = device

        if import_coco_format:
            with open(os.path.join(path_to_dataset, self.name, "dataset_coco_format", train_test_val + ".json"), "r") as dataset_file:
                self.dataset = json.load(dataset_file)
                self.annotations = collections.defaultdict(list)
                for ann in self.dataset["annotations"]:
                    self.annotations[ann["image_id"]].append(ann)
                self.dataset_len = len(self.dataset["images"])
        else:

            path_to_data = os.path.join(path_to_dataset, self.name, train_test_val, "camera_lidar_semantic_bboxes")
            dir_files = os.listdir(path_to_data)
            full_paths = map(lambda name: os.path.join(path_to_data, name), dir_files)

            dirs = [file for file in full_paths if os.path.isdir(file)]
            images_dirs = [os.path.join(dire, "camera", "cam_front_center") for dire in dirs]
            labels_dirs = [os.path.join(dire, "label3D", "cam_front_center") for dire in dirs]
            lidar_dirs = [os.path.join(dire, "lidar", "cam_front_center") for dire in dirs]

            print("Searching folders and searching for images")
            ann_id = 0
            image_id = 0
            for image_dir, label_dir, lidar_dir in tqdm(zip(images_dirs, labels_dirs, lidar_dirs)):
                files = sorted(os.listdir(image_dir))
                image_files = [file for file in files if file.split(".")[1].lower() == "png"]
                labels_files = sorted(os.listdir(label_dir))
                files = sorted(os.listdir(lidar_dir))
                lidar_files = [file for file in files if file.split(".")[1].lower() == "npz"]

                for image, label, lidar in zip(image_files, labels_files, lidar_files):

                    json_file = open(os.path.join(label_dir, label))
                    annotation = json.load(json_file)

                    annotation_list = list()

                    for key, items in annotation.items():
                        item = nested_dict()

                        # item["segmentation"] = [[0.0]]
                        item["image_id"] = image_id
                        item["id"] = ann_id
                        item["class"] = items["class"]
                        item["category_id"] = self.labels.index(items["class"])
                        # bbox_read['top'] = bboxes[bbox]['2d_bbox'][0]
                        # bbox_read['left'] = bboxes[bbox]['2d_bbox'][1]
                        # bbox_read['bottom'] = bboxes[bbox]['2d_bbox'][2]
                        # bbox_read['right'] = bboxes[bbox]['2d_bbox'][3]
                        clipped_bb = clip_bb_size(items["2d_bbox"], self.image_size)

                        item["bbox"] = functions.xyxy2xywh(np.asarray(clipped_bb)).tolist()
                        item["area"] = float(item["bbox"][2] * item["bbox"][3])
                        item["iscrowd"] = 0
                        # ['azimuth', 'row', 'lidar_id', 'depth', 'reflectance', 'col', 'points', 'timestamp', 'distance']
                        # lidar_front_center = np.load(os.path.join(lidar_dir, lidar))
                        # [1][0] - z distance bottom front left
                        # [2][0] - z distance bottom front right
                        # [6][0] - z distance top front left
                        # [7][0] - z distance top front right
                        item["distance"] = float(np.min(np.asarray(items['3d_points'])[:, 0]))

                        annotation_list.append(item)
                        if export_to_coco:
                            self.dataset["annotations"].append(item)

                        ann_id += 1

                    if not export_to_coco:
                        self.dataset[image].append((os.path.join(image_dir, image), annotation_list))
                    else:
                        image_item = nested_dict()
                        image_item["file_name"] = os.path.join(image_dir, image)
                        image_item["width"], image_item["height"] = get_image_size(os.path.join(image_dir, image))
                        image_item["id"] = image_id
                        self.dataset["images"].append(image_item)

                    image_id += 1

            self.dataset_len = len(list(self.dataset.keys()))
            self.batch_size = batch_size

            if export_to_coco:
                categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
                self.dataset["categories"] = categories
                export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=False)


class NuScenesDataSet(GeneralDataSet):
    """
    NuScenes
    link: https://www.nuscenes.org/nuimages#download
    link: https://github.com/nutonomy/nuscenes-devkit
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train_val",
                 import_coco_format=False
                 ):

        super().__init__("nuScenes", train_test_val, batch_size, path_to_dataset)

        # https://github.com/nutonomy/nuscenes-devkit/issues/773
        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "nuscenes_labels.json"))
        self.labels_numpy = np.asarray(self.labels)
        self.image_size = (1600, 900)
        self.device = device

        if import_coco_format:
            with open(os.path.join(path_to_dataset, self.name, train_test_val + "_coco_format", "annotations", train_test_val + ".json"), "r") as dataset_file:
                self.dataset = json.load(dataset_file)
                self.annotations = collections.defaultdict(list)
                for ann in self.dataset["annotations"]:
                    self.annotations[ann["image_id"]].append(ann)
                self.dataset_len = len(self.dataset["images"])
        else:

            from nuscenes.nuscenes import NuScenes
            from scripts.nuscenes_utils import get_2d_boxes

            path_to_data = os.path.join(path_to_dataset, self.name)

            nusc = NuScenes(version=train_test_val, dataroot=path_to_data, verbose=True)

            sensor = 'CAM_FRONT'
            sample_data_camera_tokens = [s['token'] for s in nusc.sample_data if
                                         s['sensor_modality'] == 'camera' and s['channel'] == sensor and s['is_key_frame']]

            # sample_data_camera_tokens = [s['token'] for s in nusc.sample_data if sensor in s["filename"] and s['is_key_frame'] == True]

            # Loop through the records and apply the re-projection algorithm.
            image_id = 0
            ann_id = 0
            for token in tqdm(sample_data_camera_tokens):
                # list_of_visibility - the higher number the higher visibility
                reprojection_records = get_2d_boxes(token, ["", "1", "2", "3", "4"], nusc)

                annotation_list = list()
                file_name = None
                for items in reprojection_records:
                    item = nested_dict()

                    item["image_id"] = image_id
                    item["id"] = ann_id
                    # @fixMe: why do they have "other vehicle", "other person" in classes
                    item["class"] = items["category_name"]

                    item["category_id"] = self.labels.index(item["class"])
                    # bbox_read['top'] = items['box2d']["x1"]
                    # bbox_read['left'] = items['box2d']["y1"]
                    # bbox_read['bottom'] = items['box2d']["x2"]
                    # bbox_read['right'] = items['box2d']["y2"]
                    xyxy = [items["bbox_corners"][0], items["bbox_corners"][1], items["bbox_corners"][2],
                            items["bbox_corners"][3]]
                    clipped_bb = clip_bb_size(xyxy, self.image_size)
                    item["bbox"] = functions.xyxy2xywh(np.asarray(clipped_bb)).tolist()
                    item["area"] = float(item["bbox"][2] * item["bbox"][3])
                    item["iscrowd"] = 0

                    item["distance"] = items["distance"]

                    file_name = items["filename"]
                    annotation_list.append(item)
                    if export_to_coco:
                        self.dataset["annotations"].append(item)

                    ann_id += 1

                if file_name is not None:
                    if not export_to_coco:
                        self.dataset[file_name].append((os.path.join(path_to_data, file_name), annotation_list))
                    else:
                        image_item = nested_dict()
                        image_item["file_name"] = file_name
                        image_item["width"], image_item["height"] = get_image_size(os.path.join(path_to_data, file_name))
                        image_item["id"] = image_id
                        self.dataset["images"].append(image_item)

                image_id += 1

            self.dataset_len = len(list(self.dataset.keys()))

            self.batch_size = batch_size

            if export_to_coco:
                categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
                self.dataset["categories"] = categories
                export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=False)


class LyftLevel5DataSet(GeneralDataSet):
    """
    Lyft Level5
    link: https://level-5.global/data/
    link: https://woven-planet.github.io/l5kit/
    link: https://github.com/lyft/nuscenes-devkit/blob/master/README.md
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train_val",
                 import_coco_format=False
                 ):

        super().__init__("LyftLevel5", train_test_val, batch_size, path_to_dataset)

        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "lyftlevel5_labels.json"))
        self.labels_numpy = np.asarray(self.labels)
        self.image_size = [1224, 1024]
        self.device = device

        from nuscenes.nuscenes import NuScenes
        from scripts.nuscenes_utils import get_2d_boxes

        path_to_data = os.path.join(path_to_dataset, self.name, train_test_val)

        nusc = NuScenes(version=train_test_val, dataroot=path_to_data, verbose=True)

        sensor = 'CAM_FRONT'
        sample_data_camera_tokens = [s['token'] for s in nusc.sample_data if
                                     s['sensor_modality'] == 'camera' and s['channel'] == sensor and s['is_key_frame']]

        # Loop through the records and apply the re-projection algorithm.
        image_id = 0
        ann_id = 0
        for token in tqdm(sample_data_camera_tokens):
            reprojection_records = get_2d_boxes(token, ["", "1", "2", "3", "4"], nusc)

            annotation_list = list()
            file_name = None
            for items in reprojection_records:
                item = nested_dict()

                item["image_id"] = image_id
                item["id"] = ann_id
                # @fixMe: why do they have "other vehicle", "other person" in classes
                item["class"] = items["category_name"]

                item["category_id"] = self.labels.index(item["class"])
                # bbox_read['top'] = items['box2d']["x1"]
                # bbox_read['left'] = items['box2d']["y1"]
                # bbox_read['bottom'] = items['box2d']["x2"]
                # bbox_read['right'] = items['box2d']["y2"]
                xyxy = [items["bbox_corners"][0], items["bbox_corners"][1], items["bbox_corners"][2],
                        items["bbox_corners"][3]]
                clipped_bb = clip_bb_size(xyxy, self.image_size)
                item["bbox"] = functions.xyxy2xywh(np.asarray(clipped_bb)).tolist()
                item["area"] = float(item["bbox"][2] * item["bbox"][3])
                item["iscrowd"] = 0

                item["distance"] = items["distance"]

                file_name = items["filename"]
                annotation_list.append(item)
                if export_to_coco:
                    self.dataset["annotations"].append(item)

                ann_id += 1

            if file_name is not None:
                if not export_to_coco:
                    self.dataset[file_name].append((os.path.join(path_to_data, file_name), annotation_list))
                else:
                    image_item = nested_dict()
                    image_item["file_name"] = file_name
                    image_item["width"], image_item["height"] = get_image_size(os.path.join(path_to_data, file_name))
                    image_item["id"] = image_id
                    self.dataset["images"].append(image_item)

            image_id += 1

        self.dataset_len = len(list(self.dataset.keys()))

        self.batch_size = batch_size

        if export_to_coco:
            categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
            self.dataset["categories"] = categories
            export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=False)


# https://pypi.org/search/?q=waymo-open-dataset
import tensorflow as tf

if not tf.executing_eagerly():
    tf.compat.v1.enable_eager_execution()

# from waymo_open_dataset import dataset_pb2 as open_dataset
# from waymo_open_dataset.camera.ops import py_camera_model_ops
# from waymo_open_dataset.utils import box_utils

from matplotlib import patches
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')

class WaymoDataSet(GeneralDataSet):
    """
    Waymo Dataset
    link: https://waymo.com/open/
    link: https://github.com/waymo-research/waymo-open-dataset/tree/master/docs
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train",
                 import_coco_format = False
                 ):
        super().__init__("Waymo", train_test_val, batch_size, path_to_dataset)

        def draw_2d_box(plt, u, v, color, label, linewidth=2):
            """Draws 2D bounding boxes as rectangles onto the given axis."""
            rect = patches.Rectangle(
                xy=(u.min(), v.min()),
                width=u.max() - u.min(),
                height=v.max() - v.min(),
                linewidth=linewidth,
                edgecolor=color,
                facecolor=list(color) + [0.1])  # Add alpha for opacity
            plt.add_patch(rect)
            plt.text(u.min() + u.max() - u.min() + 24, v.min(), label)

        def show_camera_image(camera_image):
            """Display the given camera image."""
            fig, ax = plt.subplots()
            plt.imshow(tf.image.decode_jpeg(camera_image.image))
            plt.title(open_dataset.CameraName.Name.Name(camera_image.name))
            plt.grid(False)
            plt.axis('off')
            return ax

        def project_vehicle_to_image(vehicle_pose, calibration, points):
            """Projects from vehicle coordinate system to image with global shutter.

            Arguments:
              vehicle_pose: Vehicle pose transform from vehicle into world coordinate
                system.
              calibration: Camera calibration details (including intrinsics/extrinsics).
              points: Points to project of shape [N, 3] in vehicle coordinate system.

            Returns:
              Array of shape [N, 3], with the latter dimension composed of (u, v, ok).
            """
            # Transform points from vehicle to world coordinate system (can be
            # vectorized).
            pose_matrix = np.array(vehicle_pose.transform).reshape(4, 4)
            world_points = np.zeros_like(points)
            for i, point in enumerate(points):
                cx, cy, cz, _ = np.matmul(pose_matrix, [*point, 1])
                world_points[i] = (cx, cy, cz)

            # Populate camera image metadata. Velocity and latency stats are filled with
            # zeroes.
            extrinsic = tf.reshape(
                tf.constant(list(calibration.extrinsic.transform), dtype=tf.float32),
                [4, 4])
            intrinsic = tf.constant(list(calibration.intrinsic), dtype=tf.float32)
            metadata = tf.constant([
                calibration.width,
                calibration.height,
                open_dataset.CameraCalibration.GLOBAL_SHUTTER,
            ],
                dtype=tf.int32)
            camera_image_metadata = list(vehicle_pose.transform) + [0.0] * 10

            # Perform projection and return projected image coordinates (u, v, ok).
            return py_camera_model_ops.world_to_image(extrinsic, intrinsic, metadata,
                                                      camera_image_metadata,
                                                      world_points).numpy()

        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "waymo_labels.json"))
        self.labels_numpy = np.asarray(self.labels)

        self.device = device

        path_to_data = os.path.join(path_to_dataset, self.name, train_test_val)

        if import_coco_format:
            with open(os.path.join(path_to_dataset, self.name, train_test_val + "_coco_format", "annotations", train_test_val + ".json"), "r") as dataset_file:
                self.dataset = json.load(dataset_file)
                self.annotations = collections.defaultdict(list)
                for ann in self.dataset["annotations"]:
                    self.annotations[ann["image_id"]].append(ann)
                self.dataset_len = len(self.dataset["images"])
        else:
            dir_files = os.listdir(path_to_data)
            self.full_paths = [os.path.join(path_to_dataset, self.name, train_test_val, file) for file in dir_files]
            self.frame = open_dataset.Frame()

            if export_to_coco:
                print("Saving annotations")
                path_coco_train_export = os.path.join(path_to_data, "coco_format", train_test_val)
                if not os.path.exists(path_coco_train_export):
                    os.makedirs(os.path.join(path_to_data, "coco_format"))
                    os.makedirs(path_coco_train_export)
                    os.makedirs(os.path.join(path_to_data, "coco_format", "annotations"))
    
            if os.path.isfile(os.path.join(path_to_dataset, self.name, train_test_val + "_original_format_annotation.json")):
                with open(os.path.join(path_to_dataset, self.name, train_test_val + "_original_format_annotation.json"), "r") as original_annotations:
                    self.dataset = json.load(original_annotations)

            else:
                ann_id = 0
                image_id = 0

                for tfrecord in tqdm(self.full_paths[:1]):
                    dataset = tf.data.TFRecordDataset(tfrecord, compression_type='')
                    dataset_iter = list(dataset.as_numpy_iterator())

                    for data in dataset_iter:

                        self.frame.ParseFromString(data)

                        for image in self.frame.images:
                            # To filter only front cam images
                            if image.name == 1:

                                ax = show_camera_image(image)

                                picture = tf.image.decode_jpeg(image.image).numpy()

                                file_name = str(self.frame.timestamp_micros) + "_" + str(image.name) + ".jpg"
                                if export_to_coco:
                                    im = Image.fromarray(picture)
                                    #im.save(os.path.join(path_coco_train_export, file_name))

                                calibration = next(cc for cc in self.frame.context.camera_calibrations
                                                   if cc.name == image.name)

                                annotation_list = list()

                                for label in self.frame.laser_labels:
                                    lid_vis = label.num_top_lidar_points_in_box > 5
                                    if lid_vis:
                                        box = label.camera_synced_box

                                        FILTER_AVAILABLE = any(
                                            [label.num_top_lidar_points_in_box > 0 for label in self.frame.laser_labels])

                                        if not box.ByteSize():
                                            continue  # Filter out labels that do not have a camera_synced_box.
                                        if (FILTER_AVAILABLE and not label.num_top_lidar_points_in_box) or (
                                                not FILTER_AVAILABLE and not label.num_lidar_points_in_box):
                                            continue  # Filter out likely occluded objects.

                                        # Retrieve upright 3D box corners.
                                        box_coords = np.array([[
                                            box.center_x, box.center_y, box.center_z, box.length, box.width,
                                            box.height, box.heading
                                        ]])
                                        corners = box_utils.get_upright_3d_box_corners(
                                            box_coords)[0].numpy()  # [8, 3]

                                        projected_corners = project_vehicle_to_image(self.frame.pose, calibration,
                                                                                     corners)
                                        u, v, ok = projected_corners.transpose()
                                        ok = ok.astype(bool)
                                        # Skip object if any corner projection failed. Note that this is very
                                        # strict and can lead to exclusion of some partially visible objects.
                                        if not all(ok):
                                            continue
                                        u = u[ok]
                                        v = v[ok]
                                        # Clip box to image bounds.
                                        u = np.clip(u, 0, calibration.width)
                                        v = np.clip(v, 0, calibration.height)

                                        if u.max() - u.min() == 0 or v.max() - v.min() == 0:
                                            continue

                                        x = int(u.min())
                                        y = int(v.min())
                                        width = int(u.max() - u.min())
                                        height = int(v.max() - v.min())
                                        xywh = [x, y, width, height]

                                        draw_2d_box(ax, u, v, (1.0, 1.0, 0.0), label.Type.Name(label.type))

                                        item = nested_dict()

                                        # item["segmentation"] = [[0.0]]
                                        item["iscrowd"] = 0
                                        item["image_id"] = image_id
                                        item["id"] = ann_id
                                        # @fixMe: why do they have "other vehicle", "other person" in classes
                                        item["class"] = label.Type.Name(label.type)

                                        item["category_id"] = label.type
                                        item["bbox"] = xywh
                                        item["area"] = float(xywh[2] * xywh[3])

                                        item["distance"] = np.sqrt(box.center_x * box.center_x + box.center_y * box.center_y)

                                        annotation_list.append(item)
                                        if export_to_coco:
                                            self.dataset["annotations"].append(item)

                                        ann_id += 1

                                if not export_to_coco:
                                    self.dataset[file_name].append(([calibration.width, calibration.height], annotation_list))
                                else:
                                    image_item = nested_dict()
                                    image_item["file_name"] = file_name
                                    image_item["width"], image_item["height"] = calibration.width, calibration.height
                                    image_item["id"] = image_id
                                    self.dataset["images"].append(image_item)

                                image_id += 1
                                file_name_pred = str(self.frame.timestamp_micros) + "_" + str(image.name) + "_pred.jpg"
                                plt.savefig(os.path.join(path_coco_train_export, file_name_pred), bbox_inches='tight')

                with open(os.path.join(path_to_dataset, "waymo", train_test_val + "_original_format_annotation.json"), "w") as annotations:
                    json.dump(self.dataset, annotations)

            self.dataset_len = len(list(self.dataset.keys()))
            self.batch_size = batch_size

            if export_to_coco:
                categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
                self.dataset["categories"] = categories
                export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=True)

    def label_iterator(self):
        batch_targets = list()
        batch_img_sizes = list()
        index = 0

        for key, entry in self.dataset.items():
            batch_img_sizes.append(entry[0][0])
            batch_targets.append(entry[0][1])
            index = index + 1

            if index == self.batch_size:
                yield batch_targets, batch_img_sizes
                batch_targets = list()
                batch_img_sizes = list()
                index = 0

    def dataset_iterator(self):
        batch_images = list()
        batch_targets = list()
        index = 0

        for tfrecord in tqdm(self.full_paths): # :5 - fifth does problems
            dataset = tf.data.TFRecordDataset(tfrecord, compression_type='')
            dataset_iter = dataset.as_numpy_iterator()

            for data in dataset_iter:

                self.frame.ParseFromString(data)

                for image in self.frame.images:
                    # To filter only front cam images
                    if image.name == 1:
                        image_np = tf.image.decode_jpeg(image.image).numpy() # copy deep copy?
                        file_name = str(self.frame.timestamp_micros) + "_" + str(image.name) + ".jpeg"
                        #if file_name in self.dataset.keys(): # do i have to check it?
                        batch_images.append(np.moveaxis(image_np, -1, 0))
                        batch_targets.append(self.dataset[file_name][0][1])
                        index = index + 1

                        if index == self.batch_size:
                            yield torch.Tensor(np.asarray(batch_images)).to(self.device), batch_targets
                            # yield indices, np.asarray(batch_images), batch_targets
                            batch_images = list()
                            batch_targets = list()
                            index = 0


class OnceDataSet(GeneralDataSet):
    """
    Once Dataset
    link: https://github.com/once-for-auto-driving/once_devkit
    link: https://once-for-auto-driving.github.io/documentation.html#data_annotation
    """

    def __init__(self, path_to_dataset,
                 path_to_annotations,
                 path_to_labels,
                 device="cpu",
                 batch_size=1,
                 h_size=(800, 600), v_size=(800, 1052),
                 allowed_classes=None,
                 images_to_be_investigated=None,
                 export_to_coco=False,
                 train_test_val="train"
                 ):

        super().__init__("ONCE", train_test_val, batch_size, path_to_dataset)

        self.num_classes, self.labels = functions.open_labels(os.path.join(path_to_labels, "once_labels.json"))
        self.labels_numpy = np.asarray(self.labels)

        self.image_size = (1920, 1020)
        self.device = device

        path_to_data = os.path.join(path_to_dataset, self.name, train_test_val, "data")
        dir_files = os.listdir(path_to_data)
        full_paths = map(lambda name: os.path.join(path_to_data, name), dir_files)

        dirs = [file for file in full_paths if os.path.isdir(file)]
        ann_id = 0
        img_cnt = 0

        if export_to_coco:
            destination = os.path.join(path_to_data, train_test_val + "_coco_format", train_test_val)
            if not os.path.exists(os.path.join(path_to_data, train_test_val + "_coco_format")):
                os.makedirs(os.path.join(path_to_data, train_test_val + "_coco_format"))
                os.makedirs(os.path.join(path_to_data, train_test_val + "_coco_format", train_test_val))
                os.makedirs(os.path.join(path_to_data, train_test_val + "_coco_format", "annotations"))

        for dir_index, direc in enumerate(dirs):
            images_dirs = [os.path.join(direc, dire) for dire in os.listdir(direc) if
                           "cam03" in dire]  # only the front camera
            lidar_dirs = [os.path.join(direc, dire) for dire in os.listdir(direc) if "lidar" in dire]
            labels_file = open(os.path.join(direc, dir_files[dir_index] + ".json"), "r")
            labels = json.load(labels_file)

            print("Searching folders and searching for images")
            for image_dir, lidar_dir in tqdm(zip(images_dirs, lidar_dirs)):
                files = sorted(os.listdir(image_dir))
                image_files = [file for file in files if file.split(".")[1].lower() == "jpg"]
                files = sorted(os.listdir(lidar_dir))
                lidar_files = [file for file in files if file.split(".")[1].lower() == "bin"]

                # "frames": [{
                #     "sequence_id":          < str >   -- sequence id of the scene.
                #     "frame_id":             < str >   -- timestamp of the frame.
                #     "pose":                 < list >  -- (quat_x, quat_y, quat_z, quat_w, trans_x, trans_y, trans_z).
                #     "annos":                < list >  -- 3D and 2D annotations for each objects appear in this frame, detailed description in frame annotation section.
                # }]
                for image, lidar in zip(image_files, lidar_files):
                    annotation_list = list()

                    image_id = image.split(".")[0].lower()
                    annotation = [item["annos"] for item in labels['frames'] if
                                  "annos" in item.keys() and item["frame_id"] == image_id]

                    if len(annotation) > 0:  # they sampled the lidar with 2Hz - not every image has annotation
                        if export_to_coco:
                            shutil.copy(os.path.join(image_dir, image), destination)

                        classes = annotation[0]["names"]
                        ann_3d = annotation[0]["boxes_3d"]
                        ann_2d = annotation[0]["boxes_2d"]["cam03"]
                        # "annos": [{
                        #     "name":                 < list >  -- list of object names ['Car'|'Truck'|'Bus'|'Pedestrian'|'Cyclist'].
                        #     "boxes_3d":             < list of list > -- N x 7 bounding box for each object, (cx, cy, cz, l, w, h, θ).
                        #     "boxes_2d":             < dict >  -- conatins 2D project bounding box (xmin, ymin, xmax, ymax) for each object in each camera, (-1., -1., -1., -1) otherwise.
                        #     "num_points_in_gt":     < list >  -- list of number indicating point cloud numbers in each object 3D bounding box
                        # }]

                        for cls, bb3d, bb2d in zip(classes, ann_3d, ann_2d):
                            # only objects seen by CAM03 will be processed
                            if bb2d[0] > -1.0 and bb2d[1] > -1.0 and bb2d[2] > -1.0 and bb2d[3] > -1.0:
                                item = nested_dict()

                                item["image_id"] = img_cnt
                                item["id"] = ann_id

                                item["class"] = cls
                                item["category_id"] = self.labels.index(cls)
                                clipped_bb = clip_bb_size(bb2d, self.image_size)
                                item["bbox"] = functions.xyxy2xywh(np.asarray(clipped_bb)).tolist()
                                item["area"] = float(item["bbox"][2] * item["bbox"][3])
                                dist_to_corner = calc_dist_to_corner(w=bb3d[4], l=bb3d[3],
                                                                     cx=bb3d[0], cy=bb3d[1],
                                                                     sig=bb3d[6])
                                item["iscrowd"] = 0
                                item["distance"] = float(dist_to_corner)

                                annotation_list.append(item)
                                ann_id += 1

                                if export_to_coco:
                                    self.dataset["annotations"].append(item)

                        if len(annotation_list) > 0:
                            if not export_to_coco:
                                self.dataset[image].append((os.path.join(image_dir, image), annotation_list))
                            else:
                                image_item = nested_dict()
                                image_item["file_name"] = image
                                image_item["width"], image_item["height"] = get_image_size(
                                    os.path.join(image_dir, image))
                                image_item["id"] = img_cnt
                                self.dataset["images"].append(image_item)

                        img_cnt += 1

        self.dataset_len = len(list(self.dataset.keys()))

        self.batch_size = batch_size

        if export_to_coco:
            categories = [{'id': index, 'name': label} for index, label in enumerate(self.labels)]
            self.dataset["categories"] = categories
            export_to_coco_f(path_to_data, os.path.join(path_to_dataset, self.name), train_test_val, self.dataset, just_annotation=True)
