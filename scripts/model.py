import numpy as np
import torch
import copy
import os
import collections

import torchvision.transforms as transforms

import settings
from scripts.functions import xyxy2xywh, xywh2xyxy
from utils.general import non_max_suppression, scale_coords
from scripts.metrics.mean_avg_precision import mean_average_precision


class CNNWrapper:

    def __init__(self, model, device, models_name):
        self.model = model
        self.model.to(device)
        self.model.eval()
        self.models_name = models_name

        # define the torchvision image transforms
        self.transform = transforms.Compose([
            transforms.ToTensor(),
        ])

    def predict_yolov5(self,
                       batch,
                       detection_threshold,
                       image_size,
                       dataset,
                       pred_bboxes, pred_classes_ids, pred_labels, pred_scores):

        outputs = self.model(batch)  # get the predictions on the image
        predictions = non_max_suppression(outputs, conf_thres=detection_threshold, iou_thres=0.5,
                                          classes=None, agnostic=False,
                                          multi_label=False,
                                          labels=(),
                                          max_det=100)

        for det in predictions:
            if det is not None:  # is not None
                det[:, :4] = scale_coords((image_size[0], image_size[1]), det[:, :4],
                                          (image_size[0], image_size[1])).round()

                # Write results
                temp_pred_classes_ids = list()
                temp_pred_labels = list()
                temp_pred_scores = list()
                temp_pred_bboxes = list()
                for *xyxy, conf, cls in reversed(det):
                    temp_pred_classes_ids.append(dataset.labels.index(self.model.names[int(cls)]))
                    temp_pred_labels.append(self.model.names[int(cls)])
                    temp_pred_scores.append(conf.detach().cpu().tolist())
                    temp_pred_bboxes.append([xy.detach().cpu().tolist() for xy in xyxy])
                pred_classes_ids.append(temp_pred_classes_ids)
                pred_labels.append(temp_pred_labels)
                pred_scores.append(temp_pred_scores)
                pred_bboxes.append(temp_pred_bboxes)

    def predict(self,
                batch,
                detection_threshold,
                dataset,
                pred_bboxes, pred_classes_ids, pred_labels, pred_scores):

        outputs = self.model(batch)  # get the predictions on the image

        for output in outputs:
            scores = output['scores'].detach().cpu().numpy()
            classes_ids = output['labels'].detach().cpu().numpy()
            boxes = output['boxes'].detach().cpu().numpy()
            indices = np.where(scores > detection_threshold)

            # get boxes above the threshold score

            pred_scores.append(scores[indices])  # list of numpy arrays
            pred_bboxes.append(boxes[indices])  # list of numpy arrays
            if dataset.name != "coco":
                coco_labels = dataset.coco_labels_numpy[classes_ids[indices]]
                dataset_labels = [dataset.coco_to_others_matching[label][dataset.name] for label in coco_labels]
                for labels in dataset_labels:
                    if len(labels) != 0:
                        ids = [dataset.labels.index(label) for label in labels]
                        pred_classes_ids.append(ids)  # list of numpy arrays
                pred_labels.append(dataset_labels)
            else:
                pred_classes_ids.append(classes_ids[indices])  # list of numpy arrays
                pred_labels.append(dataset.labels_numpy[classes_ids[indices]])

    def predict_and_calculate_mean_average_precision(self, dataset, path):
        # https://pytorch.org/vision/stable/models.html#object-detection-instance-segmentation-and-person-keypoint-detection
        # fasterrcnn_resnet50_fpn - 37.0
        # ssdlite320_mobilenet_v3_large - 21.3
        # https://jonathan-hui.medium.com/map-mean-average-precision-for -object - detection - 45c121a31173

        text_file = open(os.path.join(path, "mAP_output.txt"), "a")

        def nested_dict():
            return collections.defaultdict(nested_dict)

        dict_average_precisions = nested_dict()
        dict_of_predictions = nested_dict()
        list_of_average_precisions = list()
        # COCO AP - AP @ [.50: .05:.95]:
        list_of_true_boxes = list()
        list_of_predictions = list()
        list_of_distances = list()

        with torch.no_grad():
            for counter, (image, gts) in enumerate(dataset.dataset_iterator()):

                pred_bboxes, pred_labels, pred_classes_ids, pred_scores = list(), list(), list(), list()
                if self.models_name == "yolov5":
                    image_sizes = [image_shape.shape for image_shape in image.data]
                    self.predict_yolov5(image, 0.5, (image_sizes[0][1], image_sizes[0][2]), dataset,
                                        pred_bboxes, pred_labels, pred_classes_ids, pred_scores)
                else:
                    # (batch, detection_threshold, dataset, pred_bboxes, pred_classes_ids, pred_labels, pred_scores)
                    self.predict(image, 0.5, dataset, pred_bboxes, pred_labels, pred_classes_ids, pred_scores)



                for gt, pred_bb, pred_lab, pred_id, pred_score, distances in \
                        zip(gts, pred_bboxes, pred_labels, pred_classes_ids, pred_scores):

                    for entry in gt:
                        true_boxes = xywh2xyxy(entry["bbox"])
                        object_cls = entry["class"]
                        object_id = entry["category_id"]
                        image_id = entry["image_id"]
                        list_of_true_boxes.append([image_id, object_id, 1.0, true_boxes])

                        distance = entry["distance"]
                        list_of_distances.append(distance)

                    for bb, lab, score in zip(pred_bb, pred_id, pred_score):
                        list_of_predictions.append([image_id, lab, score, bb])

        for iou_threshold in settings.iou_thresholds:
            average_precision = mean_average_precision(list_of_predictions, list_of_true_boxes, dict_average_precisions,
                                                       iou_threshold=iou_threshold, box_format="corners",
                                                       num_classes=len(dataset.labels))
            average_precision_percentage = average_precision.item() * 100.0
            list_of_average_precisions.append(average_precision_percentage)
            text_file.write("AP {:.2f} for iou {}\n".format(average_precision_percentage, iou_threshold))

        average_precision = sum(list_of_average_precisions) / float(len(list_of_average_precisions))
        # print("COCO final AP Box for all images: {:.2f}".format(average_precision))
        text_file.write("Final AP Box for all images.\n")
        text_file.write("{:.2f} \n".format(average_precision))
        text_file.close()

        # AP[0.5,0.95,0.05]
        ap_per_class_matrix = np.zeros(shape=(len(list(dict_average_precisions.keys())), len(settings.iou_thresholds)),
                                       dtype=float)

        for index_label, (label, ious) in enumerate(dict_average_precisions.items()):
            for index_iou, (iou, values) in enumerate(ious.items()):
                # [mAP_class, precisions, recalls, TP, FP, IOUs, BBsize]
                ap_per_class_matrix[index_label][index_iou] = values[0]

        return ap_per_class_matrix, average_precision, dict_average_precisions
