import os
import copy
import collections
from tqdm import tqdm
import seaborn as sns
import numpy as np
import cv2
import math
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter

plt.rcParams.update({'figure.max_open_warning': 0})

analyzed_labels = ["person", "car", "bicycle", "motorcycle", "bus", "train",
                   "truck", "traffic light", "street sign", "stop sign"]

remove_string = ["head.", "module_list.", ".weight", "backbone.body.", "backbone.", "roi_heads."]

lim_highway = 101.57
lim_motorway = 61.78 # 27.77, 3.96 109.96 - 48.17
lim_city = 17.23 # 13.88, 2.23,  30.9524 - 13,72

sns.set_theme()
sns.set_context("talk", font_scale=0.65, rc={"font.family": "serif", "font.serif": ["Computer Modern Serif"]})


class Visualization:

    @staticmethod
    def setKey(dictionary, key, value):
        if key not in dictionary:
            dictionary[key] = [value]
        elif type(dictionary[key]) == list:
            dictionary[key].append(value)
        else:
            dictionary[key] = [dictionary[key], value]

    @staticmethod
    def create_video_of_static_images(path, index):
        files = sorted(os.listdir(path))
        image_files = [os.path.join(path, file) for file in files if file.split(".")[1].lower() == "png"]

        if len(image_files) > 0:
            image_cv = cv2.imread(image_files[0])
            height, width, ch = image_cv.shape
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            video = cv2.VideoWriter(os.path.join(path, 'static_outlier.mp4'), fourcc, 20, (width, height))
            for image in image_files:
                image_cv = cv2.imread(image)
                rows, cols, ch = image_cv.shape
                pts1 = np.float32([[0, 0], [0, cols], [rows, cols]])
                end_rows = rows - rows/4
                begin_rows = rows / 4
                pts2 = np.float32([[0, 0], [begin_rows, cols/2], [end_rows, cols/2]])
                #M = cv2.getPerspectiveTransform(pts1, pts2)
                M = cv2.getAffineTransform(pts1, pts2)
                dst = cv2.warpAffine(np.asarray(image_cv, dtype=np.uint8), M, (cols, rows))

                video.write(cv2.cvtColor(image_cv, cv2.COLOR_RGB2BGR))

            cv2.destroyAllWindows()
            video.release()

        else:
            print("No static images found")

    @staticmethod
    def plot_optical_flow(path, data, flow_const, dataset):
        from scipy import ndimage

        if len(data) > 0:
            fig, ax = plt.subplots(figsize=(8, 4))

            opt_flow = np.asarray(np.asarray(data)[:, 0], dtype=float)[::4]# / flow_const
            ind = np.arange(opt_flow.shape[0])
            static_scenario_flag = np.asarray(data)[:, 1]
            count_static_scene = np.where(static_scenario_flag == "True")[0].shape[0]
            count_static_scene = np.where(opt_flow < flow_const)[0].shape[0]
            scene_index = np.asarray(data)[:, 2]

            #ax.plot(ind, opt_flow, ':', alpha=0.7, label="actual value: {:.2f}".format(np.mean(opt_flow)))
            ax.plot(ind, opt_flow, ':', label="actual value", linewidth=1.0)
            result = ndimage.median_filter(opt_flow, size=8)
            ax.plot(ind, result, 'g', label="median value, win_size = 4", linewidth=2.0)
            ax.plot([0, ind.shape[0]], [flow_const, flow_const], 'r', label="static scene threshold", linewidth=1.0)

            x_ticks = list(np.linspace(0, len(data), 10, dtype=int))
            #ax.set_xticks(x_ticks)
            ax.set_xticklabels(x_ticks)

            ax.set_title("Magnitude of dense optical flow over dataset: {}".format(dataset))
            ax.set_xlabel("Index of images")
            ax.set_ylabel("Magnitude of dense optical flow")
            ax.legend()
            fig.tight_layout()
            fig.savefig(os.path.join(path, "optical_flow.pdf"))
            print("Optical flow mean: {}".format(np.mean(opt_flow)))
            print("Number of static images: {}".format(count_static_scene))

    @staticmethod
    def plot_all(plot_path, name, x, y, label, exp_decay_f=None, legends="None", xlabel="None", ylabel="None",
                 title="None", figsize=(6, 4)):
        """

        """
        normalize = True
        plt.rcParams.update({'font.size': 10})
        from scipy.stats import wasserstein_distance

        if label is not None:
            save_path = os.path.join(plot_path, "visualization", label)
        else:
            save_path = os.path.join(plot_path, "visualization")

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        fig, ax = plt.subplots(figsize=figsize)
        #fig, ax = plt.subplots()

        if not isinstance(x, np.ndarray):
            for _x, _y, legend in zip(x, y, legends):
                if len(_x) > 0:
                    num_samples = _x[2]
                    x_values = _y[1][:100]
                    y_values = (np.asarray(_x[0]))[:100]
                    y_values_norm = y_values / np.max(y_values)
                    uniform_dist = np.ones(100, dtype=float)
                    was_dist = wasserstein_distance(y_values_norm, uniform_dist)
                    if xlabel == "BB relative size [-]":
                        if normalize:
                            ax.bar(x_values, y_values_norm, 0.01,
                                    label=legend + " $n_{samp}=$" + "${}$".format(num_samples) + ", $w_{dist}=$" + "{:.2f}".format(was_dist)) #, alpha=0.7)

                    elif xlabel == "Distance to object [m]":
                        # gather final statistics
                        ind_of = np.where(np.asarray(x_values) > lim_highway)[0]
                        sum_of_highway_obj = np.sum(y_values[ind_of[0]:])
                        print("{}, {}".format(name, legend))
                        print("Number of objects further than: {} is: {}".format(lim_highway, sum_of_highway_obj))
                        print("Wasserstein dist. is {}".format(was_dist))
                        if normalize:
                            ax.bar(x_values, y_values_norm,
                                    label=legend + " $n_{samp}=$" + "${}$".format(num_samples) + ", $w_{dist}=$" + "{:.2f}".format(was_dist))

                        y_max = ax.get_ylim()[1]
                        y_max_margin = y_max - y_max * 0.03
                        x_offset = 3.5
                        ax.plot([lim_highway, lim_highway], [0.0, y_max], "r")#, label='for 130km/h')
                        #ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                        #        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))


        # ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.set_title(title)
        ax.legend()

        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(save_path, name + ".pdf"), dpi=300)
        # plt.show()

    @staticmethod
    def plot_histogram_of_dataset(_np_data,
                                  _bar_path,
                                  _name,
                                  _title,
                                  _label,
                                  _x_label="None",
                                  _y_label="None",
                                  _legend="None",
                                  r_min=0.0, r_max=1.0,
                                  num_bins=100,
                                  _np_data_s=None):

        plt.rcParams.update({'font.size': 12})

        if len(_np_data.shape) > 0:
            conf_path = os.path.join(_bar_path, "visualization", _label)
            if not os.path.exists(conf_path):
                os.makedirs(conf_path)

            fig, ax = plt.subplots(figsize=(8, 4))
            mu = np.mean(_np_data)
            sigma = np.std(_np_data)
            if _np_data_s is not None:
                label_primary = "$^y: \mu={:.2f}$, $\sigma={:.2f}$".format(mu, sigma)
            else:
                label_primary = "$\mu={:.2f}$, $\sigma={:.2f}$".format(mu, sigma)

            n, bins, patches = ax.hist(_np_data, num_bins, range=(r_min, r_max), color='b', rwidth=0.8)  #, alpha=0.7# range=(np.min(_np_data), np.max(_np_data))

            if _np_data_s is not None:
                n_p, bins_p, patches_p = ax.hist(_np_data_s, num_bins, range=(r_min, r_max),
                                                 color='r',
                                                 label="$\^y: \mu={:.2f}$, $\sigma={:.2f}$".format(np.mean(_np_data_s),
                                                                                                   np.std(_np_data_s)),
                                                 #alpha=0.7,
                                                 rwidth=0.8)
                mAP_rel_size = (n_p / n) * 100.0
                mAP_rel_size = np.nan_to_num(mAP_rel_size)
                fig_mAP, ax_mAP = plt.subplots(figsize=(8, 4))
                ind = np.arange(num_bins)
                ind_label = np.linspace(0, 100, num=11, dtype=int)
                labels = np.linspace(0.0, 1.0, num=11)
                ax_mAP.bar(ind, mAP_rel_size, label="mAP: {}".format(np.mean(mAP_rel_size))) #, alpha=0.7
                ax_mAP.plot(ind, [np.mean(mAP_rel_size)] * 100, color="r") #
                ax_mAP.set_ylabel('mAP')
                ax_mAP.set_xlabel('BB rel. size')
                ax_mAP.set_title('mAP over BB rel. size')
                # ax_mAP.set_xticks(ind, labels=labels.tolist())
                ax_mAP.grid()
                ax_mAP.legend()
                fig_mAP.tight_layout()
                fig_mAP.savefig(os.path.join(conf_path, _name + "_mAP_hist.pdf"), dpi=300)

            ax.set_ylabel(_y_label)
            ax.set_xlabel(_x_label)
            ax.set_title(_title)

            if _x_label == "Distance to object [m]" and _legend == "None":
                y_max = ax.get_ylim()[1]
                y_max_margin = y_max - y_max*0.03
                x_offset = 3.5
                # plot the speed limits and corresponding legend
                ax.plot([lim_highway, lim_highway], [0.0, y_max], "r", label='for 130km/h')
                ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.plot([lim_motorway, lim_motorway], [0.0, y_max], color='orange', label='for 100km/h')
                ax.text(lim_motorway - x_offset, y_max_margin, str(lim_motorway),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.plot([lim_city, lim_city], [0.0, y_max], "g", label='for 50km/h')
                ax.text(lim_city - x_offset, y_max_margin, str(lim_city),
                        bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
                ax.legend(title="Minimum safe distance in [m]", framealpha=1.0)
            else:
                # plot just mean and std
                ax.legend(framealpha=1.0)

            plt.axvline(mu, color='k', linestyle='dashed', linewidth=1)
            min_ylim, max_ylim = plt.ylim()
            plt.text(mu * 1.1, max_ylim * 0.9, 'Mean: {:.2f}'.format(mu))

            fig.tight_layout()
            # plt.show()
            fig.savefig(os.path.join(conf_path, _name + "_hist.pdf"), dpi=300)

            return n, bins, patches

    @staticmethod
    def canvas2rgb_array(canvas):
        """Adapted from: https://stackoverflow.com/a/21940031/959926"""
        canvas.draw()
        buf = np.frombuffer(canvas.tostring_rgb(), dtype=np.uint8)
        ncols, nrows = canvas.get_width_height()
        scale = round(math.sqrt(buf.size / 3 / nrows / ncols))
        return buf.reshape(scale * nrows, scale * ncols, 3)

    @staticmethod
    def draw_heatmap(boxes, label, distance, image_size, bb_rel_size, path):
        # draws a
        #
        visualization_path = os.path.join(path, "visualization", label)
        if not os.path.exists(visualization_path):
            os.makedirs(visualization_path)

        fig, ax = plt.subplots(figsize=(8, 4))

        max_width = np.max(np.asarray(image_size)[:, 0])
        max_height = np.max(np.asarray(image_size)[:, 1])
        heatmap = np.zeros(shape=(max_width, max_height), dtype=int)
        corner_points_distance = list()
        max_distance = np.max(distance)
        quadrants_obj_cnt = {
            ((0, int(max_width/2)), (0, int(max_height/2))): 0,
            ((0, int(max_width / 2)), (int(max_height / 2), int(max_height))): 0,
            ((int(max_width / 2), int(max_width)), (0, int(max_height / 2))): 0,
            ((int(max_width / 2), int(max_width)), (int(max_height / 2), int(max_height))): 0
        }

        for box, size, dist, bb_size in tqdm(zip(boxes.tolist(), image_size, distance, bb_rel_size)):
            offset_x = int((max_width - size[0]) / 2)
            offset_y = int((max_height - size[1]) / 2)
            heatmap[offset_x + int(box[0]): offset_x + int(box[0]) + int(box[2]),
                    offset_y + int(box[1]): offset_y + int(box[1]) + int(box[3])] += 1

            color = 'blue'
            if dist > lim_highway:
                color = 'red'
            elif lim_highway > dist > lim_motorway:
                color = 'yellow'
            elif lim_motorway > dist > lim_city:
                color = 'green'
            corner_points_distance.append(np.asarray([offset_x + int(box[0]),  # 0
                                                      offset_y + int(box[1]),  # 1
                                                      dist,  # 2
                                                      color,  # 3
                                                      np.sqrt(bb_size) * max_distance,  # 4 - to linearize it
                                                      int(box[2]),  # 5 - width
                                                      int(box[3])]  # 6 - height
                                                     ))

            center_x = offset_x + int(box[0]) + int(box[2] / 2)
            center_y = offset_y + int(box[1]) + int(box[3] / 2)

            for quadrant in quadrants_obj_cnt.keys():
                #ind = 0 - x coordinate, ind = 1 y coordinate
                if quadrant[0][0] < center_x < quadrant[0][1]:
                    if quadrant[1][0] < center_y < quadrant[1][1]:
                        quadrants_obj_cnt[quadrant] += 1

        # MxN - rows and columns
        heatmap_in_procent = np.moveaxis(np.asarray(heatmap), 0, -1) / boxes.shape[0] * 100.0
        im = ax.imshow(heatmap_in_procent, cmap='hot', interpolation='nearest')

        # middle lane - vertical
        middle_line_x = [int(max_width/2), int(max_width/2)-1]
        middle_line_y = [0, max_height-1]
        ax.plot(middle_line_x, middle_line_y, color="w", alpha=0.7)
        # middle lane - horizontal
        middle_line_x = [0, max_width-1]
        middle_line_y = [int(max_height/2), int(max_height/2)-1]
        ax.plot(middle_line_x, middle_line_y, color="w", alpha=0.7)
        # count of objects within quadrants
        for quadrant in quadrants_obj_cnt.keys():
            ax.text(float(quadrant[0][0] + 24), float(quadrant[1][0] + 64), str(quadrants_obj_cnt[quadrant]), color='white')

        x_ticks = list(np.linspace(0, max_width, 5, dtype=int))
        y_ticks = list(np.linspace(0, max_height, 5, dtype=int))
        ax.xaxis.tick_top()
        ax.set_xticks(x_ticks)
        ax.set_yticks(y_ticks)

        corner_x_mean = np.mean(np.asarray(np.asarray(corner_points_distance)[:, 0], dtype=float))
        corner_y_mean = np.mean(np.asarray(np.asarray(corner_points_distance)[:, 1], dtype=float))
        width_mean = np.mean(np.asarray(np.asarray(corner_points_distance)[:, 5], dtype=float))
        height_mean = np.mean(np.asarray(np.asarray(corner_points_distance)[:, 6], dtype=float))
        mean_bb_x = [corner_x_mean,
                     corner_x_mean,
                     corner_x_mean + width_mean,
                     corner_x_mean + width_mean,
                     corner_x_mean]
        mean_bb_y = [corner_y_mean,
                     corner_y_mean + height_mean,
                     corner_y_mean + height_mean,
                     corner_y_mean,
                     corner_y_mean]
        # ax.set(xlabel=None)
        # ax.set(ylabel=None)
        #ax.axis('off')
        fig.tight_layout()
        cbar = fig.colorbar(im, location='right', anchor=(-0.25, 0.5), shrink=0.85)
        cbar.set_label('[%]', rotation=270, labelpad=15)
        # ax.plot(mean_bb_x, mean_bb_y, color="blue")
        # for i in range(heatmap.shape[0]):
        #    for j in range(heatmap.shape[1]):
        #        text = ax.text(j, i, heatmap[i, j],
        #                       ha="center", va="center", color="w")
        ax.set_title("Heatmap of annotated objects with label: {}".format(label))
        fig.tight_layout()
        fig.savefig(os.path.join(visualization_path, "{}_BB_heatmap.pdf".format(label)), dpi=300)
        fig.savefig(os.path.join(visualization_path, "{}_BB_heatmap.png".format(label)), dpi=300)

        create_video = False
        if create_video:
            # plotting 3D scatter map
            fig = plt.figure(figsize=(12, 9))
            axs = fig.add_subplot(111, projection='3d')

            my_cmap = plt.get_cmap('RdYlGn')
            x = np.asarray(np.asarray(corner_points_distance)[:, 0], dtype=float)
            y = np.asarray(np.asarray(corner_points_distance)[:, 1], dtype=float)
            z = np.asarray(np.asarray(corner_points_distance)[:, 2], dtype=float)
            z = z / np.max(z)  # normalize for the cmap
            axs.scatter(x, y, z, c=z, cmap=my_cmap,
                        s=np.asarray(np.asarray(corner_points_distance)[:, 4], dtype=float))

            axs.set_xlabel('x')
            axs.set_ylabel('y')
            axs.set_zlabel('distance')
            fig.tight_layout()

            # axs.set_title("Heatmap of label: {}".format(label))

            plt_array = Visualization.canvas2rgb_array(fig.canvas)
            height, width, layers = plt_array.shape
            fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            video = cv2.VideoWriter(os.path.join(visualization_path, "{}_BB_3D.mp4".format(label)), fourcc, 20,
                                    (width, height))

            for angle in range(0, 360):
                axs.view_init(30, angle)
                plt_array = Visualization.canvas2rgb_array(fig.canvas)
                video.write(plt_array)

            cv2.destroyAllWindows()
            video.release()

    # Inverse of the preprocessing and plot the image
    @staticmethod
    def plot_img(x):
        """
        x is a BGR image with shape (? ,224, 224, 3)
        """
        t = np.zeros_like(x[0])
        t[:, :, 0] = x[0][:, :, 2]
        t[:, :, 1] = x[0][:, :, 1]
        t[:, :, 2] = x[0][:, :, 0]
        # plt.imshow(np.clip((t+[123.68, 116.779, 103.939]), 0, 1.0))
        # plt.imshow(np.clip(t, 0, 1.0))
        # plt.imshow(t)
        plt.grid('off')
        plt.axis('off')
        # plt.show()

    @staticmethod
    def plot_x_y(plot_path, name, x, y, label, exp_decay_f=None, legends="None", xlabel="None", ylabel="None",
                 title="None", figsize=(8, 4)):
        """

        """
        normalize = True
        plt.rcParams.update({'font.size': 10})

        if label is not None:
            save_path = os.path.join(plot_path, "visualization", label)
        else:
            save_path = os.path.join(plot_path, "visualization")

        if not os.path.exists(save_path):
            os.makedirs(save_path)

        fig, ax = plt.subplots(figsize=figsize)
        #fig, ax = plt.subplots()

        if not isinstance(x, np.ndarray):
            for _x, _y, legend in zip(x, y, legends):
                if len(_x) > 0:
                    num_samples = _x[2]
                    if normalize:
                        ax.plot(_y[1][:_x[0].shape[0]], _x[0] / np.max(_x[0]),
                                label=legend + " [" + str(num_samples) + "]", alpha=0.7)
                    else:
                        ax.plot(_y[1][:_x[0].shape[0]], _x[0],
                                label=legend + " [" + str(num_samples) + "]", alpha=0.7)
        else:
            # mark red the outliers
            colors = ["blue"] * len(x)
            if exp_decay_f is not None:
                yi = exp_decay_f(x)

                for index, (y_gt, y_calc) in enumerate(zip(y, yi)):
                    if y_calc < y_gt:
                        colors[index] = "red"
            # scale = np.asarray((x+0.1)*5, dtype=float)
            if legends == "None":
                ax.scatter(x, y, c=colors, alpha=0.2)  # , s=scale) # label=legends
            else:
                ax.scatter(x, y, c=colors, label=legends, alpha=0.2)

        # ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)
        ax.set_title(title)
        ax.legend()

        if xlabel == "Distance to object [m]" and legends == "None":
            # hardcoded the limits of y axis to be 1.0
            lin_space = np.linspace(0.0, 120.0, 120)
            exp_decay = exp_decay_f(lin_space)
            # $e^{-\frac{x}{6.0} + 0.075}$
            ax.plot(lin_space, exp_decay, "black", label=r'$e^{-\frac{x}{6.0} + 0.075}$')

            y_max = ax.get_ylim()[1]
            y_max_margin = y_max - y_max * 0.03
            x_offset = 3.5
            ax.plot([lim_highway, lim_highway], [0.0, y_max], "r", label='for 130km/h')
            ax.text(lim_highway - x_offset, y_max_margin, str(lim_highway),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.plot([lim_motorway, lim_motorway], [0.0, y_max], color='orange', label='for 100km/h')
            ax.text(lim_motorway - x_offset, y_max_margin, str(lim_motorway),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.plot([lim_city, lim_city], [0.0, y_max], "g", label='for 50km/h')
            ax.text(lim_city - x_offset, y_max_margin, str(lim_city),
                    bbox=dict(facecolor='white', edgecolor='white', boxstyle='round'))
            ax.legend(title="Minimum safe distance in [m]", framealpha=1.0)

        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(save_path, name + ".pdf"), dpi=300)


    @staticmethod
    def plot_histogram_of_confidence(_np_data, _bar_path, _name, _labels, _title, _index="_"):

        conf_path = os.path.join(_bar_path, _name)
        if not os.path.exists(conf_path):
            os.makedirs(conf_path)

        x = np.arange(len(_labels))
        width = 0.8

        fig, ax = plt.subplots()
        bar_ref = ax.bar(x, _np_data, width, color='r')
        ax.set_ylabel('Confidences')
        ax.set_title(_title)
        ax.set_xticks(x)
        ax.set_xticklabels(_labels)
        ax.set_yticks(np.arange(0, 1.1, 0.1))
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))

        # ax.set_xticklabels(_labels)
        # ax.legend()

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{:.4f}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom')

        autolabel(bar_ref)
        fig.tight_layout()
        # plt.show()
        plt.savefig(os.path.join(conf_path, "%04d_histogram.pdf" % (_index)), dpi=300)

    @staticmethod
    def plot_histogram_of_weights(_np_data, _hist_path, _name, _index, _title):
        num_bins = 50
        fig, (ax_R, ax_G, ax_B) = plt.subplots(3, 1)
        # fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)
        # the histogram of the data
        # weights_norm = np.ones_like(_np_data)/float(len(_np_data))
        n_R, bins_R, patches = ax_R.hist(_np_data[:, :, 0].flatten(), num_bins, density=1, color='r')
        n_G, bins_G, patches = ax_G.hist(_np_data[:, :, 1].flatten(), num_bins, density=1, color='g')
        n_B, bins_B, patches = ax_B.hist(_np_data[:, :, 2].flatten(), num_bins, density=1, color='b')
        # print(len(bins))
        # print(bins)
        # n, bins, patches = ax.hist(np_weights, num_bins, weights=weights_norm)
        # print(bins)
        mean_R = np.mean(_np_data[:, :, 0])
        std_R = np.std(_np_data[:, :, 0])
        mean_G = np.mean(_np_data[:, :, 1])
        std_G = np.std(_np_data[:, :, 1])
        mean_B = np.mean(_np_data[:, :, 2])
        std_B = np.std(_np_data[:, :, 2])

        # add a 'best fit' line
        hist_path = os.path.join(_hist_path, _name)
        if not os.path.exists(hist_path):
            os.makedirs(hist_path)

        # y_R = ((1 / (np.sqrt(2 * np.pi) * std_R)) * np.exp(-0.5 * (1 / std_R * (bins_R - mean_R))**2))
        # ax_R.plot(bins_R, y_R, '--')
        # axs[moment].tick_params(axis='both', which='major', labelsize=6)
        # axs[moment].tick_params(axis='both', which='minor', labelsize=4)
        # axs[moment].plot(bins, y, '--')
        # axs[moment].set_ylabel('Density', fontsize=6)
        # axs[moment].set_xlabel('Value', fontsize=6)
        # axs[moment].grid(True)
        ax_R.set_xlabel('Weight')
        ax_R.set_ylabel('Probability density')
        ax_R.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_R, std_R))

        # y_G = ((1 / (np.sqrt(2 * np.pi) * std_G)) * np.exp(-0.5 * (1 / std_G * (bins_G - mean_G))**2))
        # ax_G.plot(bins_G, y_G, '--')
        ax_G.set_xlabel('Weight')
        ax_G.set_ylabel('Probability density')
        ax_G.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_G, std_G))

        # y_B = ((1 / (np.sqrt(2 * np.pi) * std_B)) * np.exp(-0.5 * (1 / std_B * (bins_B - mean_B))**2))
        # ax_B.plot(bins_B, y_B, '--')
        ax_B.set_xlabel('Weight')
        ax_B.set_ylabel('Probability density')
        ax_B.set_title(r'{} with $\mu={:.4f}$, $\sigma={:.4f}$'.format(_title, mean_B, std_B))

        # Tweak spacing to prevent clipping of ylabel
        fig.tight_layout()
        # plt.show()
        fig.savefig(os.path.join(hist_path, "%d_histogram.pdf" % (_index)), dpi=300)

    @staticmethod
    def plot_weights_histogram(_weights, _ax, _bin, _canvas):
        mean = np.mean(_weights)
        std = np.std(_weights)
        # clear the previouse axis
        _ax.clear()
        n, bins, patches = _ax.hist(
            _weights, _bin, density=1, facecolor="green", alpha=0.75
        )
        y = ((1 / (np.sqrt(2 * np.pi) * std)) * np.exp(-0.5 * (1 / std * (bins - mean)) ** 2))
        _ax.plot(bins, y, '--')
        # self._ax.set_xlabel("Smarts")
        # self._ax.set_ylabel("Probability")
        _ax.set_title(r'$\mu={:.4f}$, $\sigma={:.4f}$'.format(mean, std))
        _ax.grid(True)
        _canvas.draw()

    @staticmethod
    def plot_kernel_criticality(ax, fig, worst_x, worst_x_means, worst_x_stds, colors, error_kw, fig_dir, layer, text,
                                _fontsize, _edge_color, _headline, _num_of_kernels):
        text_kwargs = dict(ha='center', va='center', fontsize=10, color='black', in_layout=True)
        ax.clear()
        y_pos = np.arange(len(worst_x))
        ax.barh(y_pos, worst_x_means, xerr=worst_x_stds, alpha=0.5, align='center', color=colors, error_kw=error_kw)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_yticks(y_pos)
        ax.set_yticklabels(worst_x)
        ax.set_xlabel('Criticality', fontsize=_fontsize)
        ax.set_ylabel('Indices', fontsize=_fontsize)
        ax.set_title("{}critical neurons for layer: {}".format(_headline, layer), fontsize=_fontsize + 2)
        # ax.set_title("{}".format(layer), fontsize=8)
        if _edge_color == "blue":
            x_max = np.max(worst_x_stds)
            y_max = 0.1
            plt.text(x_max - 0.125, y_max, 'Number of kernels:\n {}'.format(_num_of_kernels),
                     bbox=dict(facecolor='blue', alpha=0.5), **text_kwargs)
        elif _edge_color == "red":
            x_max = np.max(worst_x_stds)
            x_max = 2.125
            y_max = len(worst_x) - 1.125
            plt.text(x_max, y_max, 'Number of kernels:\n {}'.format(_num_of_kernels),
                     bbox=dict(facecolor='red', alpha=0.5), **text_kwargs)

        # plt.margins(0.2)
        # Tweak spacing to prevent clipping of tick-labels
        fig.set(edgecolor=_edge_color, linewidth=4, alpha=0.5)
        fig.tight_layout()
        # ax.subplots_adjust(bottom=0.25)
        # fig.savefig(os.path.join(fig_dir, _models_name + "_" + layer + "_criticality_result.pdf"), bbox_inches='tight')
        fig.savefig(os.path.join(fig_dir, text), bbox_inches='tight')

    @staticmethod
    def define_color(x_means, cri_tau):
        colors = list()
        for index in range(len(x_means)):
            criticality = x_means[index]  # + top_x_stds[index]

            if criticality > cri_tau:
                colors.append('red')
            elif criticality > cri_tau / 2:
                colors.append('orange')
            elif criticality > cri_tau / 10:
                colors.append('yellow')
            elif criticality < 0.0:
                colors.append('blue')
            else:
                colors.append('green')
        return colors

    def filter_neurons(critical_neurons, anti_or_critical):
        def nested_dict():
            return collections.defaultdict(nested_dict)

        temp_unsorted_dict = nested_dict()

        for classes, layers in critical_neurons.items():
            for layer, kernels in layers.items():
                for kernel, criticality in kernels.items():
                    cri = criticality[0] + (criticality[1] / 2.0)
                    if anti_or_critical == "anti":
                        # [0]mean + [1]std
                        if cri < 0.0:
                            temp_unsorted_dict[cri][classes]["layer"] = layer
                            temp_unsorted_dict[cri][classes]["kernel"] = kernel
                    elif anti_or_critical == "most":
                        # [0]mean + [1]std
                        if cri > 0.0:
                            temp_unsorted_dict[cri][classes]["layer"] = layer
                            temp_unsorted_dict[cri][classes]["kernel"] = kernel

        if anti_or_critical == "anti":
            return dict(sorted(temp_unsorted_dict.items()))
        else:
            return dict(sorted(temp_unsorted_dict.items(), reverse=True))

    @staticmethod
    def calculate_statistics(_stat_dict, _models_name, _path, _n_worst=15):

        cri_tau = 0.5
        from matplotlib.lines import Line2D
        custom_colors = [Line2D([0], [0], color="red", lw=4),
                         Line2D([0], [0], color="orange", lw=4),
                         Line2D([0], [0], color="yellow", lw=4),
                         Line2D([0], [0], color="green", lw=4),
                         Line2D([0], [0], color="blue", lw=4)]
        legend_text = ['mean > $\gamma$', 'mean > $\gamma$ / 2', 'mean > $\gamma$ / 8', 'mean > 0.0', 'mean < 0.0']
        legend_text = ['mean > $\gamma$', 'mean > $\gamma$ / 2', 'mean > $\gamma$ / 8', 'mean > 0.0', 'mean < 0.0']

        def nested_dict():
            return collections.defaultdict(nested_dict)

        all_classes_layers_list_of_criticality = collections.defaultdict(list)
        critical_neurons = nested_dict()
        anti_critical_neurons = nested_dict()

        for class_label, layers_kernels_criticality_dict in tqdm(_stat_dict.items()):
            if class_label in analyzed_labels:

                list_of_layers = list(layers_kernels_criticality_dict.keys())
                list_of_layers_means = list()
                list_of_layers_stds = list()
                list_of_layers_max = list()

                plt.rcParams.update({'font.size': 10})
                error_kw = {'elinewidth': 0.5, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
                fig, ax = plt.subplots(figsize=(6, 4))

                for layer, indices_dict in layers_kernels_criticality_dict.items():
                    list_of_neurons = list()
                    list_of_stds = list()
                    list_of_means = list()
                    for neurons_index, neurons_values in indices_dict.items():
                        list_of_neurons.append(str(neurons_index))
                        list_of_means.append(neurons_values[0])
                        list_of_stds.append(neurons_values[1])

                    # sorting indices after criticality
                    indexes = np.argsort(list_of_means)
                    list_of_anticritical_stds = list()
                    list_of_anticritical_means = list()
                    anti_critical_indices = [index for index, mean in enumerate(list_of_means) if mean < -0.001]
                    if len(anti_critical_indices) == 0:
                        # anti_critical_neurons[layer][class_label][str(anti_index)] = [0.0, 0.0]
                        list_of_anticritical_stds.append(0.0)
                        list_of_anticritical_means.append(0.0)
                    else:
                        for anti_index in anti_critical_indices:
                            anti_critical_neurons[layer][class_label][str(anti_index)] = [list_of_means[anti_index],
                                                                                          list_of_stds[anti_index]]
                            list_of_anticritical_stds.append(list_of_stds[anti_index])
                            list_of_anticritical_means.append(list_of_means[anti_index])

                    # filtering the x anti-critical neurons
                    number_of_worst = min(len(list_of_means), _n_worst)
                    best_x_indices = indexes[:_n_worst]
                    best_x_means = [float(list_of_means[index]) for index in best_x_indices]
                    best_x_stds = [float(list_of_stds[index]) for index in best_x_indices]

                    # filtering the x worst-critical neurons
                    list_of_critical_stds = list()
                    list_of_critical_means = list()
                    critical_indices = [index for index, mean in enumerate(list_of_means) if mean > 0.001]
                    worst_x_indices = indexes[-number_of_worst:]
                    worst_x_indices = [index for index in reversed(worst_x_indices) if list_of_means[index] > -0.05]
                    worst_x_means = [float(list_of_means[index]) for index in worst_x_indices]
                    worst_x_stds = [float(list_of_stds[index]) for index in worst_x_indices]
                    if len(critical_indices) == 0:
                        # critical_neurons[layer][class_label][str(anti_index)] = [0.0, 0.0]
                        list_of_critical_stds.append(0.0)
                        list_of_critical_means.append(0.0)
                    else:
                        for cri_index in critical_indices:
                            critical_neurons[layer][class_label][str(cri_index)] = [list_of_means[cri_index],
                                                                                    list_of_stds[cri_index]]
                            list_of_critical_stds.append(list_of_stds[cri_index])
                            list_of_critical_means.append(list_of_means[cri_index])

                    layers_name = copy.deepcopy(layer)
                    for remove_str in remove_string:
                        layers_name = layers_name.replace(remove_str, "")

                    # save images only in case the cricitality is higher than 0.05 for at least 5 neurons
                    if sum(mean > 0.005 for mean in worst_x_means) > 2:
                        colors = Visualization.define_color(worst_x_means, cri_tau)
                        fig_dir = os.path.join(_path, class_label, "critical")
                        if not os.path.exists(fig_dir):
                            os.makedirs(fig_dir)

                        text = _models_name + "_" + layer + "_criticality_result.pdf"
                        Visualization.plot_kernel_criticality(ax, fig, worst_x_indices, worst_x_means, worst_x_stds,
                                                              colors, error_kw, fig_dir, layers_name, text, 12, "red",
                                                              "Most-",
                                                              len(list_of_means))

                    # save images only in case the anti-cricitality is lower than 0.0 for at least 2 neurons
                    if sum(mean < -0.01 for mean in best_x_means) > 1:
                        colors = Visualization.define_color(best_x_means, cri_tau)
                        fig_dir = os.path.join(_path, class_label, "anti_critical")
                        if not os.path.exists(fig_dir):
                            os.makedirs(fig_dir)

                        text = _models_name + "_" + layer + "_anti_criticality_result.pdf"
                        Visualization.plot_kernel_criticality(ax, fig, best_x_indices, best_x_means, best_x_stds,
                                                              colors, error_kw, fig_dir, layers_name, text, 12, "blue",
                                                              "Anti-",
                                                              len(list_of_means))
                    list_of_layers_max.append(np.max(list_of_critical_means))
                    list_of_layers_means.append([np.mean(list_of_critical_means), np.mean(list_of_anticritical_means)])
                    list_of_layers_stds.append([np.std(list_of_critical_means), np.std(list_of_anticritical_means)])

                reduces_layers = list()
                reduces_means = list()
                reduces_stds = list()
                reduces_max = list()
                layers_colors_critical = list()
                layers_colors_anti_critical = list()
                for layers_name, layers_mean, layers_std, layers_max in zip(list_of_layers, list_of_layers_means,
                                                                            list_of_layers_stds, list_of_layers_max):
                    name = copy.deepcopy(layers_name)
                    for remove_str in remove_string:
                        name = name.replace(remove_str, "")
                    all_classes_layers_list_of_criticality[name].append(np.asarray(layers_mean))
                    # index 0 - critical, index 1 - anti critical
                    if layers_mean[0] > 0.0365 or layers_mean[1] < -0.045:
                        reduces_layers.append(name)
                        reduces_means.append(np.asarray(layers_mean))
                        reduces_stds.append(np.asarray(layers_std))
                        reduces_max.append(layers_max)
                        if layers_mean[0] > cri_tau:
                            layers_colors_critical.append('red')
                        elif layers_mean[0] > cri_tau / 2:
                            layers_colors_critical.append('orange')
                        elif layers_mean[0] > cri_tau / 8:
                            layers_colors_critical.append('yellow')
                        else:
                            layers_colors_critical.append('green')
                        layers_colors_anti_critical.append('blue')

                min_bar_index = 0
                if len(reduces_layers) > 20:
                    plt.rcParams.update({'font.size': 7})
                    fig, ax = plt.subplots(figsize=(7, 3))
                    error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
                    ax.clear()
                    max_bar_index = np.argmax(np.asarray(reduces_means)[:, 0])
                    critical_bars = ax.bar(reduces_layers,
                                           np.asarray(reduces_means)[:, 0],
                                           yerr=np.asarray(reduces_stds)[:, 0],
                                           error_kw=error_kw,
                                           alpha=0.5,
                                           color=layers_colors_critical)
                    # access the bar attributes to place the text in the appropriate location
                    for index, (bar, min_max) in enumerate(zip(critical_bars, reduces_max)):
                        if index == max_bar_index:
                            bar.set(edgecolor="black", linewidth=2)
                        plt.plot(bar.get_x() + 0.4, min_max, color='black', marker='*', linewidth=0.5, markersize=2)

                    min_bar_index = np.argmin(np.asarray(reduces_means)[:, 1])
                    anti_critical_bars = ax.bar(reduces_layers,
                                                np.asarray(reduces_means)[:, 1],
                                                alpha=0.5,
                                                color=layers_colors_anti_critical)
                    for index, bar in enumerate(anti_critical_bars):
                        if index == min_bar_index:
                            bar.set(edgecolor="black", linewidth=2)
                    ax.xaxis.set_ticklabels([])  # hiding all labels

                elif len(reduces_layers) > 1:
                    plt.rcParams.update({'font.size': 8})
                    fig, ax = plt.subplots(figsize=(7, 3))
                    ax.clear()
                    error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}

                    max_bar_index = np.argmax(np.asarray(reduces_means)[:, 0])
                    critical_bars = ax.bar(reduces_layers,
                                           np.asarray(reduces_means)[:, 0],
                                           yerr=np.asarray(reduces_stds)[:, 0],
                                           error_kw=error_kw,
                                           alpha=0.5,
                                           color=layers_colors_critical)
                    # access the bar attributes to place the text in the appropriate location
                    for index, (bar, min_max) in enumerate(zip(critical_bars, reduces_max)):
                        if index == max_bar_index:
                            bar.set(edgecolor="black", linewidth=2)
                        plt.plot(bar.get_x() + 0.4, min_max, color='black', marker='*', linewidth=0.5, markersize=4)

                    min_bar_index = np.argmin(np.asarray(reduces_means)[:, 1])
                    anti_critical_bars = ax.bar(reduces_layers,
                                                np.asarray(reduces_means)[:, 1],
                                                alpha=0.5,
                                                color=layers_colors_anti_critical)
                    for index, bar in enumerate(anti_critical_bars):
                        if index == min_bar_index:
                            bar.set(edgecolor="black", linewidth=2)
                    ax.xaxis.set_ticklabels([])  # hiding all labels

                ax.set_title(
                    "Layers\' criticality for model {} and for class: {}".format(_models_name,
                                                                                 class_label))
                ax.set_ylabel('Mean of normalized criticality')
                legend1 = ax.legend(custom_colors, legend_text)
                # ax.legend(custom_colors, legend_text)
                legend_headline = "Layers:"
                font_size = 7
                if len(reduces_layers) > 14:
                    font_size = 6
                elif len(reduces_layers) > 18:
                    font_size = 5
                elif len(reduces_layers) > 22:
                    font_size = 4
                elif len(reduces_layers) > 26:
                    font_size = 3
                legend2 = ax.legend(critical_bars, reduces_layers, loc="center left",
                                    title=legend_headline,
                                    bbox_to_anchor=(1.0, 0.5), prop={'size': font_size})
                patches = legend2.get_patches()
                for index, patch in enumerate(patches):
                    if index == min_bar_index:
                        patch.set(edgecolor="black", linewidth=2, color="blue")
                ax.add_artist(legend1)
                fig_dir = os.path.join(_path, class_label)
                if not os.path.exists(fig_dir):
                    os.makedirs(fig_dir)
                fig.tight_layout()
                fig.savefig(
                    os.path.join(fig_dir, "all_layers_criticality_result.pdf"),
                    bbox_inches='tight')

        list_of_layers = list()
        temp_overall_max_criticality = list()
        temp_overall_mean_criticality = list()
        temp_overall_std_criticality = list()
        temp_overall_mean_anti_criticality = list()
        temp_overall_std_anti_criticality = list()
        layers_colors_critical = list()
        layers_colors_anti_critical = list()

        for layers_name, layers_criticality in all_classes_layers_list_of_criticality.items():

            layers_criticality_mean = np.mean(np.asarray(layers_criticality)[:, 0])
            layers_criticality_std = np.std(np.asarray(layers_criticality)[:, 0])
            layers_anti_criticality_mean = np.mean(np.asarray(layers_criticality)[:, 1])
            layers_anti_criticality_std = np.std(np.asarray(layers_criticality)[:, 1])

            if layers_criticality_mean > 0.001 or layers_anti_criticality_mean < -0.001:

                temp_overall_max_criticality.append(np.max(np.asarray(layers_criticality)[:, 0]))
                temp_overall_mean_criticality.append(layers_criticality_mean)
                temp_overall_mean_anti_criticality.append(layers_anti_criticality_mean)
                temp_overall_std_criticality.append(layers_criticality_std)
                temp_overall_std_anti_criticality.append(layers_anti_criticality_std)
                list_of_layers.append(layers_name)

                if layers_criticality_mean > cri_tau:
                    layers_colors_critical.append('red')
                elif layers_criticality_mean > cri_tau / 2:
                    layers_colors_critical.append('orange')
                elif layers_criticality_mean > cri_tau / 8:
                    layers_colors_critical.append('yellow')
                else:
                    layers_colors_critical.append('green')
                layers_colors_anti_critical.append('blue')

        if 0 < len(list_of_layers) < 20:
            plt.rcParams.update({'font.size': 8})
            fig, ax = plt.subplots(figsize=(7, 5))
            ax.clear()
            ax.grid(True)
            error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
            max_bar_index = np.argmax(temp_overall_mean_criticality)
            critical_bars = ax.bar(list_of_layers, temp_overall_mean_criticality,
                                   yerr=temp_overall_std_criticality,
                                   error_kw=error_kw,
                                   alpha=0.5,
                                   color=layers_colors_critical)
            # access the bar attributes to place the text in the appropriate location
            for index, (bar, min_max) in enumerate(zip(critical_bars, temp_overall_max_criticality)):
                if index == max_bar_index:
                    bar.set(edgecolor="black", linewidth=2)
                plt.plot(bar.get_x() + 0.4, min_max, color='black', marker='*', linewidth=0.5, markersize=4)

            error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'blue'}
            ax.bar(list_of_layers, temp_overall_mean_anti_criticality,
                   yerr=temp_overall_std_anti_criticality,
                   error_kw=error_kw,
                   alpha=0.5,
                   color=layers_colors_anti_critical)
            font_size = 8
            ax.xaxis.set_ticklabels([])  # hiding all labels
            # labels = ax.get_xticklabels()
            # plt.setp(labels, rotation=45, horizontalalignment='right', fontsize=font_size)
        else:
            plt.rcParams.update({'font.size': 6})
            fig, ax = plt.subplots(figsize=(7, 3))
            ax.clear()
            error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
            max_bar_index = np.argmax(temp_overall_mean_criticality)
            critical_bars = ax.bar(list_of_layers, temp_overall_mean_criticality,
                                   yerr=temp_overall_std_criticality,
                                   error_kw=error_kw,
                                   alpha=0.5,
                                   color=layers_colors_critical)
            # access the bar attributes to place the text in the appropriate location
            for index, (bar, min_max) in enumerate(zip(critical_bars, temp_overall_max_criticality)):
                if index == max_bar_index:
                    bar.set(edgecolor="black", linewidth=2)
                plt.plot(bar.get_x() + 0.4, min_max, color='black', marker='*', linewidth=0.5, markersize=4)

            error_kw = {'elinewidth': 0.6, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'blue'}
            ax.bar(list_of_layers, temp_overall_mean_anti_criticality,
                   yerr=temp_overall_std_anti_criticality,
                   error_kw=error_kw,
                   alpha=0.5,
                   color=layers_colors_anti_critical)
            ax.xaxis.set_ticklabels([])  # hiding all labels

        # ax.set_title(
        #    "Layers\' normalized criticality for model {} and for all classes".format(_models_name),
        #    fontsize=8)
        ax.set_ylabel('Mean of normalized criticality')
        # ax.set_xlabel('Layers\' names')
        legend1 = ax.legend(custom_colors, legend_text)
        legend_headline = "Layers:"
        font_size = 6
        if len(reduces_layers) > 14:
            font_size = 5
        elif len(reduces_layers) > 18:
            font_size = 4
        elif len(reduces_layers) > 22:
            font_size = 3
        elif len(reduces_layers) > 26:
            font_size = 2
        legend2 = ax.legend(critical_bars, reduces_layers, loc="center left",
                            title=legend_headline,
                            bbox_to_anchor=(1.0, 0.5), prop={'size': font_size})
        ax.add_artist(legend1)

        fig.tight_layout()
        fig.savefig(
            os.path.join(_path, _models_name + "_all_labels_all_layers" + "_criticality_result.pdf"),
            bbox_inches='tight')

        return critical_neurons, anti_critical_neurons

    @staticmethod
    def plot_overlapping_matrix(data, labels_x, labels_y, layer, path, settings):
        plt.rcParams.update({'font.size': settings["font_size"]})
        fig = plt.figure(figsize=settings["fig_size"])
        ax = plt.gca()

        im = ax.matshow(data, interpolation='none')
        fig.colorbar(im)
        if settings["show_numbers"]:
            for (i, j), z in np.ndenumerate(data):
                ax.text(j, i, '{:2.0f}'.format(z), ha='center', va='center', size=settings["font_size"])

        ax.set_xticks(np.arange(len(labels_x)))
        ax.set_xticklabels(labels_x)
        ax.set_yticks(np.arange(len(labels_y)))
        ax.set_yticklabels(labels_y)

        # Set ticks on both sides of axes on
        ax.tick_params(axis="x", bottom=False, top=True, labelbottom=False, labeltop=True,
                       labelsize=settings["font_size"])
        ax.tick_params(axis="y", labelsize=settings["font_size"])
        # Rotate and align bottom ticklabels
        # plt.setp([tick.label1 for tick in ax.xaxis.get_major_ticks()], rotation=45,
        #         ha="right", va="center", rotation_mode="anchor")
        ## Rotate and align top ticklabels
        plt.setp([tick.label2 for tick in ax.xaxis.get_major_ticks()], rotation=45,
                 ha="left", va="center", rotation_mode="anchor")

        # ax.set_title("Correlation between anti-critical neurons", pad=10)
        # fig.tight_layout()
        plt.savefig(os.path.join(path, layer + "_correlation.pdf"), bbox_inches='tight', dpi=600)

    @staticmethod
    def find_overlapping_critical_neurons(_stat_dict, _path):

        settings = {"fig_size": (12, 10),
                    "font_size": 14,
                    "show_numbers": True}

        def nested_dict():
            return collections.defaultdict(nested_dict)

        between_label_correlation = nested_dict()

        # find overlapping neurons for all classes
        for layer, labels in _stat_dict.items():
            copy_labels_dict = copy.deepcopy(labels)
            for i, (label, critical_indices) in enumerate(labels.items()):
                if label in analyzed_labels:
                    ''' For all classes'''
                    # this is controversial since I'm removing layers without anti-critical neurons
                    list_of_indices = list(critical_indices.keys())

                    ''' Correlation between classes '''
                    for j, (copy_label, copy_of_critical_indices) in enumerate(copy_labels_dict.items()):
                        if copy_label in analyzed_labels:
                            copy_of_indices = list(copy_of_critical_indices.keys())
                            between_label_correlation[layer][label][copy_label] = set(copy_of_indices) & set(
                                list_of_indices)

        # filter all empty labels
        between_label_overlapping_filtered = nested_dict()
        for layer, overlapping_matrices in between_label_correlation.items():
            for label_is, entry_is in overlapping_matrices.items():
                not_empty = list()
                for label_js, entry_js in entry_is.items():
                    if len(entry_js) > 0:
                        not_empty.append(entry_js)

                if len(not_empty) > 0:
                    between_label_overlapping_filtered[layer] = overlapping_matrices

        if not os.path.exists(_path):
            os.makedirs(_path)

        for layer, overlapping_matrices in between_label_overlapping_filtered.items():
            all_labels = list(overlapping_matrices.keys())
            matrix_size = len(all_labels)
            overlapping_matrix = np.zeros(shape=(matrix_size, matrix_size), dtype=int)
            for i, (label_i, entry_i) in enumerate(overlapping_matrices.items()):
                for j, (label_j, entry_j) in enumerate(entry_i.items()):
                    overlapping_matrix[i][j] = len(entry_j)
            Visualization.plot_overlapping_matrix(overlapping_matrix, all_labels, all_labels, layer, _path, settings)

        return between_label_correlation

    @staticmethod
    def plot_histogram_with_err(fig_dir, _model, _class, _n_worst, list_of_worst_neurons, list_of_accuracy,
                                list_of_accuracy_stds):
        # plt.rcParams.update({'font.size': 6})
        error_kw = {'elinewidth': 0.5, 'capsize': 2, 'capthick': 0.5, 'ecolor': 'black'}
        fig, ax = plt.subplots(figsize=(7, 3))

        bars_list = ax.bar(list_of_worst_neurons, list_of_accuracy, yerr=list_of_accuracy_stds, error_kw=error_kw,
                           alpha=0.5)
        if _n_worst < 21:
            plt.rcParams.update({'font.size': 10})
            labels = ax.get_xticklabels()
            plt.setp(labels, rotation=45, horizontalalignment='right', fontsize=8)
        else:
            plt.rcParams.update({'font.size': 8})
            ax.xaxis.set_ticklabels([])  # hiding all labels
        bars_list[0].set_color("g")  # setting first bar as green - no masking
        # ax.set_title("Accuracy of most {} critical neurons for model {} and for class {}".format(_n_worst, _model, _class), fontsize=8)
        ax.set_ylabel('Accuracy')
        # ax.set_xlabel('Layers\' names')
        fig.tight_layout()
        fig.savefig(os.path.join(fig_dir, _model + "_" + _class + "_" + str(_n_worst) + "_accuracy_result.pdf"))

    @staticmethod
    def plot_CDP_dependently_masking_results(_path, _statistics_dict, _or_conf, _class, _image_name, _model_name,
                                             _number_of_neurons):

        # statistics_dict[every_layer].append({max_response_filter_index: or_conf})
        layers_names = list()
        texts = list()
        values = list()

        for index, each_layer in enumerate(tqdm(_statistics_dict.keys())):
            layers_names.append(each_layer)
            texts.append(list(_statistics_dict[each_layer][0].keys())[0])
            values.append(list(_statistics_dict[each_layer][0].values())[0])

        # differences = np.diff(np.asarray(values))
        differences = np.gradient(np.asarray(values), 2)
        np.insert(differences, 0, abs(_or_conf - values[0]) / 2.0)
        # print(differences)
        # print(differences.shape)

        plt.rcParams.update({'font.size': 10})
        fig, ax = plt.subplots()

        ax.bar(layers_names, np.asarray(values), alpha=0.5)

        for index, i in enumerate(ax.patches):
            # get_x pulls left or right; get_height pushes up or down
            ax.text(i.get_x() + .12, i.get_height() - 3, \
                    str(format(values[index], '.3f')), fontsize=10,
                    color='white')

        # plt.tick_params(labelright=True)
        # ax.set_xticks(x_pos, layers)#'vertical')
        labels = ax.get_xticklabels()
        plt.setp(labels, rotation=45, horizontalalignment='right')
        # ax.set_title("Layers\' dependently masking confidence for class: {}".format(_class))
        ax.set_ylabel('Confidence gradient')
        ax.set_xlabel('Layers\' names')
        # plt.margins(0.2)
        # Tweak spacing to prevent clipping of tick-labels
        # fig.tight_layout()
        # ax.subplots_adjust(bottom=0.25)
        fig_dir = os.path.join(_path, _model_name, _image_name, "criticality")
        if not os.path.exists(fig_dir):
            os.makedirs(fig_dir)
        fig.savefig(os.path.join(
            fig_dir + "Layers_dependently_masking_confidence_result_" + _class + str(_number_of_neurons) + ".pdf"))

    @staticmethod
    def plot_layers_responses_results(_path, _image_name, _model_name, _class, _sum_responses_dict,
                                      _parameter_dict=None):
        fig, ax = plt.subplots()

        for layer in _sum_responses_dict.keys():
            fm_sum_list = _sum_responses_dict[layer][0]
            fm_sum_np = np.asarray(fm_sum_list)
            norm_fm_sum_np = fm_sum_np / np.max(fm_sum_np)

            ''' --------------------------------------- '''
            ''' Plotting histogram of fm sums '''
            ''' --------------------------------------- '''
            # clear the previouse axis
            ax.clear()

            x_pos = np.arange(len(fm_sum_list))
            ax.barh(x_pos, norm_fm_sum_np, alpha=0.5, align='center')
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_xlabel('Neuron\'s response')
            ax.set_ylabel('Weights\' indices')
            # ax.set_title("Neurons\' reponses for class {}".format(_class))

            fig_dir = os.path.join(_path, _model_name, _class, _image_name, "responses_graph")
            if not os.path.exists(fig_dir):
                os.makedirs(fig_dir)
            fig.savefig(os.path.join(fig_dir, layer + "_graph.pdf"))

    @staticmethod
    def get_labels(legend_labels, number_of_labels=32):
        # produce a legend with the unique colors from the scatter
        legend_label = list()
        if legend_labels.shape[0] > number_of_labels:
            indices = np.linspace(0, legend_labels.shape[0] - 1, number_of_labels).astype(int)
            for i in indices:
                name = copy.deepcopy(legend_labels[i])
                for remove_str in remove_string:
                    name = name.replace(remove_str, "")
                legend_label.append(name)
        else:
            legend_label = list(legend_labels)
            number_of_labels = len(legend_label)

        return legend_label, number_of_labels

    @staticmethod
    def plot_scatter(legend_labels, headline, legend_headline, layers_kernels_criticality, labels_indices,
                     path_to_logging, layers_kernels_anticriticality=None, _parameter_dict=None):
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        spacing = 0.005

        # params = {#'legend.fontsize': 'x-large',
        # 'figure.figsize': (15, 5),
        # 'axes.labelsize': 'x-large',
        # 'axes.titlesize': 'x-large',
        #          'xtick.labelsize': 'small'}#,
        # 'ytick.labelsize': 'x-large'}
        # plt.rcParams.update(params)

        fig = plt.figure()
        # ax.axhspan(0, 0.5, facecolor='green', alpha=0.15)
        # ax.axvspan(0.5, 2.0, facecolor='white', alpha=1.0)
        # ax.axhspan(0.5, 1.0, facecolor='yellow', alpha=0.15)
        # ax.axvspan(1.0, 2.0, facecolor='white', alpha=1.0)
        # ax.axvspan(0.5, 1.0, facecolor='yellow', alpha=0.15)
        # ax.axhspan(1.0, 2.0, facecolor='white', alpha=1.0)
        # ax.axhspan(1.0, 2.0, facecolor='red', alpha=0.15)
        # ax.axvspan(1.0, 2.0, facecolor='red', alpha=0.15)
        # ax.axhspan(0, -0.5, facecolor='white', alpha=1.0)
        # ax.axhspan(0, -0.5, facecolor='blue', alpha=0.15)

        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom + height + spacing, width, 0.2]
        rect_histy = [left + width + spacing, bottom, 0.2, height]

        ax = fig.add_axes(rect_scatter)
        ax.clear()
        ax.grid(True)
        # ToDo!
        # Dont do clustering based on kMeans, but clustering based on layers
        np_array_criticality = np.asarray(layers_kernels_criticality, dtype=float)
        np_array_clusters = np.asarray(labels_indices, dtype=int)
        sizes = 55.0 * (np_array_criticality[:, 0] + np_array_criticality[:, 1])
        sizes = 1.0

        min_x = min(np_array_criticality[:, 0])
        max_x = max(np_array_criticality[:, 0])
        padding_x = (max_x - min_x) / 10.0
        min_x -= padding_x
        max_x += padding_x
        min_y = min(np_array_criticality[:, 1])
        max_y = max(np_array_criticality[:, 1])
        padding_y = (max_y - min_y) / 10.0
        min_y -= padding_y
        max_y += padding_y
        ax.set_xlim(left=min_x, right=max_x)  # std
        ax.set_ylim(bottom=min_y, top=max_y)  # mean

        ax_histx = fig.add_axes(rect_histx, sharex=ax)
        ax_histy = fig.add_axes(rect_histy, sharey=ax)

        ax_histx.tick_params(axis="x", labelbottom=False)
        ax_histy.tick_params(axis="y", labelleft=False)

        # the scatter plot:
        scatter = ax.scatter(np_array_criticality[:, 0], np_array_criticality[:, 1], c=np_array_clusters, s=sizes)
        # automatic setting of label size based on values
        if (abs(max_x) + abs(min_x)) < 0.01:
            ax.xaxis.label.set_size(4)
        ax.set_xlabel("criticality")
        ax.set_ylabel("weights l2 norm")
        # ploting the minimum value
        # y = np.linspace(min_y, max_y, 2)
        # ax.plot([min_x, min_x], y, '-g')

        # produce a legend with the unique colors from the scatter
        legend_label, number_of_labels = Visualization.get_labels(legend_labels, number_of_labels=32)

        legend1 = ax.legend(scatter.legend_elements(num=number_of_labels)[0], legend_label, loc="center right",
                            title=legend_headline,
                            bbox_to_anchor=(-0.1, 0.5), prop={'size': 6})

        # now determine nice limits by hand:
        binwidth = (max_x - min_x) / 200.0
        bins_x = np.arange(min_x, max_x, binwidth)
        hist, bins, _ = ax_histx.hist(np_array_criticality[:, 0], bins=bins_x)
        binwidth = (max_y - min_y) / 200.0
        bins_y = np.arange(min_y, max_y, binwidth)
        ax_histy.hist(np_array_criticality[:, 1], bins=bins_y, orientation='horizontal')

        # automatic middle positioning of text
        max_value_x_axis = np.max(hist)
        y_position = bins[np.argmax(max_value_x_axis) + 5]
        ax_histx.text(y_position, max_value_x_axis / 2.0, "Number of kernels: {}".format(np_array_criticality.shape[0]))

        path_to_pruning = os.path.join(path_to_logging, headline)
        if not os.path.exists(os.path.join(path_to_pruning, "neurons_clustering")):
            os.makedirs(os.path.join(path_to_pruning, "neurons_clustering"))
        plt.savefig(os.path.join(path_to_pruning, "neurons_clustering", headline + "_cluster_img.pdf"),
                    bbox_inches='tight')  # , dpi=600)
        # plt.savefig(os.path.join(path_to_logging, "neurons_clustering", headline + "_cluster_img.pdf"),
        #            bbox_inches='tight')

        # clustering according to criticality
        theta = 1e-6
        steps_criticality = np.linspace(min_x - theta, max_x + theta, 10)
        for index in range(len(steps_criticality) - 1):
            indices = np.argwhere(
                (steps_criticality[index] < np_array_criticality[:, 0]) & (
                        np_array_criticality[:, 0] < steps_criticality[index + 1]))

            if len(indices) > 0:
                temp_criticality = np_array_criticality[indices, 0]
                temp_y_axis = np_array_criticality[indices, 1]
                minimum_weight = np.min(temp_y_axis)
                temp_clusters = np_array_clusters[indices]
                temp_legend_labels = legend_labels[np.unique(temp_clusters) - 1]
                sizes = 55.0 * (temp_criticality + temp_y_axis)
                plt.clf()
                plt.grid(True)
                sub_scatter = plt.scatter(temp_criticality, temp_y_axis, c=temp_clusters, s=sizes)
                maximum_weight = np.max(temp_y_axis)
                minimum_cri = np.min(temp_criticality)
                plt.text(minimum_cri, maximum_weight, "min L1: {}".format(minimum_weight))
                legend_label, number_of_labels = Visualization.get_labels(temp_legend_labels, number_of_labels=32)
                plt.legend(sub_scatter.legend_elements(num=number_of_labels)[0], legend_label, loc="center right",
                           title=legend_headline,
                           bbox_to_anchor=(-0.1, 0.5), prop={'size': 6})

                if not os.path.exists(os.path.join(path_to_pruning, "clustering_criticality")):
                    os.makedirs(os.path.join(path_to_pruning, "clustering_criticality"))
                plt.savefig(os.path.join(path_to_pruning, "clustering_criticality",
                                         headline + "_" + str(steps_criticality[index]) + "-" + str(
                                             steps_criticality[index + 1]) + "_cluster_img.pdf"),
                            bbox_inches='tight')  # , dpi=600)

        # clustering according to weights
        steps_weights = np.linspace(min_y - theta, max_y + theta, 10)
        for index in range(len(steps_weights) - 1):
            indices = np.argwhere(
                (steps_weights[index] < np_array_criticality[:, 1]) & (
                        np_array_criticality[:, 1] < steps_weights[index + 1]))

            if len(indices) > 0:
                temp_criticality = np_array_criticality[indices, 0]
                temp_y_axis = np_array_criticality[indices, 1]
                minimum_weight = np.min(temp_y_axis)
                temp_clusters = np_array_clusters[indices]
                temp_legend_labels = legend_labels[np.unique(temp_clusters) - 1]
                sizes = 55.0 * (temp_criticality + temp_y_axis)
                plt.clf()
                plt.grid(True)
                sub_scatter = plt.scatter(temp_criticality, temp_y_axis, c=temp_clusters, s=sizes)
                maximum_weight = np.max(temp_y_axis)
                minimum_cri = np.min(temp_criticality)
                plt.text(minimum_cri, maximum_weight, "min L1: {}".format(minimum_weight))
                legend_label, number_of_labels = Visualization.get_labels(temp_legend_labels, number_of_labels=32)
                plt.legend(sub_scatter.legend_elements(num=number_of_labels)[0], legend_label, loc="center right",
                           title=legend_headline,
                           bbox_to_anchor=(-0.1, 0.5), prop={'size': 6})

                if not os.path.exists(os.path.join(path_to_pruning, "clustering_weights")):
                    os.makedirs(os.path.join(path_to_pruning, "clustering_weights"))
                plt.savefig(os.path.join(path_to_pruning, "clustering_weights",
                                         headline + "_" + str(steps_weights[index]) + "-" + str(
                                             steps_weights[index + 1]) + "_cluster_img.pdf"),
                            bbox_inches='tight')  # , dpi=600)

        # correlation qoeficient
        clusters = np.unique(np_array_clusters) - 1
        temp_legend_labels = legend_labels[np.unique(clusters) - 1]
        import torch

        x = (np.asarray(np_array_criticality[:, 2]) - np.min(np.asarray(np_array_criticality[:, 2]))) / np.ptp(
            np.asarray(np_array_criticality[:, 2]))  # std of criticality
        l1_norm_list = list()
        for layer, kernel_index in np.asarray(layers_kernels_anticriticality)[:, :2]:
            layers_weights = _parameter_dict[layer][0]
            for enum_index, kernel_weight in enumerate(layers_weights):
                if enum_index == int(kernel_index):
                    l1_norm_list.append(torch.norm(kernel_weight, p=1).cpu().detach().numpy())
        y = (np.asarray(l1_norm_list) - np.min(np.asarray(l1_norm_list))) / np.ptp(
            np.asarray(l1_norm_list))  # l1 kernel norm
        from scipy import stats
        r = stats.pearsonr(x, y)
        print("Correlation between std of criticality and l1 norm: {}".format(r))

        # from sklearn.cluster import KMeans
        # kmeans = KMeans(n_clusters=2, random_state=0).fit(X)
        # kmeans.fit_predict(X)

        sizes = (1.0 / np.std(x * y)) * (x * y)
        # sizes = np.clip(sizes, 1.0, 100.0)
        plt.clf()
        plt.grid(True)
        plt.scatter(x, y, s=sizes)
        plt.text(minimum_cri, maximum_weight, "Correlation factor: {:0.5f}".format(float(r[0])))
        plt.xlabel("Criticality std")
        plt.ylabel("Kernels l1-norm")
        plt.savefig(os.path.join(path_to_pruning, headline + "_l1_std_scatter_img.pdf"),
                    bbox_inches='tight')  # , dpi=600)

        # clustering according to classes
        fig_cluster = plt.figure()
        ax_cluster = fig_cluster.add_axes(rect_scatter)
        ax_cluster.clear()
        ax_cluster.grid(True)
        ax_cl_histy = fig_cluster.add_axes(rect_histy, sharey=ax_cluster)
        ax_cl_histy.tick_params(axis="y", labelleft=False)
        binwidth = (max_y - min_y) / 200.0
        bins_y = np.arange(min_y, max_y, binwidth)

        for cluster, title in zip(clusters, temp_legend_labels):
            indices = np.argwhere(cluster == np_array_clusters)

            if len(indices) > 0:
                layers_weights = _parameter_dict[title][0]
                l1_norm_list = list()
                for kernel_weight in layers_weights:
                    l1_norm_list.append(torch.norm(kernel_weight, p=1).cpu().detach().numpy())

                temp_criticality = np_array_criticality[indices, 0]
                temp_y_axis = np_array_criticality[indices, 1]
                minimum_weight = np.min(temp_y_axis)
                temp_clusters = np_array_clusters[indices]
                # temp_legend_labels = legend_labels[cluster]
                sizes = 10.0 * (temp_criticality + temp_y_axis)
                ax_cluster.clear()

                sub_scatter = ax_cluster.scatter(temp_criticality, temp_y_axis, c=temp_clusters, s=sizes)
                maximum_weight = np.max(temp_y_axis)
                minimum_cri = np.min(temp_criticality)
                ax_cluster.text(minimum_cri, maximum_weight, "min L1: {}".format(minimum_weight))
                ax_cluster.text(minimum_cri, max_y,
                                'N_k: {} \n N_ak: {}'.format(str(len(l1_norm_list)), str(len(temp_criticality))),
                                bbox=dict(facecolor='blue', alpha=0.5))
                # legend_label, number_of_labels = Visualization.get_labels(temp_legend_labels, number_of_labels=32)
                # plt.legend(sub_scatter.legend_elements(num=number_of_labels)[0], legend_label, loc="center right",
                #           title=legend_headline,
                #           bbox_to_anchor=(-0.1, 0.5), prop={'size': 6})
                # plt.xlim( min_x, max_x)
                # ax_histy.set_ylim(min_y, max_y)

                ax_cl_histy.clear()
                ax_cl_histy.hist(np.asarray(l1_norm_list), bins=bins_y, orientation='horizontal')
                ax_cl_histy.hist(np.asarray(temp_y_axis), bins=bins_y, orientation='horizontal')

                if not os.path.exists(os.path.join(path_to_pruning, "clustering_labels")):
                    os.makedirs(os.path.join(path_to_pruning, "clustering_labels"))
                plt.savefig(os.path.join(path_to_pruning, "clustering_labels",
                                         str(title) + "_cluster_img.pdf"),
                            bbox_inches='tight')  # , dpi=600)

    @staticmethod
    def plot_models_ap_criticality_correlation(models_name, class_label, criticalities, path):
        # ploting models layers criticality
        plt.rcParams.update({'font.size': 10})
        fig, ax = plt.subplots(figsize=(9.5, 4))
        width = 0.35  # the width of the bars
        max_bars = 10

        ax.clear()
        ax.grid(True)
        x_labels = list()
        y_values = list()
        colors = list()
        bars = None
        not_masked_ap = 0.0

        # joined_ap_statistics_dict[anti_or_worse][label][label][layer] = mAPs_list[0]
        for anti_or_worse, labels in criticalities.items():
            for label, layers_kernels in labels.items():
                if label == class_label:
                    colors_temp = list()
                    x_labels_temp = list()
                    not_masked_ap = layers_kernels["not_masked"]
                    if anti_or_worse == "anti":
                        sorted_dict = dict(sorted(layers_kernels.items(), key=lambda item: item[1], reverse=True))
                    else:
                        sorted_dict = dict(sorted(layers_kernels.items(), key=lambda item: item[1]))
                    for layers_kernel, ap_per_class in sorted_dict.items():
                        if layers_kernel != "not_masked":
                            if ap_per_class < (not_masked_ap / 10.0):
                                colors_temp.append('red')
                            elif ap_per_class < (not_masked_ap / 5.0):
                                colors_temp.append('orange')
                            elif ap_per_class < (not_masked_ap / 2.0):
                                colors_temp.append('yellow')
                            elif ap_per_class < (not_masked_ap):
                                colors_temp.append('green')
                            else:
                                colors_temp.append('blue')

                    remove_string = ["module_list.", ".weight", "body."]
                    for layers_name in sorted_dict.keys():
                        name = copy.deepcopy(layers_name)
                        for remove_str in remove_string:
                            name = name.replace(remove_str, "")
                        x_labels_temp.append(name)
                    y_values_temp = [value for layers_kernel, value in sorted_dict.items()]

                    x_labels.append(x_labels_temp[:max_bars])
                    y_values.append(y_values_temp[:max_bars])
                    colors.append(colors_temp[:max_bars])

        x = np.arange(len(x_labels[0]))  # the label locations
        rects1 = ax.bar(x - width / 2, y_values[0], width, alpha=0.5, color=colors[0])
        rects2 = ax.bar(x + width / 2, y_values[1], width, alpha=0.5, color=colors[1])
        ax.axhline(not_masked_ap, ls='--', alpha=1.0, color='black', linewidth=1.0)

        font_size = 8
        plt.rcParams.update({'font.size': 10})

        from matplotlib.lines import Line2D
        custom_colors = [Line2D([0], [0], color="black", ls='--', linewidth=1.0)]
        legend_text = ["Not masked AP"]
        legend0 = ax.legend(custom_colors, legend_text, loc='center right')
        legend_headline = "Anti-critical masked kernels:"
        legend1 = ax.legend(rects1, x_labels[0], title=legend_headline,
                            bbox_to_anchor=(1.0, 0.5), prop={'size': font_size})
        # legend1 = ax.legend(custom_colors, legend_text, loc="center left",
        #                      title=legend_headline,
        #                      bbox_to_anchor=(1.0, 0.5), prop={'size': font_size})

        legend_headline = "Most-critical masked kernels:"
        legend2 = ax.legend(rects2, x_labels[1], title=legend_headline,
                            bbox_to_anchor=(1.0, 1.1), prop={'size': font_size})
        ax.add_artist(legend1)
        ax.add_artist(legend0)

        # labels = ax.get_xticklabels()
        # plt.setp(labels, rotation=45, horizontalalignment='right')
        plt.rcParams.update({'font.size': 10})
        ax.set_title(
            "AP stability test for model: {}, and class: {}".format(models_name, class_label))
        ax.set_ylabel('Average Precision')
        ax.set_xlabel('Layers\' names and kernel index')
        ax.xaxis.set_ticklabels([])  # hiding all labels

        # legend1 = ax.legend(custom_colors, legend_text)

        # Tweak spacing to prevent clipping of tick-labels
        fig.tight_layout()
        # ax.subplots_adjust(bottom=0.25)
        fig.savefig(os.path.join(path, class_label + "_AP_stability_result.pdf"), bbox_inches='tight')
