# https://colab.research.google.com/github/facebookresearch/detr/blob/colab/notebooks/detr_demo.ipynb
import os
import copy
import collections
import json
import numpy as np

import torch
import torchvision.transforms as T

from utils.general import non_max_suppression, scale_coords
from scripts.metrics.mean_avg_precision import mean_average_precision
import settings
from scripts.functions import xyxy2xywh, xywh2xyxy

torch.set_grad_enabled(False)

# standard PyTorch mean-std input image normalization
transform = T.Compose([
    T.Resize(800),
    T.ToTensor(),
    T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])


# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)


def rescale_bboxes(out_bbox, size):
    img_h, img_w = size
    b = box_cxcywh_to_xyxy(out_bbox)
    #b = b * torch.FloatTensor([img_w, img_h, img_w, img_h])
    b = b * torch.cuda.FloatTensor([img_w, img_h, img_w, img_h])
    return b


class DETRWrapper:
    def __init__(self, model):
        self.model = model

    def predict(self, batch, prediction_thr, dataset, pred_bboxes, pred_classes_ids, pred_labels, pred_scores):
        # mean-std normalize the input image (batch-size: 1)
        # img = transform(im).unsqueeze(0)

        # demo model only support by default images with aspect ratio between 0.5 and 2
        # if you want to use images with an aspect ratio outside this range
        # rescale your image so that the maximum size is at most 1333 for best results
        assert batch[0].shape[-2] <= 1600 and batch[0].shape[
            -1] <= 1600, 'demo model only supports images up to 1600 pixels on each side'

        # propagate through the model
        outputs = self.model(batch)

        pred_logits = outputs['pred_logits'].softmax(-1)[:, :, :-1]
        pred_boxes = outputs['pred_boxes']
        for probabilities, pred_boxes in zip(pred_logits, pred_boxes):
            # keep only predictions with 0.7+ confidence
            keep = probabilities.max(-1).values > prediction_thr

            # convert boxes from [0; 1] to image scales
            bboxes_scaled = rescale_bboxes(pred_boxes[keep], batch.shape[2:]).detach().cpu().tolist()
            classes_ids = probabilities[keep].argmax(axis=-1).detach().cpu().tolist()

            if len(probabilities[keep].detach().cpu().tolist()) == 0:
                pred_scores.append(probabilities[keep].detach().cpu().tolist())
            else:
                pred_scores.append(probabilities[keep].detach().cpu().tolist()[0])
            pred_bboxes.append(bboxes_scaled)
            pred_labels.append(classes_ids)
            pred_classes_ids.append([dataset.labels[classes_ids[i]] for i in range(len(classes_ids))])


    def predict_and_calculate_mean_average_precision(self, dataset, path):
        # https://pytorch.org/vision/stable/models.html#object-detection-instance-segmentation-and-person-keypoint-detection
        # fasterrcnn_resnet50_fpn - 37.0
        # ssdlite320_mobilenet_v3_large - 21.3
        # https://jonathan-hui.medium.com/map-mean-average-precision-for -object - detection - 45c121a31173

        text_file = open(os.path.join(path, "mAP_output.txt"), "w")

        def xywh2xyxy(x):
            # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
            y = copy.deepcopy(x)
            y[0] = x[0]  # - x[:, 2] / 2  # top left x
            y[1] = x[1]  # - x[:, 3] / 2  # top left y
            y[2] = x[0] + x[2]  # bottom right x
            y[3] = x[1] + x[3]  # bottom right y
            return y

        def nested_dict():
            return collections.defaultdict(nested_dict)

        dict_average_precisions = nested_dict()
        list_of_average_precisions = list()
        nan_classes = [0, 12, 26, 29, 30, 45, 66, 68, 69, 71, 83]
        # COCO AP - AP @ [.50: .05:.95]:
        list_of_true_boxes = list()
        list_of_predictions = list()
        list_of_distances = list()

        with torch.no_grad():
            for counter, (image_indices, image, gts, object_distances) in enumerate(dataset.dataset_iterator()):
                pred_bboxes, pred_labels, pred_classes_ids, pred_scores = list(), list(), list(), list()
                self.predict(image, 0.5, pred_bboxes, pred_labels, pred_classes_ids, pred_scores)

                for gt, pred_bb, pred_lab, pred_id, pred_score, index, distances in \
                        zip(gts, pred_bboxes, pred_labels, pred_classes_ids, pred_scores, image_indices, object_distances):
                    # Removing N/A and __background__ classes from calculation
                    for bb, id, score in zip(pred_bb, pred_id, pred_score):
                        # if id not in nan_classes:
                        list_of_predictions.append([index, id, score, bb])

                    for entry in gt:
                        # if entry['category_id'] not in nan_classes:
                        # @FixMe add the possibility of other datasets
                        if "2d_bbox" in entry.keys():
                            # convert the xyxy to xywh to be compatible with the area calculation
                            true_boxes = entry["2d_bbox"]
                        else:
                            true_boxes = xywh2xyxy(entry["bbox"])

                        if "class" in entry.keys():
                            object_cls = entry["class"]
                            object_id = dataset.labels.index(object_cls)
                        else:
                            object_id = entry["category_id"]
                            object_cls = dataset.labels[object_id]

                        list_of_true_boxes.append([index, object_id, 1.0, true_boxes])

                    for distance in distances:
                        list_of_distances.append(distance)

        for iou_threshold in settings.iou_thresholds:
            average_precision = mean_average_precision(list_of_predictions, list_of_true_boxes, dict_average_precisions,
                                                       iou_threshold=iou_threshold, box_format="corners",
                                                       num_classes=len(dataset.labels))
            average_precision_percentage = average_precision.item() * 100.0
            list_of_average_precisions.append(average_precision_percentage)
            text_file.write("AP {:.2f} for iou {}\n".format(average_precision_percentage, iou_threshold))

        average_precision = sum(list_of_average_precisions) / float(len(list_of_average_precisions))
        print("Final AP Box for all images: {:.2f}".format(average_precision))
        text_file.write("Final AP Box for all images.\n")
        text_file.write("{:.2f} \n".format(average_precision))
        text_file.close()

        # AP[0.5,0.95,0.05]
        ap_per_class_matrix = np.zeros(shape=(len(list(dict_average_precisions.keys())), len(settings.iou_thresholds)), dtype=float)

        for index_label, (label, ious) in enumerate(dict_average_precisions.items()):
            for index_iou, (iou, values) in enumerate(ious.items()):
                ap_per_class_matrix[index_label][index_iou] = values[0]

        return ap_per_class_matrix, average_precision, dict_average_precisions
