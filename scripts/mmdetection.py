from mmengine import Config
from mmdet.apis import init_detector, inference_detector
import mmcv

import os
import numpy as np
import settings


class MMDetWrapper:

    def __init__(self, models_name, device, dataset):

        self.models_name = models_name
        self.device = device
        self.path_to_models = os.path.join(settings.path_to_models)
        self.path_to_configs = os.path.join("data", "configs")

        if self.models_name == "yolox-l":
            config_file = os.path.join(self.path_to_configs, "yolox", "yolox_l_8xb8-300e_" + dataset.name + ".py")
            checkpoint_file = os.path.join(self.path_to_models, dataset.name, "yolo_x", "epoch_42.pth")
            cfg = Config.fromfile(config_file)
            # modify num classes of the model in box head
            if "bbox_head" in cfg.model:
                cfg.model.bbox_head.num_classes = dataset.num_classes
            else:
                raise Exception("Unknown detector structure! Cannot set number of classes")

        elif self.models_name == "detr_r50":
            config_file = os.path.join(self.path_to_configs, "detr", "detr_r50_8xb2_150e_" + dataset.name + ".py")
            checkpoint_file = os.path.join(self.path_to_models, dataset.name, "detr_r50", "epoch_55.pth")
            cfg = Config.fromfile(config_file)
            # modify num classes of the model in box head
            if "bbox_head" in cfg.model:
                cfg.model.bbox_head.num_classes = dataset.num_classes
            else:
                raise Exception("Unknown detector structure! Cannot set number of classes")

        elif self.models_name == "faster-rcnn_r50":
            config_file = os.path.join(self.path_to_configs, "faster_rcnn", "faster-rcnn_r50-caffe_fpn_ms-3x_" + dataset.name + ".py")
            checkpoint_file = os.path.join(self.path_to_models, dataset.name, "faster_rcc_r50", "epoch_30.pth")
            cfg = Config.fromfile(config_file)

            # modify num classes of the model in box head
            if "roi_head" in cfg.model:
                cfg.model.roi_head.bbox_head.num_classes = dataset.num_classes
            elif "bbox_head" in cfg.model:
                cfg.model.bbox_head.num_classes = dataset.num_classes
            else:
                raise Exception("Unknown detector structure! Cannot set number of classes")

        else:
            raise Exception("Unknown detector name!")

        self.model = init_detector(cfg, checkpoint_file, device=device)
        self.model.CLASSES = dataset.num_classes

        # Convert the model into evaluation mode
        self.model.eval()
        self.visualize = False

    def predict(self,
                batch,
                detection_threshold,
                dataset,
                pred_bboxes, pred_classes_ids, pred_labels, pred_scores):

        # for mmdet it has be batch of images path
        results = inference_detector(self.model, batch)
        outputs = results[0].pred_instances

        if outputs.scores.shape[0] > 0:
            scores = np.asarray(np.vstack(outputs.scores))
            ind = scores > detection_threshold
            pred_scores.append(scores[ind[:, 0]].tolist())
            pred_bboxes.append(np.asarray(np.vstack(outputs.bboxes))[ind[:, 0]].tolist())
            classes_ids = np.asarray(np.vstack(outputs.labels))[ind[:, 0]].tolist()
            pred_classes_ids.append(classes_ids)
            pred_labels.append([self.model.dataset_meta["classes"][i[0]] for i in classes_ids])

            if self.visualize:
                image = mmcv.imread(batch[0])
                from mmdet.registry import VISUALIZERS
                # init the visualizer(execute this block only once)
                visualizer = VISUALIZERS.build(self.model.cfg.visualizer)
                # the dataset_meta is loaded from the checkpoint and
                # then pass to the model in init_detector
                visualizer.dataset_meta = self.model.dataset_meta

                # show the results
                visualizer.add_datasample(
                    'result',
                    image,
                    data_sample=results[0],
                    draw_gt=False,
                    wait_time=0,
                    out_file='visualize/result.png'  # optionally, write to output file
                )
                visualizer.show()


