import copy
import os
import json
from tqdm import tqdm
import collections
import torch
import itertools
import numpy as np
import pandas as pd
import cv2

import scripts.functions as functions
from scripts.safety import SafetyAlgorithm
import settings
import scripts.visualization as visualization


def nested_dict():
    return collections.defaultdict(nested_dict)


ap_labels = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
'''
### 1) Iterating over images
'''


def predict_on_dataset(_boxes, _classes, _labels, _scores):
    ground_truths = list()
    images_cnt = list()
    images_shape = list()

    for counter, (image_indices, images, gts) in enumerate(dataset.dataset_iterator()):
        ground_truths.append(gts)
        images_cnt.append(image_indices)
        images_shape.append([images.data.shape] * len(image_indices))

        if models_name == "yolov5":
            model.predict_yolov5(images, 0.5, (images.data.shape[1], images.data.shape[2]), _boxes, _classes, _labels,
                                 _scores)
        else:
            model.predict(images, 0.5, _boxes, _classes, _labels, _scores)

    ground_truths_flat = list(itertools.chain.from_iterable(ground_truths))
    images_counter_flat = list(itertools.chain.from_iterable(images_cnt))
    images_shape_flat = list(itertools.chain.from_iterable(images_shape))
    return ground_truths_flat, images_shape_flat, images_counter_flat


def predict_on_dataset_with_saving(temp_path_logging_img):
    if not os.path.exists(temp_path_logging_img):
        os.makedirs(temp_path_logging_img)

    for image_indices, images, gts in dataset.dataset_iterator():

        boxes = list()
        classes = list()
        labels = list()
        scores = list()

        if models_name == "yolov5":
            model.predict_yolov5(images, 0.5, (images.data.shape[1], images.data.shape[2]), boxes, classes, labels,
                                 scores)
        else:
            model.predict(images, 0.5, boxes, classes, labels, scores)

        for image_counter, image, box, cls, label in zip(image_indices, images, boxes, classes, labels):
            labeled_image = functions.draw_box(box, cls, label,
                                               image,
                                               dataset.color_maps,
                                               line=3)
            cv2.imwrite(
                os.path.join(temp_path_logging_img, str(image_counter) + "_prediction.jpg"), labeled_image)


'''
### 4) Iterating over predictions
'''


def evaluate_criticality(_gts, _image_size, _images_counter, _layer_dict, _kernel_index,
                         _boxes_m, _classes_m, _labels_m, _scores_m,
                         _boxes, _classes, _labels, _scores):
    for box_m, cls_m, label_m, score_m, box, cls, label, score, gt, im_id, im_size in \
            zip(_boxes_m, _classes_m, _labels_m, _scores_m, _boxes, _classes, _labels, _scores, _gts, _images_counter,
                _image_size):

        tp_list, iou_list, conf_list, fp_list, gt_labels = functions.calculate_true_boxes(box,
                                                                                          im_size[3],
                                                                                          im_size[2],
                                                                                          gt,
                                                                                          dataset,
                                                                                          label,
                                                                                          score)

        tp_list_m, iou_list_m, conf_list_m, fp_list_m, gt_labels_m = functions.calculate_true_boxes(
            box_m,
            im_size[3],
            im_size[2],
            gt,
            dataset,
            label_m,
            score_m)
        # Calculate criticality
        cr_tp_fp, cr_fp, cr_sum = safety.criticality_per_frame(gt_labels,
                                                               tp_list,
                                                               tp_list_m,
                                                               iou_list,
                                                               iou_list_m,
                                                               conf_list,
                                                               conf_list_m,
                                                               fp_list,
                                                               fp_list_m)
        for filtered_labels in cr_sum.keys():
            _layer_dict[filtered_labels][str(_kernel_index)][str(im_id)] = copy.deepcopy(cr_sum[filtered_labels])


def save_ap(_dataset, _dict_average_precisions, _ap_statistics_dict, _class_indices, _text, _number_of_turns):
    for class_index, ious in _dict_average_precisions.items():
        if class_index in _class_indices:
            ious_aps = [value for iou, value in ious.items()]
            ap = float(sum(ious_aps) / len(ious_aps))
            _ap_statistics_dict[str(_number_of_turns)][_dataset.labels[int(class_index)]][_text] = [ap]
            print("AP for class {}, is {:.2f}".format(_dataset.labels[int(class_index)], ap))


def create_corelation_tab(_stats_dict, _layers_dict, _parameter_dict, _path_to_logging):
    stats_dict = nested_dict()

    for turns, labels in _stats_dict.items():
        for label, layers_and_indices in labels.items():

            if label in settings.global_analyzed_classes:
                for layer_and_index, mAP in layers_and_indices.items():
                    if layer_and_index != "not_masked": # and int(turns) < 2:
                        anticritical_layer = layer_and_index.split("__")[0]
                        kernels_index = int(layer_and_index.split("__")[1])
                        index = list(_parameter_dict.keys()).index(anticritical_layer)
                        # l1-norm of weights and criticality
                        layers_weights = _parameter_dict[anticritical_layer][0]
                        kernel_weight = layers_weights[kernels_index]
                        l1_norm = torch.norm(kernel_weight, p=1).cpu().detach().numpy()
                        l2_norm = torch.norm(kernel_weight, p=2).cpu().detach().numpy()

                        # layers_dict[str(number_of_turns)][label][layers_name][str(kernel)] = \
                        #    [str(np.mean(test_set_criticalities)), str(np.std(test_set_criticalities))]
                        mean_criticality = float(_layers_dict[turns][label][anticritical_layer][str(kernels_index)][0])
                        stats_dict[anticritical_layer][index][turns][kernels_index] = [mAP[0], mean_criticality,
                                                                                       float(l2_norm)]

    df = pd.DataFrame.from_records(
        [
            (level1, level2, level3, level4, leaf[0], leaf[1], leaf[2])
            for level1, level2_dict in stats_dict.items()
            for level2, level3_dict in level2_dict.items()
            for level3, level4_dict in level3_dict.items()
            for level4, leaf in level4_dict.items()
        ],
        columns=['Layer', 'Layers index', 'Turn', 'kernel index', 'mAP', 'mean_criticality', 'weight sum']
    )
    # df = pd.DataFrame(stats_dict.items())
    df.to_excel(os.path.join(_path_to_logging, "output.xlsx"))

    for turns, labels in _layers_dict.items():

        layers_kernels_anticriticality = list()
        layers_kernels_criticality = list()

        anticritical_layers = list()
        anticritical_layers_index = 0

        critical_layers = list()
        critical_layers_index = 0

        for label, layers_and_indices in labels.items():

            if label in settings.global_analyzed_classes:
                for layers, kernels in layers_and_indices.items():
                    for kernels_indices, ciriticality in kernels.items():
                        # l1-norm of weights and criticality
                        layers_weights = _parameter_dict[layers][0]
                        kernel_weight = layers_weights[int(kernels_indices)]
                        l1_norm = torch.norm(kernel_weight, p=1).cpu().detach().numpy()
                        l2_norm = torch.norm(kernel_weight, p=2).cpu().detach().numpy()

                        # layers_dict[str(number_of_turns)][label][layers_name][str(kernel)] = \
                        #    [str(np.mean(test_set_criticalities)), str(np.std(test_set_criticalities))]
                        abs_criticality = float(ciriticality[0]) - (float(ciriticality[1]) / 2.0)
                        abs_criticality = float(ciriticality[0])
                        if abs_criticality > 0.0:
                            if layers not in critical_layers:
                                critical_layers.append(layers)
                                critical_layers_index += 1
                            layers_kernels_criticality.append(np.asarray([layers, kernels_indices,
                                                                          float(ciriticality[0]),
                                                                          float(l2_norm),
                                                                          float(ciriticality[1]),
                                                                          float(ciriticality[0]),
                                                                          float(l1_norm),
                                                                          float(ciriticality[1]),
                                                                          critical_layers_index]))
                        # std / 2 would mean still more than 75% samples should be anticritical
                        abs_criticality = float(ciriticality[0]) + (float(ciriticality[1]) / 2.0)
                        abs_criticality = float(ciriticality[0])
                        if abs_criticality < -0.004 and l1_norm > 3.0:
                            if layers not in anticritical_layers:
                                anticritical_layers.append(layers)
                                anticritical_layers_index += 1
                            layers_kernels_anticriticality.append(np.asarray([layers, kernels_indices,
                                                                              float(ciriticality[0]),
                                                                              float(l1_norm),
                                                                              float(ciriticality[1]),
                                                                              float(ciriticality[0]),
                                                                              float(l1_norm),
                                                                              float(ciriticality[1]),
                                                                              anticritical_layers_index]))

        legend_labels = list(_layers_dict[turns]["person"].keys())
        legend_headline = "Kernels layers turn: " + turns

        # headline = "Criticality vs. weights l2 norm"
        # visualization.Visualization.plot_scatter(np.asarray(critical_layers),
        #                                         headline, legend_headline,
        #                                         np.asarray(layers_kernels_criticality)[:, 2:4],
        #                                         np.asarray(layers_kernels_criticality)[:, 6],
        #                                         _path_to_logging)

        # headline = "Anticriticality vs. weights l2 norm"
        # visualization.Visualization.plot_scatter(np.asarray(anticritical_layers),
        #                                          headline, legend_headline,
        #                                          np.asarray(layers_kernels_anticriticality)[:, 2:4],
        #                                          np.asarray(layers_kernels_anticriticality)[:, 6],
        #                                          _path_to_logging)

        headline = "criticality"
        visualization.Visualization.plot_scatter(np.asarray(critical_layers),
                                                 headline, legend_headline,
                                                 np.asarray(layers_kernels_criticality)[:, 5:8],
                                                 np.asarray(layers_kernels_criticality)[:, 8],
                                                 _path_to_logging,
                                                 layers_kernels_criticality,
                                                 _parameter_dict)

        headline = "anticriticality"
        visualization.Visualization.plot_scatter(np.asarray(anticritical_layers),
                                                 headline, legend_headline,
                                                 np.asarray(layers_kernels_anticriticality)[:, 5:8],
                                                 np.asarray(layers_kernels_anticriticality)[:, 8],
                                                 _path_to_logging,
                                                 layers_kernels_anticriticality,
                                                 _parameter_dict)

    # TBD - corelation between weights and anticritical layers indices
    return anticritical_layers


def continue_analysis(_model, _path_to_save_data, _parameter_dict, mask_previouse_layers=False, analyse_only_critical=False):
    def defaultdict_from_dict(d):
        ni = nested_dict()
        ni.update(d)
        return ni

    with open(os.path.join(_path_to_save_data, "2022-04-14-08:54_class_layer_anticritical_index_mAP_dict.json"),
              'r') as json_statistics:
        _ap_statistics_dict = json.load(json_statistics, object_hook=defaultdict_from_dict)

    with open(os.path.join(path_to_save_data, "2022-04-14-08:54_class_layer_criticality_index_dict.json"),
              'r') as json_statistics_file:
        _layers_dict = json.load(json_statistics_file, object_hook=defaultdict_from_dict)

    _only_anti_critical_layers = list()
    if analyse_only_critical:
        _only_anti_critical_layers = create_corelation_tab(_ap_statistics_dict, _layers_dict, _parameter_dict,
                                                          path_to_save_data)

    test_mask_l1 = False
    if test_mask_l1:
        for layer, kernel_index, criticaliy, l1_norm in np.asarray(only_anti_critical_layers)[:, :4]:
            if float(l1_norm) > 15.0:
                # 1) mask the kernel
                safety.mask_kernel(_parameter_dict[layer][0].data, int(kernel_index))
                # 2) calculate AP
                ap_per_class_matrix, average_precision, dict_average_precisions = \
                    _model.predict_and_calculate_mean_average_precision(
                        path_to_save_data,
                        models_name,
                        ap_labels)

    _anti_critical_layers = list()
    for turns, labels in _ap_statistics_dict.items():
        for label, layers_and_indices in labels.items():
            if label in settings.global_analyzed_classes:
                for layer_and_index in layers_and_indices.keys():
                    if layer_and_index != "not_masked":
                        anticritical_layer = layer_and_index.split("__")[0]
                        _anti_critical_layers.append(anticritical_layer)
                        kernels_index = int(layer_and_index.split("__")[1])

                        if mask_previouse_layers:
                            for layers_name, param in _parameter_dict.items():
                                if anticritical_layer == layers_name:
                                    safety.mask_kernel(param[0].data, kernels_index)
                                    break

    _number_of_turns = int(list(_ap_statistics_dict.keys())[-1])
    last_layer = list(_layers_dict[str(_number_of_turns)][settings.global_analyzed_classes[0]].keys())[-1]
    layers_keys = list(_parameter_dict.keys())
    layers_index = layers_keys.index(last_layer)
    _first_layer = layers_keys[layers_index+1]

    return _ap_statistics_dict, _layers_dict, _first_layer, _anti_critical_layers, _number_of_turns, _only_anti_critical_layers


if __name__ == '__main__':
    from datetime import datetime

    boxes, classes, labels, scores = list(), list(), list(), list()
    boxes_m, classes_m, labels_m, scores_m = list(), list(), list(), list()
    criticalitiy_means = list()
    criticalitiy_stds = list()

    for models_name in settings.models_names:

        todays_date = datetime.today().strftime('%Y-%m-%d-%H:%M')

        path_to_save_data = os.path.join(settings.path_to_logging_file, models_name, "pruning")
        if not os.path.exists(path_to_save_data):
            os.makedirs(path_to_save_data)

        model, parameter_dict, dataset = settings.take_a_model(models_name, device)

        # if torch.cuda.device_count() > 1:
        #    print("Let's use", torch.cuda.device_count(), "GPUs!")
        #    model = torch.nn.DataParallel(model)
        safety = SafetyAlgorithm(model, dataset)

        class_indices = [str(dataset.labels.index(cls)) for cls in settings.global_analyzed_classes]

        ap_statistics_dict = nested_dict()

        with torch.no_grad():

            #predict_on_dataset_with_saving(os.path.join(path_to_save_data, "images_original"))
            mask_previouse_layers = False
            analyse_only_critical = False

            if not os.path.isfile(
                    os.path.join(path_to_save_data, "None")):#"2022-04-14-08:54_class_layer_criticality_index_dict.json")):

                ap_per_class_matrix, average_precision, dict_average_precisions = \
                    model.predict_and_calculate_mean_average_precision(
                        path_to_save_data,
                        models_name,
                        ap_labels)

                ap_statistics_dict["1"]["all"]["not_masked"] = [average_precision]

                save_ap(dataset, dict_average_precisions, ap_statistics_dict, class_indices, "not_masked", "1")

                layers_dict = nested_dict()
                first_layer = list(parameter_dict.keys())[0]
                only_anti_critical_layers = list(parameter_dict.keys())
                anti_critical_layers = list()
                number_of_turns = 1
            else:
                ap_statistics_dict, layers_dict, first_layer, anti_critical_layers, number_of_turns, only_anti_critical_layers = \
                    continue_analysis(model, path_to_save_data, parameter_dict, mask_previouse_layers, analyse_only_critical)

                if not mask_previouse_layers and analyse_only_critical:
                    layers_dict = nested_dict()
                    first_layer = only_anti_critical_layers[0]
                    anti_critical_layers = list()
                    number_of_turns = 1

                # print("Analyzing only anti-critical {} layers".format(len(only_anti_critical_layers)))

            number_of_layers = len(parameter_dict.keys())
            last_layer = list(parameter_dict.keys())[-1]

            # Iterate till the first layer is the last one
            while first_layer != last_layer:
                index_of_first_layer = list(parameter_dict.keys()).index(first_layer)
                '''
                ### 2) Iterating over layers
                '''
                for layers_index, (layers_name, param) in enumerate(parameter_dict.items()):

                    print("Turn: {}, Analyzing layer:{} of {}/{} ".format(number_of_turns,
                                                                          layers_name,
                                                                          layers_index,
                                                                          number_of_layers))

                    if index_of_first_layer <= layers_index:  # and layers_name in only_anti_critical_layers:

                        original_values = copy.deepcopy(param[0].data)
                        original_values.to(device)
                        masked_values = param[0].data

                        boxes, classes, labels, scores = list(), list(), list(), list()
                        gts, _, images_counter = predict_on_dataset(boxes, classes, labels, scores)

                        kernels = safety.get_layers_dimension(param[0].data)
                        '''
                        ### 3) Iterating over kernels
                        '''
                        layer_dict = nested_dict()
                        for kernel_index in tqdm(kernels):
                            # mask the kernel
                            safety.mask_kernel(masked_values, int(kernel_index))

                            # predict the bounding boxes
                            boxes_m, classes_m, labels_m, scores_m = list(), list(), list(), list()
                            gts, im_sizes, images_counter = predict_on_dataset(boxes_m, classes_m, labels_m, scores_m)

                            # evaluate the criticality of the kernel
                            evaluate_criticality(gts, im_sizes, images_counter, layer_dict, kernel_index,
                                                 boxes_m, classes_m, labels_m, scores_m, boxes, classes, labels, scores)

                            # unmasking the weights
                            safety.unmask_kernel(masked_values, original_values, int(kernel_index))

                        # calculate the mean of the criticalities the layer's kernels
                        layer_criticality_means = list()
                        layer_criticality_stds = list()
                        layer_kernels_raw_criticalities = list()
                        for label, kernels in layer_dict.items():
                            if label in settings.global_analyzed_classes:
                                for kernel, images in kernels.items():
                                    # for each kernel, calculate the mean of all images and save it to an array
                                    test_set_criticalities = np.asarray(
                                        [criticality for image, criticality in images.items()])
                                    test_set_mean_criticality = np.mean(test_set_criticalities)
                                    layer_criticality_means.append(test_set_mean_criticality)
                                    layer_kernels_raw_criticalities.append(test_set_criticalities)
                                    layer_criticality_stds.append(np.std(test_set_criticalities))
                                    layers_dict[str(number_of_turns)][label][layers_name][str(kernel)] = \
                                        [str(np.mean(test_set_criticalities)), str(np.std(test_set_criticalities))]

                        # # find the kernel with the lowest criticality and mask it if value < 0.0
                        # # @FixMe: it will work only in case that settings.global_analyzed_classes contains only one class
                        # anti_cricitcal_mask = [True] * len(layer_kernels_raw_criticalities)
                        # for index, raw_images_cri in enumerate(layer_kernels_raw_criticalities):
                        #     if sum(raw_images_cri) != 0.0:
                        #         for raw_image_cri in raw_images_cri:
                        #             if raw_image_cri > 0.0:
                        #                 anti_cricitcal_mask[index] = False
                        #                 break
                        #     else:
                        #         anti_cricitcal_mask[index] = False
                        #
                        # if True in anti_cricitcal_mask:
                        #     index = list(anti_cricitcal_mask).index(True)
                        #     if type(index) == list:
                        #         masked_critcalities = np.array(layer_criticality_means)[anti_cricitcal_mask]
                        #         index = np.argmin(masked_critcalities)

                        # still 75% of population will be anticritical
                        potential_anti_cri = np.asarray(layer_criticality_means) + (
                                    np.asarray(layer_criticality_stds) / 4.0)
                        print("potential_anti_cri list for layer:")
                        print(potential_anti_cri)

                        if any(value < 0.0 for value in potential_anti_cri):
                            index = np.argmin(potential_anti_cri)

                            if layers_name not in anti_critical_layers:
                                layer_and_index = layers_name + "__" + str(index)
                                anti_critical_layers.append(layers_name)
                                # 1) mask the anti-critical neuron
                                safety.mask_kernel(param[0].data, int(index))
                                # 2) calculate AP
                                ap_per_class_matrix, average_precision, dict_average_precisions = \
                                    model.predict_and_calculate_mean_average_precision(
                                        path_to_save_data,
                                        models_name,
                                        ap_labels)
                                # 3) save the AP for all classes and wanted class - save json
                                ap_statistics_dict[str(number_of_turns)]["all"][layer_and_index] = [average_precision]

                                save_ap(dataset, dict_average_precisions, ap_statistics_dict, class_indices,
                                        layer_and_index, number_of_turns)

                                print("Layer kernels raw criticalities : {}".format(
                                    layer_kernels_raw_criticalities[index]))

                                print("Kernel criticality mean: {}, std: {} ".format(layer_criticality_means[index],
                                                                                     layer_criticality_stds[index]))

                                with open(
                                        os.path.join(path_to_save_data,
                                                     todays_date + "_class_layer_anticritical_index_mAP_dict.json"),
                                        'w') as json_statistics:
                                    json.dump(ap_statistics_dict, json_statistics)

                        with open(os.path.join(path_to_save_data,
                                               todays_date + "_class_layer_criticality_index_dict.json"),
                                  'w') as json_statistics_file:
                            json.dump(layers_dict, json_statistics_file)

                first_layer = copy.deepcopy(anti_critical_layers[0])
                anti_critical_layers = list()
                number_of_turns += 1
