
import os
import sys
import numpy as np
import json
import matplotlib.pyplot as plt


images_to_be_investigated = [5,7,69,86,108,114, 220, 392]

# Commented out IPython magic to ensure Python compatibility.
# %pwd
# %cd drive/MyDrive/Colab_Notebooks/

import scripts.automotive_datasets as DataSet
import scripts.functions as functions
from scripts.safety import SafetyAlgorithm
from scripts.visualization import Visualization
import settings
from PIL import Image
import torch
import cv2


path_to_logging_file = os.path.join("data", "logging")
path_to_labels = os.path.join("data", "dataset", "labels", "coco_labels.json")
path_to_data = os.path.join("data", "dataset", "coco_dataset", "val2017")
path_to_annotations = os.path.join("data", "dataset", "coco_dataset", "annotations", "instances_val2017.json")

global_analyzed_classes = "person"
global_device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
number_of_analyzed_neurons = 8

def save_statistics_per_neuron(models_names, most_or_anti_neurons):
    def nested_dict():
      return collections.defaultdict(nested_dict)

    for models_name in models_names:

        model, parameter_dict, _dataset = settings.take_a_model(models_name, global_device)
        safety = SafetyAlgorithm(model, _dataset)

        for image_indices, image, gts in _dataset.dataset_iterator():
            print("Analyzing image number: {}".format(str(image_indices)))
            image_path = "img_" + str(image_indices[0]) + "-" + str(image_indices[0])

            image_sizes = [image_shape.shape for image_shape in image.data]

            logging_path = os.path.join("data", "logging", models_name)   
            
            with open(os.path.join(logging_path, "criticality", most_or_anti_neurons + '_critical_neurons.json'),
                      'r') as json_file:
                critical_neurons = json.load(json_file)
            sorted_critical_neurons = Visualization.filter_neurons(critical_neurons, most_or_anti_neurons)

            with torch.no_grad():
                count = 0
                boxes, classes, labels, scores = list(), list(), list(), list()
                if models_name == "yolov5":
                    model.predict_yolov5(image, 0.5, image_sizes[1], image_sizes[2], boxes, classes, labels, scores)
                else:
                    model.predict(image, 0.5, boxes, classes, labels, scores)

                temp_dict_fp = nested_dict()
                for mean_value, dict_of_labels in sorted_critical_neurons.items():
                    for label_key, critical_neuron in dict_of_labels.items():
                        if label_key == global_analyzed_classes and count < number_of_analyzed_neurons:
                            param = parameter_dict[critical_neuron["layer"]]
                            layers_name = critical_neuron["layer"]

                            original_values = copy.deepcopy(param[0].data)
                            original_values.to(global_device)
                            masked_values = param[0].data
                            kernel_index = critical_neuron["kernel"]

                            safety.mask_kernel(masked_values, int(kernel_index))
                            boxes_m, classes_m, labels_m, scores_m = list(), list(), list(), list()
                            if models_name == "yolov5":
                                model.predict_yolov5(image, 0.5, (
                                image_sizes[1], image_sizes[2]), boxes_m, classes_m, labels_m, scores_m)
                            else:
                                model.predict(image, 0.5, boxes_m, classes_m, labels_m, scores_m)

                            for image_index, (box_m, cls_m, label_name_m, score_m, box, cls, label_name, score, gt, size) in \
                                    enumerate(
                                        zip(boxes_m, classes_m, labels_m, scores_m, boxes, classes, labels, scores, gts,
                                            image_sizes)):

                                tp_list, iou_list, conf_list, fp_list, gt_labels = functions.calculate_true_boxes(box,
                                                                                                                  size[2],
                                                                                                                  size[1],
                                                                                                                  gt,
                                                                                                                  _dataset,
                                                                                                                  label_name,
                                                                                                                  score)

                                tp_list_m, iou_list_m, conf_list_m, fp_list_m, gt_labels_m = functions.calculate_true_boxes(
                                    box_m,
                                    size[2],
                                    size[1],
                                    gt,
                                    _dataset,
                                    label_name_m,
                                    score_m)
                                # Calculate criticality
                                cr_tp_fp, cr_fp, cr_sum = safety.criticality_per_frame(gt_labels,
                                                                                        tp_list,
                                                                                        tp_list_m,
                                                                                        iou_list,
                                                                                        iou_list_m,
                                                                                        conf_list,
                                                                                        conf_list_m,
                                                                                        fp_list,
                                                                                        fp_list_m)

                                temp_path_logging_img = os.path.join(logging_path, image_path)
                                if not os.path.exists(temp_path_logging_img):
                                    os.makedirs(temp_path_logging_img)


                                with open(os.path.join(temp_path_logging_img, most_or_anti_neurons + "kernel_fp.json"),
                                    'w') as kernel_statistics:
                                    fp_temp_list = [cr_fp[labels] for labels in global_analyzed_classes]
                                    im_index = str(image_indices[0])
                                    temp_dict_fp[im_index]["gts"] = gts
                                    temp_dict_fp[im_index][layers_name][kernel_index]["fp"] = fp_temp_list
                                    temp_dict_fp[im_index][layers_name][kernel_index]["tp_list"] = tp_list.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["tp_list_m"] = tp_list_m.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["iou_list"] = iou_list.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["iou_list_m"] = iou_list_m.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["conf_list"] = conf_list.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["conf_list_m"] = conf_list_m.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["fp_list"] = fp_list.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["fp_list_m"] = fp_list_m.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["box"] = box.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["box_m"] = box_m.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["cls"] = cls.tolist()
                                    temp_dict_fp[im_index][layers_name][kernel_index]["cls_m"] = cls_m.tolist()
                                    json.dump(temp_dict_fp, kernel_statistics)
                            
                            count += 1

import copy
def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = copy.deepcopy(x)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return y


def xyxy_abs_to_rel_bbox(boxes, im_width, im_height):
    boxes_xy = copy.deepcopy(boxes)
    boxes_xy[0] = boxes[0] / im_width
    boxes_xy[1] = boxes[1] / im_height
    boxes_xy[2] = boxes[2] / im_width
    boxes_xy[3] = boxes[3] / im_height
    return boxes_xy

import collections
import cv2

def draw_boxes(boxes_m, labels_m, ious_m, confs_m, _image, colors, labels, ground_truth, line=1):
    # this will help us create a different color for each class
    #
    # read the image with OpenCV

    #image = cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB)
    width, height = _image.size
    if width > height:
        width_resized = 800
        height_resized = 600
    else:
        width_resized = 800
        height_resized = 1052
    string_lenght = 160

    image = np.array(_image.resize((width_resized, height_resized)).convert("RGB"))
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    image_copy = image.copy()
    shapes = np.zeros_like(image, np.uint8)
    mask = shapes.astype(bool)

    # ToDo: mean shift from GT and Variation

    alpha = 1.0 - 10.0/len(boxes_m)
    stat_dict = collections.defaultdict(list)
    for boxes, labeles in zip(boxes_m, labels_m):
        for box, label in zip(boxes, labeles):
            shapes = np.zeros_like(image, np.uint8)
            color = colors[labels.index(label)]
            cv2.rectangle(
                shapes,
                (int(box[0]), int(box[1])),
                (int(box[2]), int(box[3])),
                color, line
            )
            mask = shapes.astype(bool)
            image_copy[mask] = cv2.addWeighted(image_copy, alpha, shapes, 1-alpha, 0)[mask]
            stat_dict[label].append((box[0], box[1]))

    # labeling the std of bounding boxes
    for label, xy_box in stat_dict.items():
        x_list = list()
        y_list = list()

        for entry in xy_box:
            x_list.append(entry[0])
            y_list.append(entry[1])
        x_std = np.std(x_list)
        x_pos = np.mean(x_list) - x_std
        y_std = np.std(y_list)
        y_pos = np.mean(y_list) - y_std
        color = colors[labels.index(label)]
        label_text_x = "x_std={:.0f}px,".format(float(x_std))
        label_text_y = "y_std={:.0f}px".format(float(y_std))
        if y_pos < 0:
            y_pos = 8
        elif (y_pos - 42) > height_resized:
            y_pos = height_resized - 42
        
        if x_pos < 0:
            x_pos = 8
        elif (x_pos + string_lenght) > width_resized:
            x_pos = width_resized - string_lenght

        cv2.putText(image_copy, label_text_x, (int(x_pos), int(y_pos-18)),
                  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1,
                  lineType=cv2.LINE_AA)
        cv2.putText(image_copy, label_text_y, (int(x_pos), int(y_pos)),
                  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1,
                  lineType=cv2.LINE_AA)
        
    # labeling the ground truth bounding boxes
    for gt in ground_truth:
        for entry in gt:
            gt_box = entry['bbox']
            gt_box = xywh2xyxy(gt_box)
            #gt_box = xyxy_abs_to_rel_bbox(gt_box, width_resized, height_resized)
            cv2.rectangle(
                image_copy,
                (int(gt_box[0]), int(gt_box[1])),
                (int(gt_box[2]), int(gt_box[3])),
                (0, 0, 0) , 1
            )
    return image_copy

def draw_heatmap(most_anti):
    dataset = DataSet.CocoDataSet(path_to_data, path_to_annotations, path_to_labels)

    # /content/drive/MyDrive/Colab_Notebooks/data/logging/yolov5/img_0-16/layer_model.model.model.1.conv.weight/img_5/kernel_fp.json
    for image in images_to_be_investigated:
        image_path = "img_" + str(image) + "-" + str(image)
        original_image, target = dataset.coco_val[image]
        plt.imshow(original_image)

        path_to_logging_file = os.path.join("data", "logging", "fasterrcnn_resnet50_fpn", image_path, most_anti + "kernel_fp.json")

        with open(path_to_logging_file) as json_file:
            statistics_dict = json.load(json_file)
            all_bounding_boxes = list()
            all_labels = list()
            all_ious = list()
            all_confs = list()
            all_tps = list()
            all_fps = list()
            ground_truth = None

            for layers_name, kernels_indices in statistics_dict[str(image)].items():
                if layers_name == "gts":
                    ground_truth = kernels_indices
                else:
                    for kernel_index, kernels_info in kernels_indices.items():
                        all_bounding_boxes.append(kernels_info["box_m"])
                        all_labels.append(kernels_info["cls_m"])
                        all_ious.append(kernels_info["iou_list_m"])
                        all_confs.append(kernels_info["conf_list_m"])
                        all_tps.append(kernels_info["tp_list_m"])
                        all_fps.append(kernels_info["fp_list_m"])
                        # "iou_list_m" "conf_list_m" "tp_list_m" "fp_list_m"

            image_overlay = draw_boxes(all_bounding_boxes, all_labels, all_ious, all_confs, original_image, dataset.color_maps, dataset.labels, ground_truth, line=5)
                #cv2.imshow(image_overlay)
            cv2.imwrite(os.path.join("data", "logging", "fasterrcnn_resnet50_fpn", image_path, "layer_" + layers_name, "heat_map_" + image_path + ".png"), image_overlay)

models_names = ["fasterrcnn_resnet50_fpn"]
#save_statistics_per_neuron(models_names, "most")
#save_statistics_per_neuron(models_names, "anti")
draw_heatmap("most")
draw_heatmap("anti")