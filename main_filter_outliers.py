import numpy as np
import json
import os
import torch
import cv2
import PIL
import collections

import scripts.functions as functions
import settings
import scripts.visualization as visualization
from scripts.general_dataset import GeneralDataSet
from scripts.metrics.mean_avg_precision import mean_average_precision
from scripts.metrics.iou import intersection_over_union_tensor, get_intersection_points
from scripts.domain_centric_filters import outliers_filter, outliers_filter_overlapping_bbs, \
    outliers_dense_optical_flow, calculate_area, calculate_rel_object_size


def coco_format_image_iterator(annotations, images, path_to_dataset, part):
    for entry in images:
        if os.path.isfile(os.path.join(path_to_dataset, part, entry["file_name"])):
            yield PIL.Image.open(os.path.join(path_to_dataset, part, entry["file_name"])), annotations[entry["id"]]


def analyze_groundtruth_and_images(path_to_dataset, part, _annotations, _opt_flows, img_sizes):
    number_of_outliers = 0
    prvs_im = None
    scene_cnt = 0
    prv_cnt = 0
    optical_flow_list = list()
    opt_flow_mean = 0.0

    all_pedestrian_classes = ["pedestrian", "Pedestrian", "human.pedestrian.adult", "human.pedestrian.child",
                              "human.pedestrian.construction_worker", "human.pedestrian.personal_mobility",
                              "human.pedestrian.police_officer", "human.pedestrian.stroller",
                              "human.pedestrian.wheelchair", "human.pedestrian.adult", "TYPE_PEDESTRIAN"]

    stats = collections.Counter()
    outliers = collections.defaultdict(list)
    annotations = collections.defaultdict(list)
    for item in _annotations["annotations"]:
        annotations[item["image_id"]].append(item)
    images = _annotations["images"]
    dataset_len = len(_annotations["images"])

    for cnt, (img, gt) in enumerate(coco_format_image_iterator(annotations, images, path_to_dataset, part)):
        print('\r {}/{}'.format(cnt, dataset_len), end='', flush=True)

        # get image in cv format and get width and weight
        if isinstance(img, PIL.JpegImagePlugin.JpegImageFile):
            width, height = img.size
            image_cv = cv2.cvtColor(np.asarray(img), cv2.COLOR_BGR2RGB)
        elif isinstance(img, PIL.PngImagePlugin.PngImageFile):
            width, height = img.size
            image_cv = cv2.cvtColor(np.asarray(img), cv2.COLOR_BGR2RGB)
        elif isinstance(img, np.ndarray):
            depth, width, height = img.shape
            image_cv = cv2.cvtColor(np.asarray(img), cv2.COLOR_BGR2RGB)
            # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        elif isinstance(img, torch.Tensor):
            depth, height, width = img.shape
            image_cv = cv2.cvtColor(np.moveaxis(np.asarray(img), 0, -1), cv2.COLOR_BGR2RGB)

        # 1)
        # Outlier - static sequence
        optical_flow_outlier_condition = False
        if cnt != 0:
            optical_flow_outlier_condition, opt_flow_mean = outliers_dense_optical_flow(prvs_im, image_cv, _opt_flows)

        prvs_im = image_cv
        # float, bool, int
        optical_flow_list.append((str(opt_flow_mean), str(optical_flow_outlier_condition), scene_cnt))

        if optical_flow_outlier_condition:
            if cnt - prv_cnt != 1:
                scene_cnt += 1

            number_of_outliers += 1
            prv_cnt = cnt
            stats["optical_flow_outlier"] += 1

        # 2)
        # Outlier - relative size and distance
        outlier_condition = False
        for item in gt:
            box = item["bbox"]
            dist = item["distance"]
            rel_size, object_cls = calculate_rel_object_size(width, height, item)

            # Definition of outlier conditions
            outlier_condition = outliers_filter(rel_size, dist)

            if outlier_condition:
                number_of_outliers += 1
                stats["relative_size_distance"] += 1

        # 3)
        # Outlier - overlapping bounding boxes
        boxes = np.asarray([np.asarray(item["bbox"]) for item in gt])
        overlapping_bbs_condition = False
        if boxes.shape[0] > 0:
            overlapping_bbs_condition, iou_matrix = outliers_filter_overlapping_bbs(boxes, threshold_iou=0.75)

        if overlapping_bbs_condition:
            number_of_outliers += 1
            stats["overlapping_bbs_outlier"] += 1

        if overlapping_bbs_condition or optical_flow_outlier_condition or outlier_condition:
<<<<<<< Updated upstream
            if len(gt) > 0:
                if "image_id" in gt[0].keys():
                    outliers["outliers"].append(gt[0]["image_id"])
=======
            outliers["outliers"].append(gt[0]["image_id"])
>>>>>>> Stashed changes

    with open(os.path.join(path_to_dataset, part + "_outliers.json"), 'w') as outfile:
        json.dump(outliers, outfile)

    # filter annotations
    filtred_annotations = collections.defaultdict(list)

    for item in _annotations["annotations"]:
        if item["image_id"] not in outliers["outliers"]:
            filtred_annotations["annotations"].append(item)

    for item in _annotations["images"]:
        if item["id"] not in outliers["outliers"]:
            filtred_annotations["images"].append(item)

    import copy
    filtred_annotations["categories"] = copy.deepcopy(_annotations["categories"])

    with open(os.path.join(path_to_dataset, part + "_clean.json"), 'w') as outfile:
        json.dump(filtred_annotations, outfile)

    print("Outliers: {}".format(stats))
    return outliers


if __name__ == '__main__':
    for dataset, flow_thr, img_sizes in zip(settings.datasets_names, settings.datasets_opt_flows, settings.datasets_img_sizes):
        print("Analyzing dataset: {}".format(dataset))
        for part in ["val"]:
            path_to_dataset = os.path.join(settings.path_to_annotations, dataset, "dataset_coco_format")

            data_file = open(os.path.join(path_to_dataset, part + ".json"), 'r')
            annotations = json.load(data_file)
            data_file.close()

            outliers = analyze_groundtruth_and_images(path_to_dataset, part, annotations, flow_thr, img_sizes)

            #for outlier in outliers:



