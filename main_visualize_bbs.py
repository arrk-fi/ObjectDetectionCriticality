import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import tabulate
import collections
from collections import defaultdict

import scripts.automotive_datasets as DataSet
import scripts.functions as functions
from scripts.safety import SafetyAlgorithm
from scripts.visualization import Visualization
import settings
import torch

path_to_logging_file = os.path.join("data", "logging")
path_to_labels = os.path.join("data", "dataset", "labels", "coco_labels.json")
path_to_data = os.path.join("data", "dataset", "coco_dataset", "val2017")
path_to_annotations = os.path.join("data", "dataset", "coco_dataset", "annotations", "instances_val2017.json")

dataset = DataSet.CocoDataSet(path_to_data, path_to_annotations, path_to_labels)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
number_of_analyzed_neurons = 200

import copy


def save_statistics_per_neuron(models_name, most_or_anti_neurons):
    def nested_dict():
        return collections.defaultdict(nested_dict)

    model, parameter_dict, _dataset = settings.take_a_model(models_name, device)
    safety = SafetyAlgorithm(model, _dataset)

    for image_indices, image, gts in _dataset.dataset_iterator():
        neurons_index = 0
        print("Analyzing image number: {}".format(str(image_indices)))
        image_path = "img_" + str(image_indices[0]) + "-" + str(image_indices[0])

        image_sizes = [image_shape.shape for image_shape in image.data]

        logging_path = os.path.join("data", "logging", models_name)

        with open(os.path.join(logging_path, 'concatenated_criticality_mean_std.json'), 'r') as json_file:
            neurons_statistics = json.load(json_file)
        sorted_critical_neurons = Visualization.filter_neurons(neurons_statistics, most_or_anti_neurons)

        with torch.no_grad():
            boxes, classes, labels, scores = list(), list(), list(), list()
            if models_name == "yolov5":
                model.predict_yolov5(image, 0.5, image_sizes[1], image_sizes[2], boxes, classes, labels, scores)
            else:
                model.predict(image, 0.5, boxes, classes, labels, scores)

            temp_dict_fp = nested_dict()
            for mean_value, labels_dict in sorted_critical_neurons.items():
                for label_name, critical_neuron in labels_dict.items():
                    if any(label_name == labels for labels in settings.global_analyzed_classes) and neurons_index < number_of_analyzed_neurons:
                        print("{}/{} {}-neurons found.".format(neurons_index, number_of_analyzed_neurons, most_or_anti_neurons))
                        neurons_index += 1
                        layers_name = critical_neuron["layer"]
                        param = parameter_dict[layers_name]

                        original_values = copy.deepcopy(param[0].data)
                        original_values.to(device)
                        masked_values = param[0].data
                        kernel_index = critical_neuron["kernel"]
                        safety.mask_kernel(masked_values, int(kernel_index))

                        boxes_m, classes_m, labels_m, scores_m = list(), list(), list(), list()
                        if models_name == "yolov5":
                            model.predict_yolov5(image, 0.5, (
                                image_sizes[1], image_sizes[2]), boxes_m, classes_m, labels_m, scores_m)
                        else:
                            model.predict(image, 0.5, boxes_m, classes_m, labels_m, scores_m)

                        for box_m, cls_m, label_m, score_m, box, cls, label, score, gt, size in \
                                zip(boxes_m, classes_m, labels_m, scores_m, boxes, classes, labels, scores, gts,
                                    image_sizes):
                            tp_list, iou_list, conf_list, fp_list, gt_labels = functions.calculate_true_boxes(box,
                                                                                                              size[2],
                                                                                                              size[1],
                                                                                                              gt,
                                                                                                              _dataset,
                                                                                                              label,
                                                                                                              score)

                            tp_list_m, iou_list_m, conf_list_m, fp_list_m, gt_labels_m = functions.calculate_true_boxes(
                                box_m,
                                size[2],
                                size[1],
                                gt,
                                _dataset,
                                label_m,
                                score_m)
                            # Calculate criticality
                            cr_tp_fp, cr_fp, cr_sum = safety.criticality_per_frame(gt_labels,
                                                                                   tp_list,
                                                                                   tp_list_m,
                                                                                   iou_list,
                                                                                   iou_list_m,
                                                                                   conf_list,
                                                                                   conf_list_m,
                                                                                   fp_list,
                                                                                   fp_list_m)

                            fp_temp_list = [cr_fp[labels] for labels in settings.global_analyzed_classes]
                            temp_dict_fp[str(image_indices[0])]["gts"] = gts
                            #temp_dict_fp[str(image_indices[0])]["unmasked"] = gts
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["fp"] = fp_temp_list
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["tp_list"] = tp_list.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["tp_list_m"] = tp_list_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["iou_list"] = iou_list.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["iou_list_m"] = iou_list_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["conf_list"] = conf_list.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["conf_list_m"] = conf_list_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["fp_list"] = fp_list.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["fp_list_m"] = fp_list_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["box"] = box.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["box_m"] = box_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["cls"] = cls.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["cls_m"] = cls_m.tolist()
                            temp_dict_fp[str(image_indices[0])][layers_name][kernel_index]["cri"] = mean_value
                            #temp_dict_fp[str(image_indices[0])][kernel_index]["cr_sum"] = cr_sum

                        safety.unmask_kernel(masked_values, original_values, int(kernel_index))

            temp_path_logging_img = os.path.join(logging_path, image_path)
            if not os.path.exists(temp_path_logging_img):
                os.makedirs(temp_path_logging_img)

            with open(os.path.join(temp_path_logging_img, most_or_anti_neurons+"kernel_fp.json"),
                      'w') as kernel_statistics:
                json.dump(temp_dict_fp, kernel_statistics)


def xywh2xyxy(x):
    # Convert nx4 boxes from [x, y, w, h] to [x1, y1, x2, y2] where xy1=top-left, xy2=bottom-right
    y = copy.deepcopy(x)
    y[0] = x[0]  # - x[:, 2] / 2  # top left x
    y[1] = x[1]  # - x[:, 3] / 2  # top left y
    y[2] = x[0] + x[2]  # bottom right x
    y[3] = x[1] + x[3]  # bottom right y
    return y


def xyxy_abs_to_rel_bbox(boxes, im_width, im_height):
    boxes_xy = copy.deepcopy(boxes)
    boxes_xy[0] = boxes[0] / im_width
    boxes_xy[1] = boxes[1] / im_height
    boxes_xy[2] = boxes[2] / im_width
    boxes_xy[3] = boxes[3] / im_height
    return boxes_xy


import json
import cv2

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm

class MplColorHelper:

  def __init__(self, cmap_name, start_val, stop_val):
    self.cmap = matplotlib.colors.LinearSegmentedColormap.from_list("custom", ["red", "orange", "green"])
    self.norm = mpl.colors.Normalize(vmin=start_val, vmax=stop_val)
    self.scalarMap = cm.ScalarMappable(norm=self.norm, cmap=self.cmap)

  def get_rgb(self, val):
    return self.scalarMap.to_rgba(val)


def get_position_within_histogram(criticality, cricitality_hist):
    for position, cri_thr in enumerate(cricitality_hist):
        if criticality > cri_thr:
            return position


def draw_boxes(statistics_dict, _image, colors, labels, line=1):
    # assign data
    all_bounding_boxes = list()
    all_ious = list()
    all_confs = list()
    all_bounding_boxes_m = list()
    all_labels = list()
    all_labels_m = list()
    all_ious_m = list()
    all_confs_m = list()
    all_tps = list()
    all_fps = list()
    all_cri = list()
    ground_truth = None

    # labeling the ground truth bounding boxes
    cricitality_hist = [1.5, 1.0, 0.5, 0.0, -0.5]
    headers = ["Criticality", "> 1.5", "1.5 - 1.0", "1.0 - 0.5", "0.5 - 0.0"]
    my_table = [["IOU-orig.", 0, 0, 0, 0],
                ["IOU-mean", 0, 0, 0, 0],
                ['IOU-SD', 0, 0, 0, 0],
                ["Conf.-orig.", 0, 0, 0, 0],
                ['Conf.-mean', 0, 0, 0, 0],
                ['Conf.-SD', 0, 0, 0, 0],
                ['Precision:', 0, 0, 0, 0],
                ['Recall:', 0, 0, 0, 0],
                ["FPs", 0, 0, 0, 0]]

    for layers_name, kernels_indices in statistics_dict.items():
        if layers_name == "gts":
            ground_truth = kernels_indices
        else:
            for kernel_index, kernels_info in kernels_indices.items():
                all_labels.append(kernels_info["cls"])
                all_bounding_boxes.append(kernels_info["box"])
                all_ious.append(kernels_info["iou_list"])
                all_confs.append(kernels_info["conf_list"])
                all_bounding_boxes_m.append(kernels_info["box_m"])
                all_labels_m.append(kernels_info["cls_m"])
                all_ious_m.append(kernels_info["iou_list_m"])
                all_confs_m.append(kernels_info["conf_list_m"])
                all_tps.append(kernels_info["tp_list_m"])
                all_fps.append(kernels_info["fp_list_m"])
                all_cri.append(kernels_info["cri"])
            # "iou_list_m" "conf_list_m" "tp_list_m" "fp_list_m"

    width, height = _image.size
    if width > height:
        width_resized = 800
        height_resized = 600
    else:
        width_resized = 800
        height_resized = 1052

    image = np.array(_image.resize((width_resized, height_resized)).convert("RGB"))
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    image_copy = image.copy()

    alpha = 1.0 - 50.0 / len(all_bounding_boxes_m)
    cmap = MplColorHelper(["green", "orange", "red"], np.min(all_cri), np.max(all_cri))

    fp_dict = defaultdict(list)
    iou_dict = defaultdict(list)
    conf_dict = defaultdict(list)
    # Evaluating masked prediction
    for index, (boxes, labeles, confs, ious, criticality, fps) in enumerate(
            zip(all_bounding_boxes_m, all_labels_m, all_confs_m, all_ious_m, all_cri, all_fps)):

        position = get_position_within_histogram(criticality, cricitality_hist)

        if any(analyzed_label in labeles for analyzed_label in settings.global_analyzed_classes):

            for box, label, conf, iou, fp in zip(boxes, labeles, confs, ious, fps):
                if label in settings.global_analyzed_classes:
                    shapes = np.zeros_like(image, np.uint8)
                    color = cmap.get_rgb(criticality)
                    cv2.rectangle(
                        shapes,
                        (int(box[0]), int(box[1])),
                        (int(box[2]), int(box[3])),
                        color, line
                    )
                    mask = shapes.astype(bool)
                    image_copy[mask] = cv2.addWeighted(image_copy, alpha, shapes, 1 - alpha, 0)[mask]
                    iou_dict[str(position)].append(iou)
                    conf_dict[str(position)].append(conf)
        else:
            fp_dict[str(position)].append(index)


    #my_table[0].append("{:.2f}".format(or_iou[0]))
    for positions, ious in iou_dict.items():
        my_table[1][int(positions)] = "{:.2f}".format(np.mean(ious))
        my_table[2][int(positions)] = "{:.2f}".format(np.std(ious))
    #my_table[2].append("{:.2f}".format(np.std(iou_list)))
    #my_table[3].append("{:.2f}".format(or_conf[0]))
    for positions, confs in conf_dict.items():
        my_table[4][int(positions)] = "{:.2f}".format(np.mean(confs))
        my_table[5][int(positions)] = "{:.2f}".format(np.std(confs))
    #my_table[6].append("{:.0f}%".format((sum(tp_list) / kernel_lenght) * 100))
    #my_table[7].append("{:.0f}%".format(((1.0 - sum(tp_list) / kernel_lenght) * 100)))
    for positions, fps in fp_dict.items():
        my_table[8][int(positions)] = "{:.0f}%".format(len(fps))

    # Evaluating un-masked prediction
    for index, (boxes, labeles, confs, ious) in enumerate(
            zip(all_bounding_boxes, all_labels, all_confs, all_ious)):
        for box, label, conf, iou in zip(boxes, labeles, confs, ious):
            if label in settings.global_analyzed_classes:
                #shapes = np.zeros_like(image, np.uint8)
                color = [255.0, 0.0, 0.0]
                cv2.rectangle(
                    image_copy,
                    (int(box[0]), int(box[1])),
                    (int(box[2]), int(box[3])),
                    color, 3
                )
                #mask = shapes.astype(bool)
                #image_copy[mask] = cv2.addWeighted(image_copy, alpha, shapes, 1 - alpha, 0)[mask]

    # Drawing ground-truth
    for gt in ground_truth:
        for index, (entry, or_box, or_iou, or_conf, tp) in enumerate(zip(gt, all_bounding_boxes, all_ious, all_confs, all_tps)):
            if index < 2:
                gt_box = entry['bbox']
                gt_label = dataset.labels[entry['category_id']]
                gt_box = xywh2xyxy(gt_box)
                # gt_box = xyxy_abs_to_rel_bbox(gt_box, width_resized, height_resized)
                cv2.rectangle(
                    image_copy,
                    (int(gt_box[0]), int(gt_box[1])),
                    (int(gt_box[2]), int(gt_box[3])),
                    (0, 0, 0), 1
                )
                #original_box = xywh2xyxy(or_box[0])
                #cv2.rectangle(
                #    image_copy,
                #    (int(original_box[0]), int(original_box[1])),
                #    (int(original_box[2]), int(original_box[3])),
                #    (255,215,0), 3
                #)
                if gt_box[1] - 20 < 0:
                    y_cor = gt_box[1] + 20
                    x_cor = gt_box[0]
                else:
                    y_cor = gt_box[1] - 6
                    x_cor = gt_box[0] + 6
                cv2.putText(image_copy, "index: " + str(index), (int(x_cor), int(y_cor)),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 1,
                            lineType=cv2.LINE_AA)

    return image_copy, my_table, headers


def transpose_list(list):
    numpy_array = np.array(list)
    transpose = numpy_array.T
    transpose_list = transpose.tolist()
    return transpose_list


if __name__ == '__main__':
    for models_name in settings.models_names:

        for most_anti in ["most", "anti"]:
            #save_statistics_per_neuron(models_name, most_anti)

            for image in settings.images_to_be_investigated:
                image_path = "img_" + str(image) + "-" + str(image)
                original_image, target = dataset.coco_val[image]

                path_to_logging_file = os.path.join("data", "logging", models_name, image_path)
                with open(os.path.join(path_to_logging_file, most_anti+"kernel_fp.json")) as json_file:
                    statistics_dict = json.load(json_file)

                image_overlay, my_table, headers = draw_boxes(statistics_dict[str(image)], original_image,
                                                              dataset.color_maps,
                                                              dataset.labels, line=5)
                # cv2.imshow(image_overlay)
                cv2.imwrite(os.path.join("data", "logging", models_name, image_path,
                                         most_anti + "_heat_map_" + image_path + ".png"), image_overlay)

                with open(os.path.join(path_to_logging_file, "latex_table.txt"), "w") as latex_file:
                    latex_file.write(tabulate.tabulate(my_table, headers, tablefmt="latex"))
