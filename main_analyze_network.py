import copy
import os
import json
from tqdm import tqdm
import collections
import torch

import scripts.functions as functions
from scripts.safety import SafetyAlgorithm
import settings

logging_images = True
batch_size = 1
custom_analysis = True

path_to_logging_file = os.path.join("data", "logging")
path_to_labels = os.path.join("data", "dataset", "labels", "coco_labels.json")
path_to_data = os.path.join("data", "dataset", "coco_dataset", "val2017")
path_to_annotations = os.path.join("data", "dataset", "coco_dataset", "annotations", "instances_val2017.json")

models_names = ["fasterrcnn_resnet50_fpn", "ssdlite320_mobilenet_v3_large", "yolov5", "detr"]
models_names = ["detr"]
list_of_analyzed_layers = None
images_to_be_investigated = ["all"]
images_to_be_investigated = [5, 7, 69, 86, 108, 114]
images_to_be_investigated = [int(index) for index in range(96, 5000, 1)]
allowed_classes = None
global_analyzed_classes = "person"


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


if __name__ == '__main__':

    for models_name in models_names:

        if custom_analysis:
            logging_path = os.path.join("data", "logging", models_name)
            critical_neurons_dict = networks.custom_layers_and_kernels(logging_path, "anti", 20)
            list_of_analyzed_layers = list(critical_neurons_dict.keys())

        model, parameter_dict, dataset = networks.take_a_model(models_name)
        filtered_classes = dataset.labels
        # if torch.cuda.device_count() > 1:
        #    print("Let's use", torch.cuda.device_count(), "GPUs!")
        #    model = torch.nn.DataParallel(model)

        safety = SafetyAlgorithm(model, dataset)

        def nested_dict():
            return collections.defaultdict(nested_dict)

        layers_dict = nested_dict()

        with torch.no_grad():
            for counter, (image_indices, image, gts) in enumerate(dataset.dataset_iterator()):
                print("Analyzing image number: {}".format(counter))
                if logging_images:
                    temp_path_logging_img = os.path.join(path_to_logging_file, models_name,
                                                         "img_" + str(min(image_indices)) + "-" + str(
                                                             max(image_indices)))
                    if not os.path.exists(temp_path_logging_img):
                        os.makedirs(temp_path_logging_img)

                image_sizes = [image_shape.shape for image_shape in image.data]
                boxes, classes, labels, scores = list(), list(), list(), list()
                temp_dict_fp = nested_dict()
                for layers_name, param in tqdm(parameter_dict.items()):

                    if logging_images:
                        temp_path_logging_img_layer = os.path.join(temp_path_logging_img, "layer_" + str(layers_name))
                        if not os.path.exists(temp_path_logging_img_layer):
                            os.makedirs(temp_path_logging_img_layer)

                    original_values = copy.deepcopy(param[0].data)
                    original_values.to(device)
                    masked_values = param[0].data
                    if models_name == "yolov5":
                        boxes, classes, labels, scores = model.predict_yolov5(image, 0.5,
                                                                              (image_sizes[0][1], image_sizes[0][2]))
                    else:
                        boxes, classes, labels, scores = model.predict(image, 0.5)

                    if custom_analysis:
                        kernels = list(critical_neurons_dict[layers_name].keys())
                    else:
                        kernels = safety.get_layers_dimension(param[0].data)
                    boxes_m, classes_m, labels_m, scores_m = list(), list(), list(), list()
                    for kernel_index in kernels:
                        # mask the kernel
                        safety.mask_kernel(masked_values, int(kernel_index))

                        if models_name == "yolov5":
                            boxes_m, classes_m, labels_m, scores_m = model.predict_yolov5(image, 0.5, (
                            image_sizes[0][1], image_sizes[0][2]))
                        elif models_name == "detr":
                            boxes_m, classes_m, labels_m, scores_m = model.predict(image, 0.5)
                        else:
                            boxes_m, classes_m, labels_m, scores_m = model.predict(image, 0.5)

                        for image_index, (box_m, cls_m, label_m, score_m, box, cls, label, score, gt, size) in \
                                enumerate(
                                    zip(boxes_m, classes_m, labels_m, scores_m, boxes, classes, labels, scores, gts,
                                        image_sizes)):

                            tp_list, iou_list, conf_list, fp_list, gt_labels = functions.calculate_true_boxes(box,
                                                                                                              size[2],
                                                                                                              size[1],
                                                                                                              gt,
                                                                                                              dataset,
                                                                                                              label,
                                                                                                              score)

                            tp_list_m, iou_list_m, conf_list_m, fp_list_m, gt_labels_m = functions.calculate_true_boxes(
                                box_m,
                                size[2],
                                size[1],
                                gt,
                                dataset,
                                label_m,
                                score_m)
                            # Calculate criticality
                            cr_tp_fp, cr_fp, cr_sum = safety.criticality_per_frame(gt_labels,
                                                                                   tp_list,
                                                                                   tp_list_m,
                                                                                   iou_list,
                                                                                   iou_list_m,
                                                                                   conf_list,
                                                                                   conf_list_m,
                                                                                   fp_list,
                                                                                   fp_list_m)

                            # save image in case the masking triggered FPs
                            if logging_images:  # and (len(fp_temp_list) > 0) and (fp_temp_list[0] > 0.0):
                                image_counter = image_indices[image_index]
                                image_to_save = dataset.coco_val[image_counter][0]
                                resized_image = image_to_save.resize((size[2], size[1]))
                                labeled_image = functions.draw_boxes(box, box_m,
                                                                     cls, cls_m,
                                                                     label, label_m,
                                                                     resized_image,
                                                                     dataset.color_maps,
                                                                     line=1)
                                temp_path_logging_img_layer_img = os.path.join(temp_path_logging_img_layer,
                                                                               "img_" + str(image_counter))
                                if not os.path.exists(temp_path_logging_img_layer_img):
                                    os.makedirs(temp_path_logging_img_layer_img)

                                # cv2.imwrite(
                                #    os.path.join(temp_path_logging_img_layer_img,
                                #                 "kernel_" + str(kernel_index) + "_prediction.jpg"),
                                #    labeled_image)

                                with open(os.path.join(temp_path_logging_img, "kernel_fp.json"),
                                          'w') as kernel_statistics:
                                    fp_temp_list = [cr_fp[labels] for labels in filtered_classes]
                                    temp_dict_fp[image_counter]["gts"] = gts
                                    temp_dict_fp[image_counter][kernel_index]["fp"] = fp_temp_list
                                    temp_dict_fp[image_counter][kernel_index]["tp_list"] = tp_list
                                    temp_dict_fp[image_counter][kernel_index]["tp_list_m"] = tp_list_m
                                    temp_dict_fp[image_counter][kernel_index]["iou_list"] = iou_list
                                    temp_dict_fp[image_counter][kernel_index]["iou_list_m"] = iou_list_m
                                    temp_dict_fp[image_counter][kernel_index]["conf_list"] = conf_list
                                    temp_dict_fp[image_counter][kernel_index]["conf_list_m"] = conf_list_m
                                    temp_dict_fp[image_counter][kernel_index]["fp_list"] = fp_list
                                    temp_dict_fp[image_counter][kernel_index]["fp_list_m"] = fp_list_m
                                    temp_dict_fp[image_counter][kernel_index]["box"] = box
                                    temp_dict_fp[image_counter][kernel_index]["box_m"] = box_m
                                    temp_dict_fp[image_counter][kernel_index]["cls"] = cls
                                    temp_dict_fp[image_counter][kernel_index]["cls_m"] = cls_m
                                    temp_dict_fp[image_counter][kernel_index]["cr_sum"] = cr_sum
                                    json.dump(temp_dict_fp, kernel_statistics)

                            for filtered_labels in filtered_classes:
                                # layers_dict[label][layer][kernel][image_number] = criticality
                                layers_dict[filtered_labels][layers_name][str(kernel_index)][
                                    str(images_to_be_investigated[0] + counter * batch_size + image_index)] = \
                                    copy.deepcopy(cr_sum[filtered_labels])

                        # unmasking the weights
                        safety.unmask_kernel(masked_values, original_values, int(kernel_index))

                json_logging_path_string = str(images_to_be_investigated[0]) + "-" + str(
                    images_to_be_investigated[0] + ((counter + 1) * batch_size)) + "_statistics_dict.json"
                json_logging_path = os.path.join(path_to_logging_file, models_name, json_logging_path_string)
                if not os.path.exists(os.path.join(path_to_logging_file, models_name)):
                    os.makedirs(os.path.join(path_to_logging_file, models_name))

                with open(json_logging_path, 'w') as json_statistics:
                    json.dump(layers_dict, json_statistics)
