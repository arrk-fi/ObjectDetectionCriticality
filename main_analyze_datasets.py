import numpy as np
import json
from tqdm import tqdm
import os
import collections
import torch
from collections import defaultdict
import itertools
import cv2
import PIL
import copy

import scripts.functions as functions
import settings
import scripts.visualization as visualization
from scripts.general_dataset import GeneralDataSet
from scripts.metrics.mean_avg_precision import mean_average_precision
from scripts.metrics.iou import intersection_over_union_tensor, get_intersection_points


def nested_dict():
    return collections.defaultdict(nested_dict)


ap_labels = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def exp_decay_dist_f(x):
    return np.power(np.e, -x / 6.0) + 0.075  # offset at least 1 meter from ego


def outliers_filter_overlapping_bbs(_boxes, threshold_iou=0.4):
    boxes_xyxy = functions.xywh2xyxy(_boxes)
    iou_matrix = intersection_over_union_tensor(
        np.asarray(boxes_xyxy),
        copy.deepcopy(np.asarray(boxes_xyxy))
    )

    iou_matrix = np.tril(iou_matrix.numpy(), -1)

    if np.max(iou_matrix) > threshold_iou:
        return True, iou_matrix
    else:
        return False, iou_matrix


def outliers_filter(rel_size, dist):
    # Definition of outlier conditions
    outlier_condition = 0.0 > rel_size > 1.0
    outlier_condition |= rel_size > exp_decay_dist_f(dist)
    return outlier_condition


def outliers_dense_optical_flow(prvs_im, next_im, _flow_constant):
    # cv.calcOpticalFlowFarneback(	prev, next, flow, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma,
    # flags	) ->	flow
    flow_or_diff = True
    if prvs_im.shape[0] != next_im.shape[0]:
        dim = (prvs_im.shape[1], prvs_im.shape[0])
        next_im = cv2.resize(next_im, dim, interpolation=cv2.INTER_AREA)
    if flow_or_diff:
        flow = cv2.calcOpticalFlowFarneback(cv2.cvtColor(prvs_im, cv2.COLOR_BGR2GRAY),
                                            cv2.cvtColor(next_im, cv2.COLOR_BGR2GRAY),
                                            None, 0.5, 1, 7, 3, 5, 1.1, 0)
        # to discover stop and go
        #hsv = np.zeros_like(prvs_im)
        mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
        mean_val = np.mean(mag, axis=1)  # to take the mean of each row

        mean_val = np.mean(mean_val, axis=0) / _flow_constant
        #flow_mag = np.sum(np.sqrt(flow[:, :, 0] * flow[:, :, 0] + flow[:, :, 1] * flow[:, :, 1]))
        return float(mean_val) < 1.0, mean_val

    else:
        if prvs_im.shape[0] == next_im.shape[0] and prvs_im.shape[1] == next_im.shape[1]:
            diff_im = np.sum(np.abs(prvs_im - next_im))  # background subtraction - switch images to greyscale
        else:
            diff_im = _flow_constant
        # ratio between static and dynamic images
        return np.sum(diff_im) < _flow_constant, np.sum(diff_im)


def calculate_area(bbox):
    # within the datasets classes I cast the format to xywh
    return bbox[2] * bbox[3]


def cast_to_list(list_of_arrays):
    return np.asarray(list_of_arrays).tolist()


def predict_on_dataset(_model, _dataset, _dict_of_predictions):
    for cnt, (images, gts) in enumerate(_dataset.dataset_iterator()):
        print('\r {}/{}'.format(cnt, int(_dataset.dataset_len / _dataset.batch_size)), end='', flush=True)

        _boxes, _classes, _labels, _scores = list(), list(), list(), list()

        if models_name == "yolov5":
            _model.predict_yolov5(images, 0.5, (images.data.shape[1], images.data.shape[2]), _dataset,
                                  _boxes, _classes, _labels, _scores)
        else:
            _model.predict(images, 0.5, _dataset, _boxes, _classes, _labels, _scores)

        if len(gts[0]) > 0:
            image_id = gts[0][0]["image_id"]
            list_of_predictions = list()
            for bbs, clss, labs, scrs in zip(_boxes, _classes, _labels, _scores):
                for bb, cls, lab, scr in zip(bbs.tolist(), clss.tolist(), labs.tolist(), scrs.tolist()):
                    list_of_predictions.append([image_id, cls, scr, bb])
            _dict_of_predictions[image_id] = [gts, [list_of_predictions, images.data.shape]]
            # @FixMe - just to break out
            if image_id > 1500:
                break


def calculate_f_mAP_distance(_dict_of_predictions, _path, _models_name, _dataset_name):
    list_of_true_boxes = list()
    list_of_predictions = list()

    print("Analyzing predictions")
    for image_id, datas in tqdm(_dict_of_predictions.items()):
        gts = datas[0]
        preds = datas[1]

        im_height, im_width = preds[1][2:4]
        for gt in gts:
            for entry in gt:
                true_boxes = functions.xywh2xyxy_1d(np.asarray(entry["bbox"]))
                object_id = entry["category_id"]
                image_id = entry["image_id"]
                distance = 0.0
                # distance = entry["distance"]
                rel_size = calculate_area(true_boxes) / (im_width * im_height)
                list_of_true_boxes.append(
                    [image_id, object_id, 1.0, true_boxes, distance, rel_size, (im_height, im_width)])

        for pred in preds[0]:
            list_of_predictions.append([pred[0], pred[1], pred[2], pred[3]])

    print("Calculating mAPs")
    dict_average_precisions = nested_dict()
    for iou_threshold in tqdm(settings.iou_thresholds):
        average_precision = mean_average_precision(list_of_predictions, list_of_true_boxes, dict_average_precisions,
                                                   labels=dataset.labels,
                                                   iou_threshold=iou_threshold, box_format="corners",
                                                   num_classes=len(dataset.labels))

    with open(os.path.join(_path, "{}_{}_mAP_dataset_stats_dict.json".format(_models_name, _dataset_name)),
              'w') as mAP_statistics:
        json.dump(dict_average_precisions, mAP_statistics)

    return dict_average_precisions


def plot_f_mAP_distance(_dict_average_precisions, path_to_save_data, models_name, dataset_name):
    print("Plotting results of mAP")
    for cls, ious in tqdm(_dict_average_precisions.items()):
        x, y, z = list(), list(), list()
        x_fp = list()
        y_fp = list()
        mAP = list()
        for iou, datas in ious.items():
            # [mAP_class, recalls[1], precisions[1], SCOREs, IOUs, BBrel_size, IMGsize, OBJdistances, FP_score, FP_BB_rel_size]
            gt_indices = [index for index, bb in enumerate(datas[5]) if bb > 0.0]
            mAP.append(datas[0])
            x.append(list(np.asarray(datas[3])[gt_indices]))  # scores
            y.append(list(np.asarray(datas[5])[gt_indices]))  # bb rel size
            z.append(list(np.asarray(datas[7])[gt_indices]))  # object distances
            x_fp.append(datas[8])
            y_fp.append(datas[9])

        name = cls + "_score_vs_object_size"
        label = "Score vs. Object size"
        title = cls + ": Score vs. Object size"
        x = list(itertools.chain.from_iterable(x))
        y = list(itertools.chain.from_iterable(y))
        z = list(itertools.chain.from_iterable(z))
        fp = list(itertools.chain.from_iterable(x_fp))
        FN = x.count(0.0)
        TP = len(y) - FN
        FP = len(fp)
        legend = "mAP:{:2.3f}\nTP:{}\nFN:{}".format(np.sum(mAP) / len(mAP), TP, FN)
        visualization.Visualization.plot_x_y(path_to_save_data, name, np.asarray(x), np.asarray(y), label,
                                             exp_decay_f=None, legends=legend, xlabel="Scores", ylabel="BB rel. size",
                                             title=title)

        label = cls + "_gt_vs_predicted"
        det_indices = [index for index, scr in enumerate(x) if scr > 0.0]
        bb_detected = np.asarray(y)[det_indices]
        n_bb, bins_bb, _ = visualization.Visualization. \
            plot_histogram_of_dataset(np.asarray(y),  # relative BB sizes
                                      path_to_save_data,
                                      label + "_relative_BB_sizes",
                                      dataset_name + "'s dataset distribution of BB relative size for objects: " + cls,
                                      label,
                                      _x_label="Relative BB size [-]",
                                      r_min=0.0, r_max=1.0,
                                      num_bins=100,
                                      _np_data_s=bb_detected)

        n_bb, bins_bb, _ = visualization.Visualization. \
            plot_histogram_of_dataset(np.asarray(z),  # object distances
                                      path_to_save_data,
                                      label + "_object_distance",
                                      dataset_name + ":'s dataset distribution of distance for objects: " + cls,
                                      label,
                                      _x_label="Object distance [m]",
                                      r_min=0.0, r_max=1.0,
                                      num_bins=100,
                                      _np_data_s=bb_detected)


def save_ap(_dataset, _dict_average_precisions, _ap_statistics_dict, _class_indices, _text, _number_of_turns):
    for class_index, ious in _dict_average_precisions.items():
        if class_index in _class_indices:
            ious_aps = [value for iou, value in ious.items()]
            ap = float(sum(ious_aps) / len(ious_aps))
            _ap_statistics_dict[str(_number_of_turns)][_dataset.labels[int(class_index)]][_text] = [ap]
            print("AP for class {}, is {:.2f}".format(_dataset.labels[int(class_index)], ap))


def calculate_rel_object_size(im_width, im_height, item):
    # object area / image area = relative object size
    rel_size = calculate_area(item["bbox"]) / (im_width * im_height)

    # TB Investigated - in AUDI one object has relative size 85 - bug in dataset
    return rel_size, item["class"]


def analyze_groundtruth(_dataset, _stat_dict):
    for counter, (gts, img_sizes) in enumerate(_dataset.label_iterator()):
        print('\r {}/{}'.format(counter * dataset.batch_size, _dataset.dataset_len), end='', flush=True)
        for gt, img_size in zip(gts, img_sizes):
            for item in gt:
                width, height = img_size
                rel_size, object_cls = calculate_rel_object_size(width, height, item)

                _stat_dict[object_cls].append((rel_size, item["distance"], item["bbox"], tuple(img_size)))


def analyze_groundtruth_and_images(_dataset, _stat_dict, _path, _opt_flows):
    number_of_outliers = 0
    prvs_im = None
    scene_cnt = 0
    prv_cnt = 0
    optical_flow_list = list()
    optical_flow_outlier_condition = False
    opt_flow_mean = 0.0

    all_pedestrian_classes = ["pedestrian", "Pedestrian", "human.pedestrian.adult", "human.pedestrian.child",
                              "human.pedestrian.construction_worker", "human.pedestrian.personal_mobility",
                              "human.pedestrian.police_officer", "human.pedestrian.stroller",
                              "human.pedestrian.wheelchair", "human.pedestrian.adult", "TYPE_PEDESTRIAN"]

    save_path = os.path.join(_path, "outliers")
    save_path_bb_size = os.path.join(_path, "outliers", "bb_size")
    save_path_optical_flow = os.path.join(_path, "outliers", "optical_flow")
    save_path_overlapping_bb = os.path.join(_path, "outliers", "overlapping_bb")

    if not os.path.exists(save_path):
        os.makedirs(save_path)
        os.makedirs(save_path_bb_size)
        os.makedirs(save_path_optical_flow)
        os.makedirs(save_path_overlapping_bb)

    for cnt, (torch_batch, batch_targets) in enumerate(_dataset.dataset_iterator()):
        print('\r {}/{}'.format(cnt * dataset.batch_size, dataset.dataset_len), end='', flush=True)

        for gt, image in zip(batch_targets, torch_batch):

            # get image in cv format and get width and weight
            if isinstance(image, PIL.PngImagePlugin.PngImageFile):
                width, height = image.size
                image_cv = cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB)
            elif isinstance(image, np.ndarray):
                depth, width, height = image.shape
                image_cv = cv2.cvtColor(np.asarray(image), cv2.COLOR_BGR2RGB)
                # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            elif isinstance(image, torch.Tensor):
                depth, height, width = image.shape
                image_cv = cv2.cvtColor(np.moveaxis(np.asarray(image), 0, -1), cv2.COLOR_BGR2RGB)

            # 1)
            # Outlier - static sequence
            optical_flow_outlier_condition = False
            if cnt != 0:
                optical_flow_outlier_condition, opt_flow_mean = outliers_dense_optical_flow(prvs_im, image_cv, _opt_flows)
            prvs_im = image_cv
            # float, bool, int
            optical_flow_list.append((str(opt_flow_mean), str(optical_flow_outlier_condition), scene_cnt))

            if optical_flow_outlier_condition:
                if cnt - prv_cnt != 1:
                    scene_cnt += 1
                #cv2.imwrite(os.path.join(save_path_optical_flow, '{}_optical_flow_outlier_scene_{}.jpg'.format(number_of_outliers, scene_cnt)),
                #            image_cv)
                number_of_outliers += 1
                prv_cnt = cnt

            for item in gt:
                box = item["bbox"]
                dist = item["distance"]
                rel_size, object_cls = calculate_rel_object_size(width, height, item)

                # Definition of outlier conditions
                outlier_condition = outliers_filter(rel_size, dist)

                _stat_dict[object_cls].append((rel_size, dist, box, (width, height)))

                if outlier_condition:
                    txt_x_off = 24
                    txt_y_off = 12
                    # All converted format to xywh
                    top_left, bottom_right = (int(box[0]) + txt_y_off, int(box[1]) + txt_x_off), \
                                             (int(box[0] + box[2]), int(box[1] + box[3]))

                    cv2.rectangle(image_cv, top_left, bottom_right,
                                  (255, 0, 0), 3)

                    cv2.putText(image_cv, object_cls, top_left,
                                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255), 2,
                                lineType=cv2.LINE_AA)

                    txt_x_off += 16
                    top_left_text = (int(box[0]) + txt_y_off, int(box[1]) + txt_x_off)
                    rel_size_text = "rel. size: {:.2f}".format(rel_size)
                    cv2.putText(image_cv, rel_size_text, top_left_text,
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1,
                                lineType=cv2.LINE_AA)

                    txt_x_off += 16
                    top_left_text = (int(box[0]) + txt_y_off, int(box[1]) + txt_x_off)
                    rel_size_text = "distance: {:.2f}".format(dist)
                    cv2.putText(image_cv, rel_size_text, top_left_text,
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1,
                                lineType=cv2.LINE_AA)

                    txt_x_off += 16
                    top_left_text = (int(box[0]) + txt_y_off, int(box[1]) + txt_x_off)
                    dist_text = "dist. decay: {:.2f}".format(exp_decay_dist_f(dist))
                    cv2.putText(image_cv, dist_text, top_left_text,
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1,
                                lineType=cv2.LINE_AA)
                    exp_decay_dist_f(dist)

                    outlier_class = item["class"]
                    #cv2.imwrite(os.path.join(save_path_bb_size,
                    #                         '{}_{}_bb_size_outlier.jpg'.format(number_of_outliers, outlier_class)),
                    #            image_cv)
                    number_of_outliers += 1

            boxes = np.asarray([np.asarray(item["bbox"]) for item in gt])
            overlapping_bbs_condition = False
            if boxes.shape[0] > 0:
                overlapping_bbs_condition, iou_matrix = outliers_filter_overlapping_bbs(boxes, threshold_iou=0.4)

            if overlapping_bbs_condition:

                overlapping_outlier = False
                image_copy = np.asarray(image_cv, dtype=np.uint8).copy()

                # filtering only one class for the moment
                objects_class = [item["class"] for item in gt]
                ped_in_gt = [index for index, obj in enumerate(objects_class) if obj in all_pedestrian_classes]

                if len(ped_in_gt) > 0:
                    # intersection threshold to be investigated
                    indices = np.argwhere(iou_matrix > 0.4)
                    for index in indices:
                        # stored two boxes
                        if index[0] in ped_in_gt or index[1] in ped_in_gt:
                            # drawing the objects rectangles
                            txt_x_off = 24
                            txt_y_off = 12
                            box1 = boxes[index[0]]
                            box2 = boxes[index[1]]
                            cls1 = objects_class[index[0]]
                            cls2 = objects_class[index[1]]
                            top_left, bottom_right = functions.xywh2topleft_bottomright(box1)
                            cv2.rectangle(image_copy, top_left, bottom_right,
                                          (255, 0, 0), 2)
                            top_left, bottom_right = functions.xywh2topleft_bottomright(box2)
                            cv2.rectangle(image_copy, top_left, bottom_right,
                                          (255, 0, 0), 2)

                            # drawing the intersections rectangles
                            (x1, y1), (x2, y2) = get_intersection_points(functions.xywh2xyxy_1d(box1),
                                                                         functions.xywh2xyxy_1d(box2))
                            shapes = np.zeros_like(image_cv, np.uint8)
                            cv2.rectangle(
                                shapes,
                                (int(x1), int(y1)),
                                (int(x2), int(y2)),
                                (0, 255, 0), -1
                            )
                            mask = shapes.astype(bool)
                            cv2.rectangle(image_copy, (int(x1), int(y1)), (int(x2), int(y2)),
                                          (255, 255, 255), 2)

                            image_copy[mask] = cv2.addWeighted(image_copy, 0.1, shapes, 0.5, 0)[mask]

                            top_left = (int(box1[0]) + txt_y_off, int(box1[1]) + txt_x_off)
                            cv2.putText(image_copy, cls1 + " " + cls2, top_left,
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 255), 2,
                                        lineType=cv2.LINE_AA)
                            overlapping_outlier = True

                if overlapping_outlier:
                    #cv2.imwrite(os.path.join(save_path_overlapping_bb,
                    #                         '{}_overlapping_BB_outlier.jpg'.format(number_of_outliers)),
                    #            image_copy)
                    number_of_outliers += 1

    with open(os.path.join(path_to_results, "optical_flow_stats_list.json"), 'w') as json_statistics:
        json.dump(optical_flow_list, json_statistics)


def plot_dataset_histogram(_statistics_dict, _dataset, _path_to_dataset_dict, _path_to_save_data):
    rel_bb_dict = defaultdict(list)
    distances_dict = defaultdict(list)

    for label, data in _statistics_dict.items():
        rel_bb_array = np.asarray([item[0] for item in data])
        n_bb, bins_bb, _ = visualization.Visualization. \
            plot_histogram_of_dataset(rel_bb_array,  # relative BB sizes
                                      _path_to_save_data,
                                      label + "_relative_BB_sizes",
                                      _dataset.name + "'s dataset distribution of BB relative size for objects: " + label,
                                      label,
                                      _x_label="Relative BB size [-]", _y_label="frequency",
                                      r_min=0.0, r_max=1.0,
                                      num_bins=100)

        rel_bb_dict[label] = [n_bb, bins_bb, len(data)]

        obj_dist_array = np.asarray([item[1] for item in data])
        n_dis, bins_dis, _ = visualization.Visualization. \
            plot_histogram_of_dataset(obj_dist_array,  # distances in meters
                                      _path_to_save_data,
                                      label + "_distances_in_meters",
                                      _dataset.name + "'s dataset distribution of distance to ego for objects: " + label,
                                      label,
                                      _x_label="Distance to object [m]", _y_label="frequency",
                                      r_min=0.0, r_max=120.0,
                                      num_bins=100)

        distances_dict[label] = [n_dis, bins_dis, len(data)]

        visualization.Visualization.plot_x_y(_path_to_save_data, label + "_bb_relative_to_distance",
                                             obj_dist_array, rel_bb_array,
                                             label=label,
                                             exp_decay_f=exp_decay_dist_f,
                                             legends="None",
                                             xlabel="Distance to object [m]", ylabel="Relative BB size [-]",
                                             title=_dataset.name + ": relative BB size as a function of distance to object for class: " + label)

    # cluster the labels first - before they will be plotted
    matching_labels_dict_rel_bb = defaultdict(list)
    matching_labels_dict_dist = defaultdict(list)
    temp_stats_rel = defaultdict(list)
    temp_stats_dist = defaultdict(list)

    for general_label, matching_datasets in _dataset.clustered_labels.items():
        matching_labels = matching_datasets[_dataset.name]
        for cls, data_1 in rel_bb_dict.items():
            if cls in matching_labels:
                # if key exists - just add the values on top of the previous class
                if general_label in matching_labels_dict_rel_bb.keys():
                    matching_labels_dict_rel_bb[general_label][0] += list(data_1[0])
                    temp_stats_rel[general_label][0] += data_1[0]
                else:
                    matching_labels_dict_rel_bb[general_label] = [list(data_1[0]), list(data_1[1]), data_1[2]]
                    temp_stats_rel[general_label] = data_1

        for cls, data_2 in distances_dict.items():
            if cls in matching_labels:
                # if key exists - just add the values on top of the previous class
                if general_label in matching_labels_dict_dist.keys():
                    matching_labels_dict_dist[general_label][0] += list(data_2[0])
                    temp_stats_dist[general_label][0] += data_2[0]
                else:
                    matching_labels_dict_dist[general_label] = [list(data_2[0]), list(data_2[1]), data_2[2]]
                    temp_stats_dist[general_label] = data_2

    list_of_quantities = temp_stats_rel.values()
    list_of_keys = temp_stats_rel.keys()

    with open(os.path.join(_path_to_save_data, "matching_labels_dataset_rel_bb_stats.json"), 'w') as json_file_1:
        json.dump(matching_labels_dict_rel_bb, json_file_1)

    # plot_x_y(plot_path, name, x, y, legend="None", xlabel="None", ylabel="None", title="None")
    visualization.Visualization.plot_x_y(_path_to_save_data, "all_labels_in_on_bb_relative",
                                         list_of_quantities, list_of_quantities,
                                         label=None,
                                         exp_decay_f=exp_decay_dist_f,
                                         legends=list_of_keys,
                                         xlabel="Relative BB size [-]", ylabel="frequency density",
                                         title="Distribution of objects' relative BB sizes",
                                         figsize=(8, 4))

    list_of_quantities = temp_stats_dist.values()
    list_of_keys = temp_stats_dist.keys()

    with open(os.path.join(_path_to_save_data, "matching_labels_dataset_dist_stats.json"), 'w') as json_file_2:
        json.dump(matching_labels_dict_dist, json_file_2)

    visualization.Visualization.plot_x_y(_path_to_save_data, "all_labels_in_on_obj_distances",
                                         list_of_quantities, list_of_quantities,
                                         label=None,
                                         exp_decay_f=exp_decay_dist_f,
                                         legends=list_of_keys,
                                         xlabel="Distance to object [m]", ylabel="frequency density",
                                         title="Distribution of objects' distances",
                                         figsize=(8, 4))


if __name__ == '__main__':

    analyze_datasets = False
    analyze_outliers = False  # will iterate over images - can take a long time
    online = False
    analyze_predictions = True
    export_datasets = False
    concatenate_datasets = False

    if export_datasets:
        for dataset_name, train_test_val in zip(settings.datasets_names, settings.datasets_split):
            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name)

            dataset = settings.get_a_dataset(dataset_name,
                                             _batch_size=1,
                                             export_to_coco=True,
                                             train_test_val=train_test_val)

    """
    ### 1 ) Analyze dataset
    ### a) size of objects contained by labels
    """
    if analyze_datasets:
        for dataset_name, train_test_val, flow_const in zip(settings.datasets_names, settings.datasets_split, settings.datasets_opt_flows):

            statistics_dict = defaultdict(list)

            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name)
            path_to_results = os.path.join(path_to_dataset, "results", train_test_val)
            if not os.path.exists(path_to_results):
                os.makedirs(path_to_results)

            if online:
                dataset = settings.get_a_dataset(dataset_name, _batch_size=1, train_test_val=train_test_val)

                if analyze_outliers:
                    print("Analyzing {} dataset - ground truths and outliers".format(dataset_name))
                    analyze_groundtruth_and_images(dataset, statistics_dict, path_to_results, flow_const)

                else:
                    print("Analyzing {} dataset - ground truths".format(dataset_name))
                    analyze_groundtruth(dataset, statistics_dict)

                with open(os.path.join(path_to_results, "dataset_stats_dict.json"), 'w') as json_statistics:
                    json.dump(statistics_dict, json_statistics)
            else:
                print("Analyzing {} dataset - only ground truths".format(dataset_name))

                dataset = GeneralDataSet(dataset_name, train_test_val, 1, path_to_dataset)

                if os.path.exists(os.path.join(path_to_results, "dataset_stats_dict.json")):
                    with open(os.path.join(path_to_results, "dataset_stats_dict.json"), 'r') as json_statistics:
                        statistics_dict = json.load(json_statistics)
                else:
                    print("File not found - program terminated")
                    break

            # stat_dict [key]: class of te object -> items
            # [0] -> relative size of the object
            # [1] -> distance to the ego car
            # [2] -> list of BB coordinates [x,y,w,h]
            # [3] -> tuple size of image (w,h)

            plot_dataset_histogram(statistics_dict, dataset, path_to_results, path_to_results)
            for label, datas in statistics_dict.items():
                visualization.Visualization.draw_heatmap(np.asarray(datas)[:, 2], label, np.asarray(datas)[:, 1],
                                                         list(np.asarray(datas)[:, 3]), np.asarray(datas)[:, 0],
                                                         path_to_results)

            #visualization.Visualization.create_video_of_static_images(
            #    os.path.join(path_to_results, "outliers", "optical_flow"))

    """
    ### 2 ) Analyze predictions of a model
    ### a) mAP related to object size
    ### -- x: precision, y: recall, z: relative size of object
    """
    if analyze_predictions:
        for dataset_name, train_test_val in zip(settings.datasets_names, settings.datasets_split):

            statistics_dict = defaultdict(list)

            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name, train_test_val)
            dataset = settings.get_a_dataset(dataset_name, train_test_val=train_test_val, _device=device, _batch_size=1)

            for models_name in settings.models_names:

                boxes, classes, labels, scores = list(), list(), list(), list()

                model, _ = settings.take_a_model(models_name, device)

                path_to_save_data = os.path.join(settings.path_to_logging_file, models_name)
                if not os.path.exists(path_to_save_data):
                    os.makedirs(path_to_save_data)

                if online:
                    # predict on the complete dataset
                    dict_of_predictions = nested_dict()
                    predict_on_dataset(model, dataset, dict_of_predictions)

                    with open(
                            os.path.join(path_to_save_data, '{}_{}_predictions.json'.format(models_name, dataset_name)),
                            'w') as outfile:
                        json.dump(dict_of_predictions, outfile)

                else:
                    with open(
                            os.path.join(path_to_save_data, '{}_{}_predictions.json'.format(models_name, dataset_name)),
                            'r') as json_predictions:
                        dict_of_predictions = json.load(json_predictions)

                    d1 = dict(list(dict_of_predictions.items())[:1000])
                    dict_average_precisions = calculate_f_mAP_distance(d1, path_to_save_data, models_name, dataset_name)

                    plot_f_mAP_distance(dict_average_precisions, path_to_save_data, models_name, dataset_name)
    """
    ### 3 ) Concatenate models and create optical flow diagram
    ### 
    """
    if concatenate_datasets:
        all_datasets_bb_stats = defaultdict(list)
        all_datasets_dist_stats = defaultdict(list)

        for dataset_name, train_test_val, flow_const in zip(settings.datasets_names, settings.datasets_split, settings.datasets_opt_flows):

            print("Concatenating {} dataset - optical flow and labels".format(dataset_name))

            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name)
            path_to_results = os.path.join(path_to_dataset, "results", train_test_val)
            if not os.path.exists(path_to_results):
                break

            # a) concatenate all datasets
            with open(os.path.join(path_to_results, "matching_labels_dataset_rel_bb_stats.json"), 'r') as json_file_1:
                matching_labels_dict_rel_bb = json.load(json_file_1)
                all_datasets_bb_stats[dataset_name].append(matching_labels_dict_rel_bb)

            with open(os.path.join(path_to_results, "matching_labels_dataset_dist_stats.json"), 'r') as json_file_2:
                matching_labels_dict_dist = json.load(json_file_2)
                all_datasets_dist_stats[dataset_name].append(matching_labels_dict_dist)

            with open(os.path.join(path_to_results, "optical_flow_stats_list.json"), 'r') as json_file_3:
                optical_flow_list = json.load(json_file_3)
                visualization.Visualization.plot_optical_flow(path_to_results, optical_flow_list, flow_const, dataset_name)

        reversed_sorted_bb = nested_dict()

        for datasets_name, datas in all_datasets_bb_stats.items():
            for labels_name, stats in datas[0].items():
                reversed_sorted_bb[labels_name][datasets_name] = stats

        for labels_name, datas in reversed_sorted_bb.items():
            # presort the dictionary based on sum of values - for nicer plotting ob overlapping bars
            sorted_dict = {k: v for k, v in sorted(datas.items(), key=lambda item: np.sum(np.asarray(item[1][0])/max(item[1][0])), reverse=True)}
            list_of_quantities = sorted_dict.values()
            list_of_keys = sorted_dict.keys()
            visualization.Visualization.plot_all(settings.path_to_dataset, labels_name + "_all_datasets_bb_relative",
                                                 list_of_quantities, list_of_quantities,
                                                 label=None,
                                                 exp_decay_f=exp_decay_dist_f,
                                                 legends=list_of_keys,
                                                 xlabel="BB relative size [-]", ylabel="frequency density",
                                                 title="Distribution of relative BB sizes for objects: " + labels_name,
                                                 figsize=(8, 4))

        reversed_sorted_dist = nested_dict()
        for datasets_name, datas in all_datasets_dist_stats.items():
            for labels_name, stats in datas[0].items():
                reversed_sorted_dist[labels_name][datasets_name] = stats

        for labels_name, datas in reversed_sorted_dist.items():
            sorted_dict = {k: v for k, v in sorted(datas.items(), key=lambda item: np.sum(np.asarray(item[1][0])/max(item[1][0])), reverse=True)}
            list_of_quantities = sorted_dict.values()
            list_of_keys = sorted_dict.keys()
            visualization.Visualization.plot_all(settings.path_to_dataset, labels_name + "_all_datasets_dist",
                                                 list_of_quantities, list_of_quantities,
                                                 label=None,
                                                 exp_decay_f=exp_decay_dist_f,
                                                 legends=list_of_keys,
                                                 xlabel="Distance to object [m]", ylabel="frequency density",
                                                 title="Distribution of distances for objects: " + labels_name,
                                                 figsize=(8, 4))
