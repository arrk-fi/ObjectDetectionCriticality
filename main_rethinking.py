import matplotlib.pyplot as plt
import numpy as np
import json
from tqdm import tqdm
import os
import collections
import torch
from collections import defaultdict
import itertools
import cv2
import PIL

import scripts.functions as functions
from scripts.safety import SafetyAlgorithm
import settings
import scripts.visualization as visualization


def nested_dict():
    return collections.defaultdict(nested_dict)


ap_labels = [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


def f_lin(x):
    return x


def f_quad(x):
    return 1 - np.power(1 - x, 2)


def f_cubic(x):
    return 1 - np.power(1 - x, 3)


def f_pow_of_eight(x):
    return 1 - np.power(1 - x, 8)


def f_tanh(x):
    return np.tanh(x) * 1 / np.tanh(1)


def plot_activations():
    x_val = np.linspace(0.0, 1.0, 100)
    y_lin = f_lin(x_val)
    y_quad = f_quad(x_val)
    y_tanh = f_tanh(x_val)
    y_cubic = f_cubic(x_val)
    y_pow_eight = f_pow_of_eight(x_val)

    plt.plot(x_val, y_lin, "b", label=r'$f(x) = x$')
    plt.plot(x_val, y_quad, "r", label=r'$f(x) = 1 - {(1 - x)}^2$')
    plt.plot(x_val, y_tanh, "g", label=r'$f(x) = tanh(x) - \frac{1}{tanh(1)}$')
    plt.plot(x_val, y_cubic, "y", label=r'$f(x) = 1 - {(1 - x)}^3$')
    plt.plot(x_val, y_pow_eight, label=r'$f(x) = 1 - {(1 - x)}^8$')
    plt.grid(True)
    plt.xlabel("x - IoU between image and BB")
    plt.ylabel("f(x)")
    plt.title("Criticality ration based on the size of the image")
    plt.legend()
    plt.show()
    plt.savefig("criticality_ration_function.png", dpi=300)


def calculate_area(bbox):
    # within the datasets classes I cast the format to xywh
    return bbox[2] * bbox[3]


def predict_on_dataset(_boxes, _classes, _labels, _scores):
    ground_truths = list()
    images_cnt = list()
    images_shape = list()

    for counter, (image_indices, images, gts) in enumerate(dataset.dataset_iterator()):
        print('\r {}/{}'.format(counter, int(dataset.dataset_len / dataset.batch_size)), end='', flush=True)
        ground_truths.append(gts)
        images_cnt.append(image_indices)
        images_shape.append([images.data.shape] * len(image_indices))

        if models_name == "yolov5":
            model.predict_yolov5(images, 0.5, (images.data.shape[1], images.data.shape[2]), dataset,
                                 _boxes, _classes, _labels, _scores)
        else:
            model.predict(images, 0.5, dataset, _boxes, _classes, _labels, _scores)

    ground_truths_flat = list(itertools.chain.from_iterable(ground_truths))
    images_counter_flat = list(itertools.chain.from_iterable(images_cnt))
    images_shape_flat = list(itertools.chain.from_iterable(images_shape))
    return ground_truths_flat, images_shape_flat, images_counter_flat


def save_ap(_dataset, _dict_average_precisions, _ap_statistics_dict, _class_indices, _text, _number_of_turns):
    for class_index, ious in _dict_average_precisions.items():
        if class_index in _class_indices:
            ious_aps = [value for iou, value in ious.items()]
            ap = float(sum(ious_aps) / len(ious_aps))
            _ap_statistics_dict[str(_number_of_turns)][_dataset.labels[int(class_index)]][_text] = [ap]
            print("AP for class {}, is {:.2f}".format(_dataset.labels[int(class_index)], ap))


if __name__ == '__main__':

    analyze_datasets = True
    online = True
    analyze_predictions = False

    """
    ### 1 ) Analyze dataset
    ### a) size of objects contained by labels
    """
    if analyze_datasets:
        for dataset_name in settings.datasets_names:

            statistics_dict = defaultdict(list)

            dataset = settings.get_a_dataset(dataset_name)
            path_to_dataset = os.path.join(settings.path_to_dataset, dataset_name)

            if online:
                print("Analyzing {} dataset - only ground truths".format(dataset_name))
                for counter, (gts, img_sizes) in enumerate(dataset.label_iterator()):
                    print('\r {}/{}'.format(counter * dataset.batch_size, dataset.dataset_len), end='', flush=True)
                    analyze_groundtruth(gts, img_sizes, statistics_dict)

                with open(os.path.join(path_to_dataset, "dataset_stats_dict.json"),
                          'w') as json_statistics:
                    json.dump(statistics_dict, json_statistics)
            else:
                with open(os.path.join(path_to_dataset, "dataset_stats_dict.json"),
                          'r') as json_statistics:
                    statistics_dict = json.load(json_statistics)

            # stat_dict [key]: class of te object -> items
            # [0] -> relative size of the object
            # [1] -> distance to the ego car
            # [2] -> list of BB coordinates [x,y,w,h]
            # [3] -> tuple size of image (w,h)

            plot_dataset_histogram(statistics_dict, dataset_name, path_to_dataset, path_to_dataset)
            for label, datas in statistics_dict.items():
                visualization.Visualization.draw_heatmap(np.asarray(datas)[:, 2], label, np.asarray(datas)[:, 1],
                                                         list(np.asarray(datas)[:, 3]), path_to_dataset)

    """
    ### 2 ) Analyze predictions of a model
    ### a) mAP related to object size
    ### -- x: precision, y: recall, z: relative size of object
    """
    if analyze_predictions:

        for models_name in settings.models_names:

            boxes, classes, labels, scores = list(), list(), list(), list()

            model, parameter_dict = settings.take_a_model(models_name, device)

            path_to_save_data = os.path.join(settings.path_to_logging_file, models_name)
            if not os.path.exists(path_to_save_data):
                os.makedirs(path_to_save_data)

            # predict on the complete dataset
            ap_per_class_matrix, average_precision, dict_average_precisions = \
                model.predict_and_calculate_mean_average_precision(dataset, path_to_save_data)

            with open(os.path.join(path_to_save_data, 'dict_average_precisions.json'), 'w') as outfile:
                json.dump(dict_average_precisions, outfile)

